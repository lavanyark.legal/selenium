package com.core.qa.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.core.qa.base.TestBase;
import com.paulhammant.ngwebdriver.NgWebDriver;

public class quoteorderdetailpage extends TestBase{
	
	@FindBy(xpath="//span[contains(text(),'With Flat Embroidery')]//parent::div")
	WebElement withflatembroidrey;
	
	@FindBy(xpath="//span[contains(text(),'With 3D Embroidery')]//parent::div")
	WebElement threedembroidrey;
	
	@FindBy(xpath="//span[contains(text(),'Start Designing Now ')]//parent::button")
	WebElement startdesigning;
	
	@FindBy(xpath="//a[contains(text(),'Add Text')]//parent::li")
	WebElement addtext;
	
	@FindBy(xpath="//textarea")
	WebElement text;
	
	@FindBy(xpath="//span[contains(text(),' Add to Design ')]//parent::button")
	WebElement addtodesign;
	
	@FindBy(xpath="//a[contains(text(),'Clipart Library')]//parent::li")
	WebElement addclipart;
	
	@FindBys({
        @FindBy(xpath="//cdk-virtual-scroll-viewport//img"),
 })
	 List<WebElement>clipartlib;
	
	@FindBys({
        @FindBy(xpath="//div[@class='art-con ng-star-inserted']//p"),
 })
	 List<WebElement>clipartlibrary;
	
	@FindBys({
        @FindBy(xpath="//mat-card-content//li"),
 })
	 List<WebElement>clipartlib1;
	
	
	@FindBy(xpath="//a[contains(text(),'Back')]")
	WebElement back;
	
	@FindBy(xpath="//a[contains(text(),'Upload Picture')]//parent::li")
	WebElement uploadpic;
	
	@FindBy(xpath="//div[@class='dropzone dz-single dz-clickable']")
	WebElement upload;
	
	@FindBy(xpath="//span[contains(text(),'Finish Select Color')]//parent::button")
	WebElement finishselectcolour;
	
	@FindBy(xpath="//span[contains(text(),' Continue to Place Order ')]//parent::button")
	WebElement continuetoplaceorder;
	
	@FindBy(xpath="//span[contains(text(),' Sign In and Place Order ')]//parent::button")
	WebElement signinandplaceorder;
	
	@FindBy(xpath="//textarea[@placeholder='Enter a location']")
	WebElement address;
	
	@FindBy(xpath="//span[contains(text(),' Get Estimated Delivery Date and Shipping Cost ')]//parent::button")
	WebElement estimatebutton;
	
	@FindBy(xpath="//span[contains(text(),' Proceed to Checkout')]//parent::button")
	WebElement proceedtocheck;
	
	@FindBy(xpath="//span[contains(text(),'Done')]//parent::button")
	WebElement Done;

	@FindBy(xpath="//span[contains(text(),' Continue to Submit Quote Request ')]//parent::button")
	WebElement submitquotebutton;
	
	@FindBys({
        @FindBy(xpath="//input[@formcontrolname='email']"),
 })
	 List<WebElement>Email;
	
	
        @FindBy(xpath="//input[@formcontrolname='email']")
        WebElement Emails;
	
	@FindBys({
        @FindBy(xpath="//input[@formcontrolname='password']"),
 })
	 List<WebElement>password;
	

        @FindBy(xpath="//span[contains(text(),'Sign In and Submit Quote Request')]//parent::span//parent::button")

	 WebElement signinandsubmitquoterequestbutton1;
	
	@FindBy(xpath="//span[contains(text(),'Sign In and Submit Quote Request')]//parent::span//parent::button")
	WebElement signinandsubmitquoterequestbutton;
	
	@FindBys({
        @FindBy(xpath="//div[@class='product-detail']//input[@type='number']"),
 })
	 List<WebElement>quantity;
	
	
	@FindBy(xpath="//span[contains(text(),' Continue to Submit Quote Request ')]//parent::span//parent::button")
	WebElement continuetosubmit;

	
	@FindBy(xpath="//input[@formcontrolname='phone']")
	WebElement phone;
	
	@FindBy(xpath="//input[@formcontrolname='zip']")
	WebElement zip;
	
	@FindBy(xpath="//span[contains(text(),'Submit Quote Request')]//parent::span//parent::button")
	WebElement submitquote;
	
	@FindBy(xpath="//span[contains(text(),' Create Your Account ')]")
	WebElement createaccount;
	
	@FindBy(xpath="//span[contains(text(),' Create New Account And Submit Quote Request ')]//parent::button")
	WebElement createnewaccountandsubmitquote;
	
	@FindBy(xpath="//input[@formcontrolname='first_name']")
	WebElement firstname;
	
	@FindBy(xpath="//input[@formcontrolname='last_name']")
	WebElement lastname;
	
	@FindBy(xpath="//input[@formcontrolname='confirm_password']")
	WebElement confirmpassword;
	
	@FindBy(linkText="CREATE YOUR OWN")
	WebElement createyourown;
	
	@FindBys({
        @FindBy(xpath="//ngx-ecommerce-core-product-card//mat-card//a"),
 })
	 List<WebElement>products;
	
	@FindBys({
        @FindBy(xpath="//ngx-ecommerce-core-product-card//mat-card//a//div[2]//div//div"),
 })
	 List<WebElement>productsstyle;
	
	@FindBys({
        @FindBy(xpath="//div[@class='tile']//img"),
 })
	 List<WebElement>productssize;
	
	@FindBys({
        @FindBy(xpath="//div[@class='preview-wrap d-inline-block ng-star-inserted']//img"),
 })
	 List<WebElement>colourswatch;
	
	@FindBy(xpath="//span[contains(text(),' Select Product and Start Design Now ')]//parent::button")
	WebElement selectproductanddesignnow;
	
	@FindBy(xpath="//span[contains(text(),' ADD PRODUCTS ')]//parent::button")
	WebElement addproducts;
	
	@FindBy(xpath="//mat-card//span[contains(text(),' Add Product ')]//parent::button")
	WebElement addproduct;
	
	@FindBys({
	@FindBy(xpath="//div[@class='pct-add-product']//p//span")
	 })
	List<WebElement> stylee;
	
	
	@FindBy(xpath="//*[@id=\"app\"]/ngx-ecommerce-core-layout/mat-sidenav-container/mat-sidenav-content/div/app-raq-thankyou/div/div/h2")
	WebElement Thankyoupage;
	
	@FindBys({
        @FindBy(xpath="//span[contains(text(),' Place Order')]//parent::button"),
 })
	 List<WebElement>placeorder;
	
	@FindBy(xpath="//span[contains(text(),' Continue ')]//parent::span//parent::button")
	WebElement continuee;
	
	@FindBy(xpath="//span[contains(text(),' Create Account and Submit Quote Request ')]//parent::button")
	WebElement  CreateAccountandSubmitQuoteRequest ;
	
	@FindBy(xpath="//span[contains(text(),'Submit Quote Request as Guest')]//parent::span//parent::button")
	WebElement  SubmitQuoteRequestasGuest ;
	
	@FindBy(xpath="//div[@class='decoration-popup-wrap']//span[contains(text(),'Submit Quote Request as Guest')]//parent::span//parent::button")
	WebElement  popupSubmitQuoteRequestasGuest ;
	
	@FindBy(xpath="//app-raq-thankyou//a")
	WebElement  quoteno ;
	
	public quoteorderdetailpage() {
		
		 PageFactory.initElements(driver, this);
	}
	
	public void Quickquotetest1() throws InterruptedException, AWTException {
		Robot robot=new Robot();
		createyourown.click();
		products.get(2).click();
		colourswatch.get(1).click();
		selectproductanddesignnow.click();
		 Thread.sleep(10000);
		 addtext.click();
		 Thread.sleep(5000);
		 text.sendKeys("FAYA");
		 addtodesign.click();
		Thread.sleep(5000);
		 continuee.click();
		 int Sum=0;
		 Random rand = new Random();
		 int j=0;
		 Thread.sleep(5000);
		// Addanothercolurqty.click();
		 int i;
		 for(i=0;i<quantity.size();i++) {
			 quantity.get(i).clear();
			 quantity.get(i).sendKeys(String.valueOf(getRandomInteger(24, 25)));
			
		 }
		 continuee.click();
		 phone.sendKeys("(123) 333-3333 x33333");
		 zip.sendKeys("91761");
		 signinandsubmitquoterequestbutton.click();
		 Email.get(0).sendKeys(prop.getProperty("Emailid"));
		 password.get(0).sendKeys(prop.getProperty("password"));
		 signinandsubmitquoterequestbutton.click();
		
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 Thankyoupage.isDisplayed();
		 String quotenum=quoteno.getText();
		 quoteno.click();
		 String quotenumdetail=driver.findElement(By.xpath("//h2[contains(text(),' Quote Details ')]//span//strong")).getText();
		 Assert.assertTrue(quotenum.contains(quotenumdetail));
		 String status=driver.findElement(By.xpath("//h3//span[@class='fw-500 text-link']")).getText();
		 Assert.assertEquals(status, " Quote - New");
		 driver.findElement(By.xpath("//div[@class='dst-image']")).isDisplayed();
		 
		 
		 
	}
	
	
	
	

}
