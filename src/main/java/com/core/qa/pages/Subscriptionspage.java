package com.core.qa.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.core.qa.base.TestBase;

public class Subscriptionspage extends TestBase {
	
	@FindBys({
	@FindBy(xpath="//mat-checkbox[@class='mat-checkbox mat-primary ng-untouched ng-pristine ng-valid']//div"),
	 })
	List<WebElement> subscriptionuncheck;
	
	@FindBy(xpath="//mat-checkbox[@class='mat-checkbox mat-primary ng-untouched ng-pristine ng-valid']//div[@class='mat-checkbox-inner-container']")
	WebElement subscriptionuncheck1;
	
	@FindBy(xpath="//span[contains(text(),'Thank You for subscribing!')]")
	WebElement thankyousubscriptionsmessage;
	
	@FindBy(xpath="//span[contains(text(),'Unsubscribed!')]")
	WebElement Unsubscribed;
	
	@FindBy(xpath="//span[contains(text(),'Save')]//parent::button")
	WebElement Save;
	
	@FindBy(xpath="//mat-checkbox[@class='mat-checkbox mat-primary ng-untouched ng-pristine ng-valid mat-checkbox-checked']//div[@class='mat-checkbox-inner-container']")
	WebElement subscriptionuncheck2;
	
	@FindBy(xpath="//span[contains(text(),'You’re Already Subscribed!')]")
	WebElement Alreadysubscribed;
	


	@FindBy(xpath="//mat-checkbox[@class='mat-checkbox mat-primary ng-valid ng-dirty ng-touched mat-checkbox-checked']//div[@class='mat-checkbox-inner-container']")
	WebElement subscriptionchecked;
	
	
	
	public Subscriptionspage() {
		
		 PageFactory.initElements(driver, this);
	}
	public void subsciptiontest1() throws InterruptedException {
		
		if(subscriptionchecked.isSelected())
		{
			System.out.println("11");
			Save.click();
			Alreadysubscribed.isDisplayed();
			Thread.sleep(5000);
			subscriptionuncheck2.click();
			Save.click();
			Unsubscribed.isDisplayed();
		}
	}
		public void subsciptiontest2() throws InterruptedException {
			
		
		if(subscriptionuncheck1.isDisplayed()) {
			System.out.println("22");
			subscriptionuncheck1.click();
			Save.click();
			thankyousubscriptionsmessage.isDisplayed();
		}
}
}
