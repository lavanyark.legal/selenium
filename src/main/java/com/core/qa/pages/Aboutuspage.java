package com.core.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.core.qa.base.TestBase;

public class Aboutuspage extends TestBase {
	
	@FindBy(linkText="About Us")
	WebElement AboutUs;
	
	@FindBy(xpath="//div[@class='cms-wrap fw-light ng-star-inserted']//img")
	WebElement aboutusbanner;
	
	@FindBy(xpath="//h2")
	WebElement header;
	
	public Aboutuspage() {
		
		 PageFactory.initElements(driver, this);
	}
	
	 public String aboutusurl() throws InterruptedException
	  {
		 AboutUs.click();
		 Thread.sleep(10000);
		 return driver.getCurrentUrl();

	  }
	 
	 public boolean aboutusbannercheck() throws InterruptedException
	  {
		 AboutUs.click();
		 Thread.sleep(5000);
		 return aboutusbanner.isDisplayed();

	  }
	 public boolean aboutusheadercheck() throws InterruptedException
	  {
		 AboutUs.click();
		 Thread.sleep(5000);
		 return header.isDisplayed();

	  }
}
