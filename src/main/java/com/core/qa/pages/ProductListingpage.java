package com.core.qa.pages;

import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.core.qa.base.TestBase;

public class ProductListingpage extends TestBase {
		
		@FindBy(xpath="//a//span[contains(text(),'Product search filter')]")
		WebElement Productsearchfilter ;
		
		@FindBy(xpath="//a[@class='mat-focus-indicator mat-menu-trigger sort-select p-0 mat-button mat-button-base']")
		WebElement Sortby ;
		
		@FindBy(xpath="//mat-expansion-panel//span[contains(text(),' T-Shirts ')]")
		WebElement Tshirts ;
		
		@FindBy(xpath="//mat-expansion-panel//span[contains(text(),'Accessories')]")
		WebElement Accessories ;
		
		@FindBy(xpath="//mat-expansion-panel//span[contains(text(),' Workwear')]")
		WebElement Workwear ;
		
		@FindBy(xpath="//mat-expansion-panel//span[contains(text(),' Polos/Knits ')]")
		WebElement polos ;

		@FindBy(xpath="//mat-expansion-panel//h4[contains(text(),'Price')]")
		WebElement Price ;
		
		@FindBy(xpath="//mat-expansion-panel//h4[contains(text(),'Color Group')]")
		WebElement ColorGroup ;
		
		@FindBy(xpath="//mat-expansion-panel//span[contains(text(),' BLUE')]")
		WebElement Blue ;
		
		@FindBy(xpath="//mat-expansion-panel//span[contains(text(),'Caps')]")
		WebElement Caps ;
		
		@FindBys({
		    @FindBy(xpath="//mat-expansion-panel//span[@class='mat-checkbox-label' and contains(text(),'$')]"), })
			List<WebElement>Pricesubfilter;
		
		    @FindBy(xpath="//div[@id='mat-menu-panel-1']")
			WebElement SortBydropdown;
	
		
	    @FindBy(xpath="//div[@class='filter-row text-muted ng-star-inserted']//strong")
		WebElement Productscount;
	    
	    @FindBy(xpath="//mat-expansion-panel//li//span[contains(text(),'Tops & Bottoms')]")
		WebElement TopsandBottoms;
	    
	    @FindBy(xpath="//mat-expansion-panel//li//span[contains(text(),'Easy Care')]")
		WebElement Easycare;
		
		@FindBys({
		    @FindBy(xpath="//div[@class='products-wrapper mt-0 accelerate ng-star-inserted']//div[@class='col accelerate ng-star-inserted']"), })
			List<WebElement>Products;
	
		@FindBys({
		    @FindBy(xpath="//div[@class='products-wrapper mt-0 accelerate ng-star-inserted']//div[@class='col accelerate ng-star-inserted']//a/div[2]/div/div/span"), })
			List<WebElement>Productsstyle;
		
		@FindBys({
		    @FindBy(xpath="//ngx-ecommerce-core-product-card/mat-card/a/div[2]/div/div/div/p"), })
			List<WebElement>Productprice;
	
		@FindBys({
		@FindBy(xpath="//mat-expansion-panel"), })
		List<WebElement> Filter;
		
		@FindBys({
			@FindBy(xpath="//div[@role='region']//ul//li//a"), })
			List<WebElement> Categoryfilter;
		
		@FindBys({
		@FindBy(xpath="//mat-expansion-panel//li"), })
		List<WebElement>  Subfilters ;
		
		@FindBys({
		@FindBy(xpath="//mat-expansion-panel//li//span"), })
		List<WebElement> Subfilterscount ;
	
		@FindBy(xpath="//a[contains(text(),'Clear All')]")
		WebElement clearall;
		
		@FindBys({
		    @FindBy(xpath="//ngx-ecommerce-core-product-card//mat-card"), })
			List<WebElement>Productss;
		
		  public ProductListingpage() {
			  PageFactory.initElements(driver, this);
		}
		  
		  public int[] count() throws InterruptedException {
			  String noofitems=Productscount.getText();
			  int no= Integer.parseInt(noofitems);
			 
			  //WebDriverWait wait = new WebDriverWait(driver,5);
		        JavascriptExecutor js = (JavascriptExecutor) driver;

		        int dataSize = driver.findElements(By.xpath("//div[@class='products-wrapper mt-0 accelerate ng-star-inserted']//div[@class='col accelerate ng-star-inserted']")).size();
		        while (true){
		            js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		          Thread.sleep(5000);
//		            wait.ignoring(NoSuchElementException.class)
//		            .until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//mat-spinner")));
		            
		            if (driver.findElements(By.xpath("//div[@class='products-wrapper mt-0 accelerate ng-star-inserted']//div[@class='col accelerate ng-star-inserted']")).size()== dataSize)
		               
		            	break;
		            dataSize = driver.findElements(By.xpath("//div[@class='products-wrapper mt-0 accelerate ng-star-inserted']//div[@class='col accelerate ng-star-inserted']")).size();
		        System.out.print(dataSize);
		        }
				return new int[] {dataSize,no}; 
		  }

		  public int[] totalitemscount() throws InterruptedException{
			  Productsearchfilter.click();
			  return count();
		  }
		  public String bydefaultsorting(){
			  Productsearchfilter.click();
			  String text=Sortby.getText();
			  return text;
	  
		  }
		  
		  public String Relevencetest(){
			  Productsearchfilter.click();
			  Sortby.click();
			  SortBydropdown.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
			  String text=Sortby.getText();
			  String price=Productprice.get(0).getText();
			  return text;
	  
		  }
		  
		  public String lowerstfirstsorttest(){
			  Productsearchfilter.click();
			  Sortby.click();
			  SortBydropdown.sendKeys(Keys.ARROW_DOWN,Keys.ARROW_DOWN,Keys.ENTER);
			  String text=Sortby.getText();
			  return text;
	  
		  }
		  public String highestfirstsorttest(){
			  Productsearchfilter.click();
			  Sortby.click();
			  SortBydropdown.sendKeys(Keys.ARROW_DOWN,Keys.ARROW_DOWN,Keys.ARROW_DOWN,Keys.ENTER);
			  String text=Sortby.getText();
			  return text;
	  
		  }
		  public void categoryfiltercounttest() throws InterruptedException{
			  Productsearchfilter.click();
			  System.out.println("a");
			  int dataSize=0,no = 0;
			  Thread.sleep(5000);
			  System.out.println("b");
			  System.out.println(Categoryfilter.size());
			 for(int i =0;i<Categoryfilter.size();i++) {
				  System.out.println("c");
				  Thread.sleep(5000);
				 Categoryfilter.get(i).click();
				 String categoryname=driver.findElements(By.xpath("//span[@class='mat-checkbox-label']")).get(i).getText();
				 System.out.println(categoryname);
				 String filtername=driver.findElement(By.xpath("//span[@class='fw-500 filter-item']")).getText();
				 System.out.println(filtername);
				 Assert.assertTrue(categoryname.contains(filtername));
				  
				  String noofitems=Productscount.getText();
				   no= Integer.parseInt(noofitems);
				 
				 // WebDriverWait wait = new WebDriverWait(driver, 5);
			        JavascriptExecutor js = (JavascriptExecutor) driver;

			         dataSize = driver.findElements(By.xpath("//div[@class='products-wrapper mt-0 accelerate ng-star-inserted']//div[@class='col accelerate ng-star-inserted']")).size();
			        while (true){
			            js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			          Thread.sleep(5000);
//			            wait.ignoring(NoSuchElementException.class)
//			            .until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//mat-spinner")));
			            
			            if (driver.findElements(By.xpath("//div[@class='products-wrapper mt-0 accelerate ng-star-inserted']//div[@class='col accelerate ng-star-inserted']")).size()== dataSize)
			               
			            	break;
			            dataSize = driver.findElements(By.xpath("//div[@class='products-wrapper mt-0 accelerate ng-star-inserted']//div[@class='col accelerate ng-star-inserted']")).size();
			        System.out.print(dataSize);
			        }
			        System.out.println("e");
			        Assert.assertEquals(dataSize, no);
			        clearall.click();
			
			        js.executeScript("scroll(0, -250);");

			 }
			 System.out.println("e");
		  }
		  
		
		  public String Accessoriesfiltertest(){
			  Productsearchfilter.click();
			  Accessories.click();
			  String url=driver.getCurrentUrl();
			  return url;
		  }
		  public int[] workwearfiltercounttest(){
			  Productsearchfilter.click();
			  Workwear.click();
			  String noofitems=Productscount.getText();
			  int no= Integer.parseInt(noofitems);
			 
			 // WebDriverWait wait = new WebDriverWait(driver, 5);
		        JavascriptExecutor js = (JavascriptExecutor) driver;

		        int dataSize = driver.findElements(By.xpath("//div[@class='products-wrapper mt-0 accelerate ng-star-inserted']//div[@class='col accelerate ng-star-inserted']")).size();
		        while (true){
		            js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		          try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
//		            wait.ignoring(NoSuchElementException.class)
//		            .until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//mat-spinner")));
//		            
		            if (driver.findElements(By.xpath("//div[@class='products-wrapper mt-0 accelerate ng-star-inserted']//div[@class='col accelerate ng-star-inserted']")).size()== dataSize)
		               
		            	break;
		            dataSize = driver.findElements(By.xpath("//div[@class='products-wrapper mt-0 accelerate ng-star-inserted']//div[@class='col accelerate ng-star-inserted']")).size();
		        System.out.print(dataSize);
		        }
			  
		        return new int[] {dataSize,no}; 
		  }
		
		  
		  public String polossubfiltertest() throws InterruptedException{
			  Productsearchfilter.click();
			  polos.click();
			  Thread.sleep(5000);
			  Easycare.click();
			   String url=driver.getCurrentUrl();
			   return url;
		  }
		  public String Tshirtsubfiltertest() throws InterruptedException{
			  Productsearchfilter.click();
			  	Tshirts.click();
			   Thread.sleep(5000);
			   TopsandBottoms.click();
			   Thread.sleep(5000);
			   String url=driver.getCurrentUrl();
			   return url;
		  }
		  
		  public int[] Colourgroupfiltercounttest() throws InterruptedException{
			  Productsearchfilter.click();
			  ColorGroup.click();
			  Blue.click();
			  System.out.println(Blue.getText());
			  String Bluecolur=Blue.getText();
			  String testString = Bluecolur;
			  int startIndex = testString.indexOf("(");
			  int endIndex = testString.indexOf(")");
			  String subString = testString.substring(startIndex+1, endIndex);
			  System.out.println(subString);
			  int count=Integer.parseInt(subString);

			 // WebDriverWait wait = new WebDriverWait(driver, 5);
		        JavascriptExecutor js = (JavascriptExecutor) driver;

		        int dataSize = driver.findElements(By.xpath("//div[@class='products-wrapper mt-0 accelerate ng-star-inserted']//div[@class='col accelerate ng-star-inserted']")).size();
		        while (true){
		            js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		          try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
//		            wait.ignoring(NoSuchElementException.class)
//		            .until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//mat-spinner")));
		            
		            if (driver.findElements(By.xpath("//div[@class='products-wrapper mt-0 accelerate ng-star-inserted']//div[@class='col accelerate ng-star-inserted']")).size()== dataSize)
		               
		            	break;
		            dataSize = driver.findElements(By.xpath("//div[@class='products-wrapper mt-0 accelerate ng-star-inserted']//div[@class='col accelerate ng-star-inserted']")).size();
		        System.out.print(dataSize);
		        }
		        return new int[] {dataSize,count}; 
		  }
		  
		  public String[] randomproductclick() throws InterruptedException {
			  Productsearchfilter.click();
			  
			  String style=Productsstyle.get(7).getText();
			  System.out.println(style);
			  String price=Productprice.get(7).getText();
			  System.out.println(price);

			 // Thread.sleep(5000);
			  Products.get(7).click();
			  return new String[] {style,price};
			  
			  
		  }
		  public void randomproductclicktest2() {
			  Productsearchfilter.click();
			  Products.get(getRandomInteger(0, 20)).click();
			  
		  }
		  
		  
		 
		  public void pricefiltertest(){
			  Productsearchfilter.click();
			  Price.click();
			  Pricesubfilter.get(1).click();
		  }
		  
		  public void productdetailpagetest() throws InterruptedException{
			  Productsearchfilter.click();
			  String noofitems=driver.findElement(By.xpath("//ngx-ecommerce-core-product-listing//div//mat-sidenav-container//mat-sidenav-content//div[2]//div[1]//span//strong")).getText();
			  System.out.println(noofitems);
			  int no= Integer.parseInt(noofitems);
			 
			  //WebDriverWait wait = new WebDriverWait(driver,5);
			  
			  for(int i=0;i<no;i++) {
				  
				 List<WebElement>  element = driver.findElements(By.xpath("//ngx-ecommerce-core-product-card//mat-card"));
				 
				  ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element.get(i));
				  System.out.println(i);
				  element.get(i).click();
				  Thread.sleep(5000);
				  driver.navigate().back();
				  Thread.sleep(10000); 
//
//				  
//
////					  driver.findElements(By.xpath("//ngx-ecommerce-core-product-card//mat-card")).get(i).click();
////					  Thread.sleep(5000);
////					  driver.navigate().back();
////					  Thread.sleep(5000); 
//				 
//				  
//				  
//				  
			  	}
//		        JavascriptExecutor js = (JavascriptExecutor) driver;
//		        
//		        int dataSize = driver.findElements(By.xpath("//div[@class='products-wrapper mt-0 accelerate ng-star-inserted']//div[@class='col accelerate ng-star-inserted']")).size();
//		        while (true){
//		            js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
//		          Thread.sleep(5000);
////		            wait.ignoring(NoSuchElementException.class)
////		            .until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//mat-spinner")));
//		            
//		            if (driver.findElements(By.xpath("//div[@class='products-wrapper mt-0 accelerate ng-star-inserted']//div[@class='col accelerate ng-star-inserted']")).size()== dataSize)
//		               
//		            	break;
		           
		        }
			  
		 // }
	
		 
}
