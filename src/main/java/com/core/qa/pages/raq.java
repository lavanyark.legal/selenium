package com.core.qa.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import com.core.qa.base.TestBase;
import com.paulhammant.ngwebdriver.NgWebDriver;

public class raq extends TestBase {
	
	Signinpage signinpage;

	
	@FindBy(xpath="//p//a[contains(text(),'Request A Quote')]")
	WebElement RequestAQuote;
	

	@FindBy(xpath="//input[@formcontrolname='product']")
	WebElement product;
	
	@FindBys({
        @FindBy(xpath="//mat-option"),
 })
	 List<WebElement>dropdown;
	
	@FindBy(xpath="//mat-select[@formcontrolname='color']")
	WebElement colur;
	
	@FindBy(xpath="//input[@formcontrolname='qty']")
	WebElement qty;
	
	@FindBy(xpath="//mat-select[@formcontrolname='location']")
	WebElement location;
	
	@FindBy(xpath="//mat-select[@placeholder='Artwork']")
	WebElement Artwork;
	
	@FindBy(xpath="//span[contains(text(),' Choose File ')]//parent::a")
	WebElement choosefile;
	
	@FindBy(xpath="//a[contains(text(),'Add another Design')]")
	WebElement addanotherdesign;
	
	@FindBy(xpath="//input[@formcontrolname='phone']")
	WebElement phone;
	
	@FindBy(xpath="//input[@formcontrolname='zip']")
	WebElement zip;
	
	@FindBy(xpath="//textarea[@formcontrolname='comments']")
	WebElement comments;
	
	@FindBy(xpath="//span[contains(text(),' Submit Quote Request ')]//parent::button")
	WebElement submitquotereq;
	
	@FindBy(xpath="//input[@formcontrolname='first_name']")
	WebElement firstname;
	
	@FindBy(xpath="//input[@formcontrolname='last_name']")
	WebElement lastname;
	
	@FindBy(xpath="//input[@formcontrolname='confirm_password']")
	WebElement confirmpassword;
	
	@FindBy(xpath="//input[@formcontrolname='text']")
	WebElement artworktext;
	
	@FindBys({
        @FindBy(xpath="//input[@formcontrolname='email']"),
 })
	 List<WebElement>Email;
	
	@FindBys({
        @FindBy(xpath="//input[@formcontrolname='password']"),
 })
	 List<WebElement>password;
	
	@FindBy(xpath="//span[contains(text(),' Sign In and Submit Quote Request ')]//parent::button")
	WebElement signinandsubmitquoterequest;
	
	@FindBy(xpath="//span[contains(text(),' Create Account and Submit Quote Request ')]//parent::button")
	WebElement  CreateAccountandSubmitQuoteRequest ;
	
	@FindBy(xpath="//*[@id=\"app\"]/ngx-ecommerce-core-layout/mat-sidenav-container/mat-sidenav-content/div/app-raq-thankyou/div/div/h2")
	WebElement Thankyoupage;
	
	@FindBy(xpath="//span[contains(text(),'Sign In and Submit Quote Request')]//parent::span//parent::button")
	WebElement signinandsubmitquoterequestbutton;
	
	@FindBys({
        @FindBy(xpath="//span[contains(text(),'Sign In and Submit Quote Request')]//parent::span//parent::button"),
 })
	 List<WebElement>signinandsubmitquoterequestbutton1;
	
	@FindBy(xpath="//span[contains(text(),'Submit Quote Request as Guest')]//parent::span//parent::button")
	WebElement  SubmitQuoteRequestasGuest ;
	
	@FindBy(xpath="//div[@class='decoration-popup-wrap']//span[contains(text(),'Submit Quote Request as Guest')]//parent::span//parent::button")
	WebElement  popupSubmitQuoteRequestasGuest ;
	
	public raq() {
		
		 PageFactory.initElements(driver, this);
	}
	
	public boolean Quickquotetest1() throws InterruptedException, AWTException {
		Robot robot=new Robot();
		 JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		RequestAQuote.click();
		
		product.sendKeys("500");
		dropdown.get(0).click();
		Thread.sleep(5000);
		colur.click();
		dropdown.get(0).click();
		qty.sendKeys(prop.getProperty("quantity"));
		location.click();
		dropdown.get(1).click();
		Thread.sleep(5000);
		 Artwork.click();
		 dropdown.get(0).click();
		 
		 Thread.sleep(1000);

		 choosefile.click();
		 robot.setAutoDelay(1000);
		 
		 StringSelection stringselection=new StringSelection(System.getProperty("user.dir")+"\\Testdatafolder\\US_Flag_4.DST");
		 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection,null);
		 robot.setAutoDelay(2000);
		 
		
		 
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_V);
		 
		 robot.setAutoDelay(2000);

		 robot.keyPress(KeyEvent.VK_ENTER);
		 robot.keyPress(KeyEvent.VK_ENTER);
		 Thread.sleep(1000);
		
		 phone.sendKeys(prop.getProperty("phone"));
		 zip.sendKeys(prop.getProperty("zip"));
		 comments.sendKeys(prop.getProperty("comments"));
		 Thread.sleep(5000);
		 signinandsubmitquoterequestbutton.click();
		 Email.get(1).sendKeys(prop.getProperty("Emailid"));
		 password.get(0).sendKeys(prop.getProperty("password"));
		 signinandsubmitquoterequestbutton.click();
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 return Thankyoupage.isDisplayed();

	}
	
	public boolean Quickquotetest2() throws InterruptedException, AWTException {
		Robot robot=new Robot();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		RequestAQuote.click();
		
		product.sendKeys("23");
		dropdown.get(4).click();
		Thread.sleep(5000);
		colur.click();
		dropdown.get(0).click();
		qty.sendKeys("1");
		location.click();
		dropdown.get(1).click();
		Thread.sleep(5000);
		 Artwork.click();
		 dropdown.get(1).click();
		 artworktext.sendKeys("FAYA");
		 robot.setAutoDelay(1000);
		 
		 phone.sendKeys("(123) 333-3333 x33333");
		 zip.sendKeys("91761");
		 comments.sendKeys("This is a test quote");
		 Thread.sleep(5000);
		 signinandsubmitquoterequestbutton.click();
		 firstname.sendKeys(prop.getProperty("firstname"));
		 lastname.sendKeys(prop.getProperty("lastname"));
		 Email.get(2).sendKeys(randomEmail());
		 password.get(1).sendKeys(prop.getProperty("password1"));
		 confirmpassword.sendKeys(prop.getProperty("confirmpassword"));
		 CreateAccountandSubmitQuoteRequest.click();
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 return Thankyoupage.isDisplayed();

	}
	public boolean Quickquotetest3() throws InterruptedException, AWTException {
		Robot robot=new Robot();
		Thread.sleep(5000);JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		RequestAQuote.click();
		
		product.sendKeys("44");
		dropdown.get(0).click();
		Thread.sleep(5000);
		colur.click();
		dropdown.get(0).click();
		qty.sendKeys("1");
		location.click();
		dropdown.get(1).click();
		Thread.sleep(5000);
		 Artwork.click();
		 dropdown.get(2).click();
		 phone.sendKeys("(123) 333-3333 x33333");
		 zip.sendKeys("91761");
		 comments.sendKeys("This is a test quote");
		 Thread.sleep(5000);
		 submitquotereq.click();
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 return Thankyoupage.isDisplayed();

	}
	public boolean Quickquotetest4() throws InterruptedException, AWTException {
		Robot robot=new Robot();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		RequestAQuote.click();
		
		product.sendKeys("12");
		dropdown.get(0).click();
		Thread.sleep(5000);
		colur.click();
		dropdown.get(0).click();
		qty.sendKeys("1");
		location.click();
		dropdown.get(1).click();
		Thread.sleep(5000);
		 Artwork.click();
		 dropdown.get(0).click();
		 
		 Thread.sleep(1000);

		 choosefile.click();
		 robot.setAutoDelay(1000);
		 
		 StringSelection stringselection=new StringSelection(System.getProperty("user.dir")+"\\Testdatafolder\\logo_e6d3db04afcf4e7aad52fd6961724789.png");
		 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection,null);
		 robot.setAutoDelay(2000);
		 
		
		 
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_V);
		 
		 robot.setAutoDelay(2000);

		 robot.keyPress(KeyEvent.VK_ENTER);
		 robot.keyPress(KeyEvent.VK_ENTER);
		 Thread.sleep(1000);
		
		 phone.sendKeys("(123) 333-3333 x33333");
		 zip.sendKeys("91761");
		 comments.sendKeys("This is a test quote");
		 Thread.sleep(5000);
		 SubmitQuoteRequestasGuest.click();
		 firstname.sendKeys("faya");
		 lastname.sendKeys("qa");
		 Email.get(1).sendKeys(prop.getProperty("Emailid"));
		 popupSubmitQuoteRequestasGuest.click();
		 driver.findElement(By.xpath("//h3[contains(text(),' This email address is already registered, please sign in or click on Go Back and use another email address ')]")).isDisplayed();
		 Email.get(1).sendKeys(prop.getProperty("Emailid"));
		 password.get(0).sendKeys(prop.getProperty("password"));
		 signinandsubmitquoterequestbutton1.get(1).click();
		
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 return Thankyoupage.isDisplayed();

	}
	
	public boolean Quickquotetest5() throws InterruptedException, AWTException {
		Robot robot=new Robot();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		RequestAQuote.click();
		
		product.sendKeys("500");
		
		dropdown.get(0).click();
		Thread.sleep(5000);
	
		colur.click();
		dropdown.get(0).click();
		qty.sendKeys("1");
		location.click();
		dropdown.get(1).click();
		Thread.sleep(5000);
		 Artwork.click();
		 dropdown.get(0).click();
		 
		 Thread.sleep(1000);

		 choosefile.click();
		 robot.setAutoDelay(1000);
		 
		 StringSelection stringselection=new StringSelection(System.getProperty("user.dir")+"\\Testdatafolder\\logo_e6d3db04afcf4e7aad52fd6961724789.png");
		 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection,null);
		 robot.setAutoDelay(2000);
		 
		
		 
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_V);
		 
		 robot.setAutoDelay(2000);

		 robot.keyPress(KeyEvent.VK_ENTER);
		 robot.keyPress(KeyEvent.VK_ENTER);
		 Thread.sleep(1000);
		 
		
		 phone.sendKeys(prop.getProperty("phone"));
		 zip.sendKeys(prop.getProperty("zip"));
		 comments.sendKeys(prop.getProperty("comments"));
		 Thread.sleep(5000);
		 SubmitQuoteRequestasGuest.click();
		 firstname.sendKeys("faya");
		 lastname.sendKeys("qa");
		 Email.get(1).sendKeys(randomEmail());
		 popupSubmitQuoteRequestasGuest.click();
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 return Thankyoupage.isDisplayed();

	}
	
}
