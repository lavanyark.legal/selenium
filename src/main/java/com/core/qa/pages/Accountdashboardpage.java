package com.core.qa.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.core.qa.base.TestBase;

public class Accountdashboardpage  extends TestBase{
	@FindBys({
	    @FindBy(xpath="//mat-icon//parent::span[@class='mat-button-wrapper']//parent::a"),})
		 List<WebElement>accountdropdown;
	
	@FindBy(xpath="//a[contains(text(),'Account Dashboard')]//parent::div")
	WebElement Accountdashboard;
	
	@FindBy(xpath="//div[@class='account-dashboard-wrap']")
	WebElement Accountdashboardtable;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'My Orders')]")
	WebElement MyOrders;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'My Quote Requests')]")
	WebElement MyQuoteRequests;
	
	@FindBy(xpath="//h2[contains(text(),'My Quote Requests')]")
	WebElement header;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'My Wishlist')]")
	WebElement MyWishlist;
	
	@FindBy(xpath="//h2[contains(text(),'My Wishlist')]")
	WebElement wishlist;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'My Saved Cards')]")
	WebElement mysavedcards;
	
	
	@FindBy(xpath="//span[contains(text(),' Add New Card')]//parent::button")
	WebElement addnewcard;
	
	@FindBy(xpath="//input[@formcontrolname='cardNumber']")
	WebElement cardnumber;
	
	@FindBy(xpath="//input[@formcontrolname='cardHolderName']")
	WebElement cardHolderName;
	
	@FindBy(xpath="//mat-select[@formcontrolname='expirationMonth']")
	WebElement expirationmonth;
	
	@FindBy(xpath="//span[contains(text(),'June')]")
	WebElement june;
	
	@FindBy(xpath="//mat-select[@formcontrolname='expirationYear']")
	WebElement expirationYear;
	
	@FindBy(xpath="//span[contains(text(),'2029')]")
	WebElement year;
	
	@FindBy(xpath="//input[@formcontrolname='cvv']")
	WebElement cvv;
	
	@FindBys({
        @FindBy(xpath="//mat-radio-group"),
 })
	 List<WebElement>address;
	
	@FindBy(xpath="//a[contains(text(),'+Add New Address')]")
	WebElement addnewaddress;
	
	@FindBy(xpath="//input[@formcontrolname='name']")
	WebElement name;
	
	@FindBy(xpath="//div[@class='save-card-popup ng-star-inserted']//input[@formcontrolname='email']")
	WebElement email;
	
	@FindBy(xpath="//input[@formcontrolname='company_name']")
	WebElement company_name;
	
	@FindBy(xpath="//input[@formcontrolname='address_1']")
	WebElement address_1;
	
	@FindBy(xpath="//input[@formcontrolname='city']")
	WebElement city;
	
	@FindBy(xpath="//mat-select[@formcontrolname='state']")
	WebElement state;
	
	@FindBy(xpath="//span[contains(text(),'Louisiana')]")
	WebElement  Louisiana ;
	
	@FindBy(xpath="//input[@formcontrolname='zip']")
	WebElement zip;
	
	@FindBy(xpath="//input[@formcontrolname='phone']")
	WebElement phone;
	
	@FindBy(xpath="//span[contains(text(),'Save')]//parent::button")
	WebElement save;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'Account Information')]")
	WebElement AccountInformation;
		
	@FindBy(xpath="//h2[contains(text(),'Account Information')]")
	WebElement accountinfo ;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'Address Book')]")
	WebElement AddressBook ;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'FAQ - Customer Order Process')]")
	WebElement faq ;
	
	@FindBy(xpath="//h1[contains(text(),'FAQ - Custom Embroidery Order')]")
	WebElement faqheader ;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'Log Out')]")
	WebElement Logout ;
	
	@FindBys({
	@FindBy(xpath="//input[@placeholder='Search']"),
	 })
	List<WebElement> Search ;
	
	@FindBys({
		@FindBy(xpath="//h2[contains(text(),'My Custom Orders')]//parent::div//parent::ngx-ecommerce-core-orders//tbody[@role='rowgroup']//td"),
		 })
	List<WebElement> customordernumber ;
	
	@FindBys({
		@FindBy(xpath="//ngx-ecommerce-core-orders/quote-request-table/div/table/tbody/tr//td"),
		 })
	List<WebElement> blankdernumber ;
	
	@FindBys({
		@FindBy(xpath="//mat-icon[contains(text(),'search')]//parent::button"),
		 })
	List<WebElement> searchbutton ;
	

	@FindBys({
		@FindBy(xpath="//div/ngx-ecommerce-core-orders/div[2]/form/div[2]/select/option"),
	 })
	List<WebElement> customstatusdropdown ;
	
	@FindBys({
		@FindBy(xpath="//div/ngx-ecommerce-core-orders/div[5]/form/div[2]/select/option"),
	 })
	List<WebElement> statusdropdown ;
	
	@FindBys({
		@FindBy(xpath="//ngx-ecommerce-core-orders//div[2]//form//div[2]//select//option"),
		 })
	List<WebElement> customstatus ;
	
	
	@FindBys({
		@FindBy(xpath="//option[@value='order__customer_orders']"),
		 })
	List<WebElement> orderprocessing ;
	
	@FindBys({
		@FindBy(xpath="//h2[contains(text(),'My Custom Orders')]//parent::div//parent::ngx-ecommerce-core-orders//div[@class='pct-quote-request ng-star-inserted']//tr"),
		 })
	List<WebElement> customrows ;
	@FindBys({
		@FindBy(xpath="//ngx-ecommerce-core-orders/quote-request-table/div/table/tbody/tr"),
		 })
	List<WebElement> blankrows ;
	@FindBys({
		@FindBy(xpath="//div[@class='mat-paginator-range-label']"),
		 })
	List<WebElement> page ;
	
	@FindBys({
		@FindBy(xpath="//mat-icon[@mattooltip='View']"),
		 })
	List<WebElement> view ;
	

		@FindBy(xpath="//ngx-ecommerce-core-orders/quote-request-table/div/table/tbody/tr//td//mat-icon[@mattooltip='View']")
	
	WebElement blankview ;
	
	@FindBy(xpath="//h2//strong")
	
		WebElement ordernum ;
	
	@FindBy(xpath="//div//strong[contains(text(),'Order Date:')]//parent::div//parent::div//h3")
	WebElement orderdate ;
	
	@FindBy(xpath="//div//strong[contains(text(),'Order Status:')]//parent::div//parent::div//h3")
	WebElement orderstatus ;
	
	@FindBys({
		@FindBy(xpath="//tr[@class='total-quantity']//strong"),
		 })
	List<WebElement> qty ;
	
	@FindBys({
		@FindBy(xpath="//strong[@class='font-xl ng-star-inserted']"),
		 })
	List<WebElement> grandtotal ;
	
	@FindBy(xpath="//td[@class='pt-2 text-right pr-2']")
	WebElement shippingamt ;
	
	@FindBy(xpath="//td[@class='text-right pr-2']")
	WebElement tax ;
	
	@FindBy(xpath="//p[contains(text(),'No custom orders have been placed.')]")
	WebElement nocustomorder ;
	
	public Accountdashboardpage() {
		
		 PageFactory.initElements(driver, this);
	}
	
	public boolean accountdashboardtest() throws InterruptedException {
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);
		MyOrders.click();
		Accountdashboard.click();
		
		return Accountdashboardtable.isDisplayed();
	}
	public void accountdashboardcustomtablesearchtest() throws InterruptedException {
		String subString;
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);
		MyOrders.click();
		Accountdashboard.click();
		if(customordernumber.size()!=0){
			String ordernum=customordernumber.get(1).getText();
			Search.get(0).sendKeys(ordernum);
			System.out.println(ordernum);
			Assert.assertTrue(customordernumber.get(1).isDisplayed());
		}
		else {
			nocustomorder.isDisplayed();
		}
		
		
	}
	public void accountdashboardcustomstatustest() throws InterruptedException {
		String subString;
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);
		MyOrders.click();
		Accountdashboard.click();
		Thread.sleep(10000);
		int size1=driver.findElements(By.xpath("//div/ngx-ecommerce-core-orders/div[2]/form/div[2]/select/option")).size();
		
		 driver.findElement(By.xpath("//*[@id=\"app\"]/ngx-ecommerce-core-layout/mat-sidenav-container/mat-sidenav-content/div[1]/ngx-ecommerce-core-account/mat-sidenav-container/mat-sidenav-content/ngx-ecommerce-core-dashboard/div/ngx-ecommerce-core-orders/div[3]/quote-request-table/div/mat-paginator/div/div/div[1]/mat-form-field/div/div[1]/div")).click();
		  driver.findElement(By.xpath("//span[contains(text(),'100')]")).click();
		  Thread.sleep(10000);
		for(int k=1;k<size1;k++) {
		customstatusdropdown.get(k).click();
		Thread.sleep(5000);
		String quotestatus=customstatusdropdown.get(k).getText();
		
		Thread.sleep(5000);
			if(customordernumber.size()>0){
				String pagen=page.get(0).getText();
				  String testString = pagen;
				  int startIndex = testString.indexOf("of ");
				  subString = testString.substring(startIndex+3);
				  System.out.println(subString);
				
				  Thread.sleep(10000);
				  int size=Integer.parseInt(subString)*8;
				
			for(int i=3;i<size;i+=8) {
					String stat=customordernumber.get(i).getText();
					System.out.println(stat);
					Assert.assertTrue(stat.contains(quotestatus));	
					
				}
				
					
			  }
				else {
					nocustomorder.isDisplayed();
				}			
			}		
		}

	public void accountdashboardcustomorderdetailstest() throws InterruptedException {
		String subString;
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);

		MyOrders.click();
		Accountdashboard.click();
		Thread.sleep(10000);
		String pagen=page.get(0).getText();
		  String testString = pagen;
		  int startIndex = testString.indexOf("of ");
		  subString = testString.substring(startIndex+3);
		  System.out.println(subString);
		  int size=Integer.parseInt(subString)*8;
		int size1=driver.findElements(By.xpath("//div/ngx-ecommerce-core-orders/div[2]/form/div[2]/select/option")).size();

		for(int k=1;k<size1;k++) {
			customstatusdropdown.get(k).click();
			Thread.sleep(5000);
			String quotestatus=customstatusdropdown.get(k).getText();
			
			Thread.sleep(5000);
				if(customordernumber.size()>0){
			String orderdates=customordernumber.get(0).getText();
			System.out.println(orderdates);
			String ordernumb=customordernumber.get(1).getText();
			System.out.println(ordernumb);
			String orderstat=customordernumber.get(3).getText();
			System.out.println(orderstat);
			String totalqty=customordernumber.get(5).getText();
			System.out.println(totalqty);
			String totalprice=customordernumber.get(6).getText();
			System.out.println(totalprice);
			view.get(0).click();	
			Thread.sleep(10000);
			//String date=orderdate.getText();
			//Assert.assertEquals(orderdates,date);
//			String onum=ordernum.getText();
//			String testString1 = onum;
//			int startIndex1 = testString1.indexOf("#");
//			subString = testString1.substring(startIndex1+1);
//			Assert.assertEquals(ordernumb,subString);
			Assert.assertEquals(orderstat,orderstatus.getText());
			Assert.assertEquals(totalqty,qty.get(0).getText());
			String totamt=qty.get(1).getText();
			int tmt=totamt.indexOf("$");
			String totalamount=totamt.substring(tmt+1);
			System.out.println(totalamount);
			String shipamt=shippingamt.getText();
			int ship=shipamt.indexOf("$");
			String shippingmethod=shipamt.substring(ship+1);
			System.out.println(shippingmethod);
			String taxamt=tax.getText();
			int taxx=taxamt.indexOf("$");
			String taxamtt=taxamt.substring(taxx+1);
			System.out.println(taxamtt);
			String grandtotal=String.format("%.2f", Float.parseFloat(totalamount)+Float.parseFloat(shippingmethod)+Float.parseFloat(taxamtt));
			
			System.out.println(grandtotal);
			Assert.assertEquals(totalprice,grandtotal);
			driver.navigate().back();
			Thread.sleep(10000);
				}
				else {
					nocustomorder.isDisplayed();
				}
		}
	}
	
	
	public void accountdashboardcustompaginationtest() throws InterruptedException {
		String subString;
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);
		MyOrders.click();
		Accountdashboard.click();
		int dropdownsize=customstatus.size();

		  for (int k=1;k<dropdownsize;k++) {	
			   Select statusdropdown1=new Select(customstatusdropdown.get(0));
			   statusdropdown1.selectByValue(customstatus.get(k).getAttribute("value"));
				String statuss=customstatus.get(k).getAttribute("value");
				System.out.println(statuss);
				Thread.sleep(5000);
				if(customordernumber.size()>0){
				String pagen=page.get(0).getText();
				String testString = pagen;
				int startIndex = testString.indexOf("of ");
				subString = testString.substring(startIndex+3);
				  System.out.println(subString);
				  int size=Integer.parseInt(subString);
					
				  driver.findElements(By.xpath("//mat-select[@role='listbox']")).get(0).click();
				  driver.findElement(By.xpath("//span[contains(text(),'100')]")).click();
				  Thread.sleep(5000);
				  int sizes=customrows.size();
				 sizes--;
				 Assert.assertEquals(size, size);
				
					
						
				  }
					else {
						nocustomorder.isDisplayed();
					}			
				}	
		
		

	}
	public void accountdashboardblanktablesearchtest() throws InterruptedException {
		String subString;
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		MyOrders.click();
		Accountdashboard.click();
		if(blankdernumber.size()!=0){
		String ordernum=blankdernumber.get(1).getText();
		Search.get(1).sendKeys(ordernum);
		System.out.println(ordernum);
		Assert.assertTrue(blankdernumber.get(1).isDisplayed());
		}
		else {
			nocustomorder.isDisplayed();
		}
	}
	public void accountdashboardblankstatustest() throws InterruptedException {
		String subString;
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);
		MyOrders.click();
		Accountdashboard.click();
		Thread.sleep(5000);
		int dropdownsize=customstatus.size();
		 driver.findElements(By.xpath("//mat-select[@role='listbox']")).get(1).click();
		  Thread.sleep(5000);
		  driver.findElement(By.xpath("//span[contains(text(),'100')]")).click();
		  for (int k=0;k<dropdownsize;k++) {	
		   Select statusdropdown1=new Select(statusdropdown.get(1));
		   statusdropdown1.selectByValue(customstatus.get(k).getAttribute("value"));
			String statuss=customstatus.get(k).getAttribute("value");
			System.out.println(statuss);
			Thread.sleep(5000);
			if(customordernumber.size()>0){
			String pagen=page.get(0).getText();
			String testString = pagen;
			int startIndex = testString.indexOf("of ");
			subString = testString.substring(startIndex+3);
			  System.out.println(subString);
			  int size=Integer.parseInt(subString)*8;		
			for(int i=3;i<size;i+=8) {
					String stat=customordernumber.get(i).getText();
					System.out.println(stat);
					SoftAssert softAssertion= new SoftAssert();
					softAssertion.assertTrue(stat.contains(statuss));	
				}
			  }
				else {
					nocustomorder.isDisplayed();
				}			
			}		
	}
	public void accountdashboardblankorderdetailstest() throws InterruptedException {
		String subString;
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);
		MyOrders.click();
		Accountdashboard.click();
		Thread.sleep(10000);
		String pagen=page.get(1).getText();
		  String testString = pagen;
		  int startIndex = testString.indexOf("of ");
		  subString = testString.substring(startIndex+3);
		  System.out.println(subString);
		 
		  int size=Integer.parseInt(subString)*8;
		
			String orderdates=blankdernumber.get(0).getText();
			System.out.println(orderdates);
			String ordernumb=blankdernumber.get(1).getText();
			System.out.println(ordernumb);
			String orderstat=blankdernumber.get(3).getText();
			System.out.println(orderstat);
			String totalqty=blankdernumber.get(5).getText();
			System.out.println(totalqty);
			String totalprice=blankdernumber.get(6).getText();
			System.out.println(totalprice);
			blankview.click();
			SoftAssert softAssertion= new SoftAssert();
			String date=orderdate.getText();
			softAssertion.assertEquals(orderdates,date);
			softAssertion.assertEquals(ordernumb,ordernum.getText());
			softAssertion.assertEquals(orderstat,orderstatus.getText());
			softAssertion.assertEquals(totalqty,qty.get(0).getText());
			softAssertion.assertEquals(totalprice,qty.get(1).getText());

	}
	public void accountdashboardblankpaginationtest() throws InterruptedException {
		String subString;
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);
		MyOrders.click();
		Accountdashboard.click();
		Thread.sleep(10000);
		String pagen=page.get(1).getText();
		  String testString = pagen;
		  int startIndex = testString.indexOf("of ");
		  subString = testString.substring(startIndex+3);
		  System.out.println(subString);
		  int sizee=Integer.valueOf(subString);
		  Thread.sleep(10000);
		  driver.findElements(By.xpath("//mat-select[@role='listbox']")).get(1).click();
		  driver.findElement(By.xpath("//span[contains(text(),'100')]")).click();
		  Thread.sleep(5000);
		 int size=blankrows.size();
		 Assert.assertEquals(size, sizee);
		

	}
	

}
