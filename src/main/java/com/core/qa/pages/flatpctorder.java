package com.core.qa.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.core.qa.base.TestBase;
import com.paulhammant.ngwebdriver.NgWebDriver;


public class flatpctorder extends TestBase {
	
	GuestAddtocartBlank productdetailpage;
	
	@FindBy(xpath="//span[contains(text(),'With Flat Embroidery')]//parent::div")
	WebElement withflatembroidrey;
	
	@FindBy(xpath="//span[contains(text(),'With 3D Embroidery')]//parent::div")
	WebElement threedembroidrey;
	
	@FindBy(xpath="//span[contains(text(),'Start Designing Now ')]//parent::button")
	WebElement startdesigning;
	
	@FindBy(xpath="//a[contains(text(),'Add Text')]//parent::li")
	WebElement addtext;
	
	@FindBy(xpath="//textarea")
	WebElement text;
	
	@FindBy(xpath="//span[contains(text(),' Add to Design ')]//parent::button")
	WebElement addtodesign;
	
	@FindBy(xpath="//a[contains(text(),'Clipart Library')]//parent::li")
	WebElement addclipart;
	
	@FindBys({
        @FindBy(xpath="//cdk-virtual-scroll-viewport//div//div[@class='preview-wrap d-inline-block ng-star-inserted']"),
 })
	 List<WebElement>clipartlib;
	
	@FindBys({
        @FindBy(xpath="//mat-card-content//li"),
 })
	 List<WebElement>clipartlib1;
	
	
	@FindBy(xpath="//a[contains(text(),'Back')]")
	WebElement back;
	
	@FindBy(xpath="//a[contains(text(),'Upload Picture')]//parent::li")
	WebElement uploadpic;
	
	@FindBy(xpath="//div[@class='dropzone dz-single dz-clickable']")
	WebElement upload;
	
	@FindBy(xpath="//span[contains(text(),'Finish Select Color')]//parent::button")
	WebElement finishselectcolour;
	
	@FindBy(xpath="//span[contains(text(),' Continue to Place Order ')]//parent::button")
	WebElement continuetoplaceorder;
	
	@FindBy(xpath="//span[contains(text(),'Sign In and Continue')]//parent::button")
	WebElement signinandplaceorder;
	
	@FindBy(xpath="//textarea[@placeholder='Enter a location']")
	WebElement address;
	
	@FindBy(xpath="//span[contains(text(),' Get Estimated Delivery Date and Shipping Cost ')]//parent::button")
	WebElement estimatebutton;
	
	@FindBys({
        @FindBy(xpath="//span[contains(text(),'Checkout')]//parent::button"),
 })
	 List<WebElement>proceedtocheckout;
	

	@FindBy(xpath="//span[contains(text(),' Continue to Submit Quote Request ')]//parent::button")
	WebElement submitquotebutton;
	

     @FindBy(xpath="//input[@formcontrolname='email']")
	 WebElement Email;
	
	@FindBy(xpath="//input[@formcontrolname='password']")
	 WebElement password;
	
	@FindBy(xpath="//span[contains(text(),'Sign In and Submit Quote Request')]//parent::span//parent::button")
	WebElement signinandsubmitquoterequestbutton;
	
	@FindBys({
        @FindBy(xpath="//div[@class='product-detail']//input[@type='number']"),
 })
	 List<WebElement>quantity;
	
	
	@FindBy(xpath="//span[contains(text(),' Continue to Submit Quote Request ')]//parent::span//parent::button")
	WebElement continuetosubmit;

	
	
	@FindBy(xpath="//input[@formcontrolname='zip']")
	WebElement zip;
	
	@FindBy(xpath="//span[contains(text(),' Submit Quote Request ')]//parent::span//parent::button")
	WebElement submitquote;
	
	@FindBy(xpath="//span[contains(text(),' Create Your Account ')]")
	WebElement createaccount;
	
	@FindBy(xpath="//span[contains(text(),' Create New Account And Continue ')]//parent::button")
	WebElement createnewaccountandplaceorder;
	
	@FindBy(xpath="//input[@formcontrolname='first_name']")
	WebElement firstname;
	
	@FindBy(xpath="//input[@formcontrolname='last_name']")
	WebElement lastname;
	
	@FindBy(xpath="//input[@formcontrolname='confirm_password']")
	WebElement confirmpassword;
	
	@FindBy(linkText="CREATE YOUR OWN")
	WebElement createyourown;
	
	@FindBys({
        @FindBy(xpath="//div[@id='product-list-container']//mat-card"),
 })
	 List<WebElement>products;
	@FindBys({
        @FindBy(xpath="//ngx-ecommerce-core-product-card//mat-card//a//div[2]//div//div"),
 })
	 List<WebElement>productsstyle;
	
	@FindBys({
        @FindBy(xpath="//div[@class='tile']//img"),
 })
	 List<WebElement>productssize;
	
	@FindBys({
        @FindBy(xpath="//div[@class='preview-wrap d-inline-block ng-star-inserted']//img"),
 })
	 List<WebElement>colourswatch;
	
	@FindBy(xpath="//span[contains(text(),' Select Product and Start Design Now ')]//parent::button")
	WebElement selectproductanddesignnow;
	
	@FindBy(xpath="//span[contains(text(),' ADD PRODUCTS ')]//parent::button")
	WebElement addproducts;
	
	@FindBy(xpath="//mat-card//span[contains(text(),' Add Product ')]//parent::button")
	WebElement addproduct;
	
	@FindBys({
	@FindBy(xpath="//div[@class='pct-add-product']//p//span")
	 })
	List<WebElement> stylee;
	
	
	@FindBy(xpath="//img[@alt='Thankyou']")
	WebElement Thankyoupage;
	
	@FindBys({
        @FindBy(xpath="//span[contains(text(),' Place Order')]//parent::button"),
 })
	 List<WebElement>placeorder;
	
	@FindBy(xpath="//span[contains(text(),'Continue')]//parent::button")
	WebElement continuee;
	
	@FindBy(xpath="//span[contains(text(),' Add To Cart ')]//parent::button")
	WebElement Addtocart;
	
	
	@FindBy(xpath="//textarea[@placeholder='Enter a location']")
	WebElement addresss;
	

	
	@FindBy(xpath="//input[@formcontrolname='phone']")
	WebElement phone;
	
	@FindBys({
	@FindBy(xpath="//span[contains(text(),' Use this Shipping Address ')]//parent::button"),
	 })
	List<WebElement> usethisshippingaddress;
	
	@FindBys({
		@FindBy(xpath="//input[@type='checkbox']//parent::span"),
		 })
		List<WebElement> chckbox;
	
	public flatpctorder() {
		
		 PageFactory.initElements(driver, this);
	}
	
	public boolean pctordertest1() throws InterruptedException, AWTException {
		Robot robot=new Robot();
		createyourown.click();
		products.get(1).click();
		colourswatch.get(0).click();
		selectproductanddesignnow.click();
		// Thread.sleep(10000);
		 uploadpic.click();
		// Thread.sleep(5000);
		 upload.click();
		// Thread.sleep(5000);
		 robot.setAutoDelay(1000);
		 
		 StringSelection stringselection=new StringSelection(System.getProperty("user.dir")+"\\Testdatafolder\\US_Flag_4.DST");
		 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection,null);
		 robot.setAutoDelay(2000);
		 
		
		 
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_V);
		 
		 robot.setAutoDelay(2000);

		 robot.keyPress(KeyEvent.VK_ENTER);
		 robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(1000);
		 finishselectcolour.click();
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 JavascriptExecutor js = (JavascriptExecutor) driver;
		 js.executeScript("window.scrollBy(0,250)");
		 Thread.sleep(5000);
		 continuee.click();
		 int Sum=0;
		 Random rand = new Random();
		 int j=0;
		 Thread.sleep(5000);
		// Addanothercolurqty.click();
		 int i;
		 for(i=0;i<quantity.size();i++) {
			 quantity.get(i).clear(); 
			 quantity.get(i).sendKeys(String.valueOf(getRandomInteger(1, 1)));
			
		 }	
		 continuee.click();
		// Thread.sleep(5000);
		 Addtocart.click(); 
		 Thread.sleep(2000);
		// proceedtocheck.click();
		 Email.sendKeys(prop.getProperty("Emailid"));
		 password.sendKeys(prop.getProperty("password"));
		 Thread.sleep(10000);
		 signinandplaceorder.click();
		 Thread.sleep(20000);
		 proceedtocheckout.get(0).click();
		// driver.switchTo().alert().accept();
		 Thread.sleep(5000);
		 //phone.sendKeys("(111) 111-1111 x11111");
		
		 //usethisshippingaddress.get(0).click();
		 robot.keyRelease(KeyEvent.VK_CONTROL);
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 productdetailpage=new GuestAddtocartBlank();
		
		Thread.sleep(5000);
		 productdetailpage.Deliverymethodconftest();
		// Thread.sleep(5000);
		 driver.findElement(By.xpath("//mat-radio-button[@class='mat-radio-button credit-item-wrap paypal mat-accent ng-star-inserted']")).click();
			
			//productdetailpage.Deliverymethodconftest();
		// chckbox.get(0).click();
		// placeorder.get(0).click();
//		 jsDriver = (JavascriptExecutor) driver;
//		 ngWebDriver = new NgWebDriver(jsDriver);
//		 try {
//			 ngWebDriver.waitForAngularRequestsToFinish();
//
//		 }
//		 catch (Exception e) {
//
//		}
		return true;
	}

	public boolean pctordertest2(String fname,String email,String companyname,String address,String city,String state,String zipcode,String phonenumber) throws InterruptedException, AWTException {
		Robot robot=new Robot();
		createyourown.click();
		products.get(2).click();
		colourswatch.get(1).click();
		selectproductanddesignnow.click();
		Thread.sleep(10000);
		 uploadpic.click();
		 Thread.sleep(5000);
		 upload.click();
		 Thread.sleep(5000);
		 robot.setAutoDelay(1000);
		 
		 StringSelection stringselection=new StringSelection(System.getProperty("user.dir")+"\\Testdatafolder\\US_Flag_4.DST");
		 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection,null);
		 robot.setAutoDelay(2000);
		 
		
		 
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_V);
		 
		 robot.setAutoDelay(2000);

		 robot.keyPress(KeyEvent.VK_ENTER);
		 robot.keyPress(KeyEvent.VK_ENTER);
		 Thread.sleep(1000);
		 finishselectcolour.click();
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}

		 continuee.click();
		 Thread.sleep(5000);

		 int Sum=0;
		 Random rand = new Random();
		 int j=0;
		 Thread.sleep(5000);
		// Addanothercolurqty.click();
		 int i;
		 for(i=0;i<quantity.size();i++) {
			 quantity.get(i).clear(); 
			 quantity.get(i).sendKeys(String.valueOf(getRandomInteger(1, 1)));
			
		 }

		 continuee.click();
		
		 addresss.sendKeys("2330 South Archibald Avenue, Ontario, CA, USA");
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_DOWN);
		 robot.keyPress(KeyEvent.VK_ENTER);
		 Thread.sleep(5000);
		 estimatebutton.click();
		 Thread.sleep(10000);
		 robot.keyPress(KeyEvent.VK_CONTROL);

		 proceedtocheckout.get(0).click();
		 Thread.sleep(5000);

		 createaccount.click();
		 firstname.sendKeys(prop.getProperty("firstname"));
		 lastname.sendKeys(prop.getProperty("lastname"));
		 Email.sendKeys(randomEmail());
		 password.sendKeys(prop.getProperty("password1"));
		 confirmpassword.sendKeys(prop.getProperty("confirmpassword"));
		 createnewaccountandplaceorder.click();

		 Thread.sleep(10000);
		 driver.switchTo().alert().accept();

		 
		 
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		productdetailpage=new GuestAddtocartBlank();
		productdetailpage.Shippingaddress(fname, email, companyname, address, city, state, zipcode, phonenumber);
		 float[] price=	productdetailpage.checkoutprice();
		 float price1=price[0];
		 float price2=price[1];
		 Assert.assertEquals(price1, price2);
		productdetailpage.paymenttest1(); 
		 Thread.sleep(5000);
		 chckbox.get(0).click();

		placeorder.get(0).click();
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		
		 return Thankyoupage.isDisplayed();

	}

	public boolean pctordertest3() throws InterruptedException, AWTException {
		Robot robot=new Robot();
		createyourown.click();
		products.get(2).click();
		colourswatch.get(1).click();
		selectproductanddesignnow.click();
		Thread.sleep(10000);
		 uploadpic.click();
		 Thread.sleep(5000);
		 upload.click();
		 Thread.sleep(5000);
		 robot.setAutoDelay(1000);
		 
		 StringSelection stringselection=new StringSelection(System.getProperty("user.dir")+"\\Testdatafolder\\US_Flag_4.DST");
		 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection,null);
		 robot.setAutoDelay(2000);
		 
		
		 
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_V);
		 
		 robot.setAutoDelay(2000);

		 robot.keyPress(KeyEvent.VK_ENTER);
		 robot.keyPress(KeyEvent.VK_ENTER);
		 Thread.sleep(1000);
		 finishselectcolour.click();
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 
		 continuee.click();
		 Thread.sleep(5000);

		 int Sum=0;
		 Random rand = new Random();
		 int j=0;
		 Thread.sleep(5000);
		// Addanothercolurqty.click();
		 int i;
		 for(i=0;i<quantity.size();i++) {
			 quantity.get(i).clear(); 
			 quantity.get(i).sendKeys(String.valueOf(getRandomInteger(1, 1)));
			
		 }
		 robot.keyPress(KeyEvent.VK_CONTROL);

		 continuee.click();
		
		 addresss.sendKeys("2330 South Archibald Avenue, Ontario, CA, USA");
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_DOWN);
		 robot.keyPress(KeyEvent.VK_ENTER);
		 Thread.sleep(5000);
		 estimatebutton.click();
		 Thread.sleep(10000);
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_CONTROL);

		 proceedtocheckout.get(0).click();
		 Thread.sleep(5000);
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		productdetailpage=new GuestAddtocartBlank();
		phone.sendKeys("(222) 222-2222 x22222");
		usethisshippingaddress.get(0).click();
		 float[] price=	productdetailpage.checkoutprice();
		 float price1=price[0];
		 float price2=price[1];
		 Assert.assertEquals(price1, price2);
		 Thread.sleep(5000);
		 chckbox.get(0).click();

		 placeorder.get(0).click();
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 return Thankyoupage.isDisplayed();
	}
	
	public boolean Quickquotetest5(String FullName, String email, String Companyname, String Streetaddress, String city, String state, String zip, String phonenumber) throws InterruptedException, AWTException {
		Robot robot=new Robot();
		createyourown.click();
		products.get(2).click();
		colourswatch.get(1).click();
		selectproductanddesignnow.click();
		 Thread.sleep(10000);
		 uploadpic.click();
		 Thread.sleep(5000);
		 upload.click();
		 Thread.sleep(5000);
		 robot.setAutoDelay(1000);
		 
		 StringSelection stringselection=new StringSelection(System.getProperty("user.dir")+"\\Testdatafolder\\US_Flag_4.DST");
		 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection,null);
		 robot.setAutoDelay(2000);
		 
		
		 
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_V);
		 
		 robot.setAutoDelay(2000);

		 robot.keyPress(KeyEvent.VK_ENTER);
		 robot.keyPress(KeyEvent.VK_ENTER);
		 Thread.sleep(1000);
		 finishselectcolour.click();
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 continuee.click();
		 Thread.sleep(5000);

		 int i;
		 for(i=0;i<quantity.size();i++) {
			 quantity.get(i).clear(); 
			 quantity.get(i).sendKeys(String.valueOf(getRandomInteger(1, 1)));
			
		 }
		 continuee.click();
		
		 address.sendKeys("2330 South Archibald Avenue, Ontario, CA, USA");
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_DOWN);
		 robot.keyPress(KeyEvent.VK_ENTER);
		 Thread.sleep(5000);
		 estimatebutton.click();
		 Thread.sleep(10000);
		 proceedtocheckout.get(0).click();
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_CONTROL);

		 Email.sendKeys(prop.getProperty("Emailid"));
		 password.sendKeys(prop.getProperty("password"));
		 Thread.sleep(10000);

		 signinandplaceorder.click();

		 Thread.sleep(5000);
		 driver.switchTo().alert().accept();

		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		
		 robot.keyPress(KeyEvent.VK_CONTROL);

		 phone.sendKeys("(222) 222-2222 x22222");
		 usethisshippingaddress.get(0).click();
		 productdetailpage=new GuestAddtocartBlank();
		 productdetailpage.newshippingaddressadd(FullName, email, Companyname, Streetaddress, city, state, zip, phonenumber);
		 Thread.sleep(7000);
		 productdetailpage.newcardadd();
		 Thread.sleep(7000);
		 productdetailpage.newbillingaddressadd();
		 Thread.sleep(7000);
		 int[]qty= productdetailpage.qtytestcheckout();
		 int qty1=qty[0];
		 int qty2=qty[1];
		 Assert.assertEquals(qty1, qty2); 
		 float[] price=	productdetailpage.checkoutprice();
		 float price1=price[0];
		 float price2=price[1];
		 Assert.assertEquals(price1, price2);
		// productdetailpage.placeorderbuttonclick();
		 chckbox.get(0).click();

		 placeorder.get(0).click();
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 return Thankyoupage.isDisplayed();
	}
	public boolean Quickquotetest6() throws InterruptedException, AWTException {
		Robot robot=new Robot();
		createyourown.click();
		products.get(2).click();
		colourswatch.get(1).click();
		selectproductanddesignnow.click();
		 Thread.sleep(10000);
		 uploadpic.click();
		 Thread.sleep(5000);
		 upload.click();
		 Thread.sleep(5000);
		 robot.setAutoDelay(1000);
		 
		 StringSelection stringselection=new StringSelection(System.getProperty("user.dir")+"\\Testdatafolder\\US_Flag_4.DST");
		 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection,null);
		 robot.setAutoDelay(2000);
		 
		
		 
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_V);
		 
		 robot.setAutoDelay(2000);

		 robot.keyPress(KeyEvent.VK_ENTER);
		 robot.keyPress(KeyEvent.VK_ENTER);
		 Thread.sleep(1000);
		 finishselectcolour.click();
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 continuee.click();
		 Thread.sleep(5000);
		 int Sum=0;
		 Random rand = new Random();
		 int j=0;
		 Thread.sleep(5000);
		// Addanothercolurqty.click();
		 int i;
		 for(i=0;i<quantity.size();i++) {
			 quantity.get(i).clear(); 
			 quantity.get(i).sendKeys(String.valueOf(getRandomInteger(1, 1)));
			
		 }

		 continuee.click();
		
		 address.sendKeys("2330 South Archibald Avenue, Ontario, CA, USA");
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_DOWN);
		 robot.keyPress(KeyEvent.VK_ENTER);
		 Thread.sleep(5000);
		 estimatebutton.click();
		 Thread.sleep(10000);
		 proceedtocheckout.get(0).click();
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_CONTROL);

		 Email.sendKeys(prop.getProperty("Emailid"));
		 password.sendKeys(prop.getProperty("password"));
		 Thread.sleep(10000);

		 signinandplaceorder.click();
		

		 Thread.sleep(10000);

		 driver.switchTo().alert().accept();

		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 phone.sendKeys("(222) 222-2222 x22222");
		 usethisshippingaddress.get(0).click();
		 productdetailpage=new GuestAddtocartBlank();

		 productdetailpage.shippingaddresschange();
		 Thread.sleep(5000);
		 productdetailpage.paymentmethodchange();
		 Thread.sleep(5000);
		 productdetailpage.billingaddresschange();
		 Thread.sleep(5000);
		 int[]qty= productdetailpage.qtytestcheckout();
		 int qty1=qty[0];
		 int qty2=qty[1];
		 Assert.assertEquals(qty1, qty2); 
		 float[] price=	productdetailpage.checkoutprice();
		 float price1=price[0];
		 float price2=price[1];
		 Assert.assertEquals(price1, price2);
		 chckbox.get(0).click();

		 placeorder.get(0).click();
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 return Thankyoupage.isDisplayed();
	}
}
	