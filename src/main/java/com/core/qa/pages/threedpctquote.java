package com.core.qa.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.core.qa.base.TestBase;
import com.paulhammant.ngwebdriver.NgWebDriver;


public class threedpctquote extends TestBase {
	@FindBy(xpath="//span[contains(text(),'With Flat Embroidery')]//parent::div")
	WebElement withflatembroidrey;
	
	@FindBy(xpath="//span[contains(text(),'With 3D Embroidery')]//parent::div")
	WebElement threedembroidrey;
	
	@FindBy(xpath="//span[contains(text(),'Start Designing Now ')]//parent::button")
	WebElement startdesigning;
	
	@FindBy(xpath="//a[contains(text(),'Add Text')]//parent::li")
	WebElement addtext;
	
	@FindBy(xpath="//textarea")
	WebElement text;
	
	@FindBy(xpath="//span[contains(text(),' Add to Design ')]//parent::button")
	WebElement addtodesign;
	
	@FindBy(xpath="//a[contains(text(),'Clipart Library')]//parent::li")
	WebElement addclipart;
	
	@FindBys({
        @FindBy(xpath="//cdk-virtual-scroll-viewport//img[2]"),
 })
	 List<WebElement>clipartlib;
	
     @FindBy(xpath="//div[@class='art-con ng-star-inserted']//a")
     WebElement clipartlibrary;
	
	@FindBys({
        @FindBy(xpath="//mat-card-content//li"),
 })
	 List<WebElement>clipartlib1;
	
	
	@FindBy(xpath="//a[contains(text(),'Back')]")
	WebElement back;
	
	@FindBy(xpath="//a[contains(text(),'Upload Picture')]//parent::li")
	WebElement uploadpic;
	
	@FindBy(xpath="//div[@class='dropzone dz-single dz-clickable']")
	WebElement upload;
	
	@FindBy(xpath="//span[contains(text(),'Finish Select Color')]//parent::button")
	WebElement finishselectcolour;
	
	@FindBy(xpath="//span[contains(text(),' Continue to Place Order ')]//parent::button")
	WebElement continuetoplaceorder;
	
	@FindBy(xpath="//span[contains(text(),' Sign In and Place Order ')]//parent::button")
	WebElement signinandplaceorder;
	
	@FindBy(xpath="//textarea[@placeholder='Enter a location']")
	WebElement address;
	
	@FindBy(xpath="//span[contains(text(),' Get Estimated Delivery Date and Shipping Cost ')]//parent::button")
	WebElement estimatebutton;
	
	@FindBy(xpath="//span[contains(text(),' Proceed to Checkout')]//parent::button")
	WebElement proceedtocheck;
	
	@FindBy(xpath="//span[contains(text(),'Done')]//parent::button")
	WebElement Done;

	@FindBy(xpath="//span[contains(text(),' Continue to Submit Quote Request ')]//parent::button")
	WebElement submitquotebutton;
	
	@FindBys({
        @FindBy(xpath="//input[@formcontrolname='email']"),
 })
	 List<WebElement>Email;
	
	
        @FindBy(xpath="//input[@formcontrolname='email']")
        WebElement Emails;
	
	@FindBys({
        @FindBy(xpath="//input[@formcontrolname='password']"),
 })
	 List<WebElement>password;
	

        @FindBy(xpath="//span[contains(text(),'Sign In and Submit Quote Request')]//parent::span//parent::button")

	 WebElement signinandsubmitquoterequestbutton1;
	
	@FindBy(xpath="//span[contains(text(),'Sign In and Submit Quote Request')]//parent::span//parent::button")
	WebElement signinandsubmitquoterequestbutton;
	
	@FindBys({
        @FindBy(xpath="//div[@class='product-detail']//input[@type='number']"),
 })
	 List<WebElement>quantity;
	
	
	@FindBy(xpath="//span[contains(text(),' Continue to Submit Quote Request ')]//parent::span//parent::button")
	WebElement continuetosubmit;

	
	@FindBy(xpath="//input[@formcontrolname='phone']")
	WebElement phone;
	
	@FindBy(xpath="//input[@formcontrolname='zip']")
	WebElement zip;
	
	@FindBy(xpath="//span[contains(text(),'Submit Quote Request')]//parent::span//parent::button")
	WebElement submitquote;
	
	@FindBy(xpath="//span[contains(text(),' Create Your Account ')]")
	WebElement createaccount;
	
	@FindBy(xpath="//span[contains(text(),' Create New Account And Submit Quote Request ')]//parent::button")
	WebElement createnewaccountandsubmitquote;
	
	@FindBy(xpath="//input[@formcontrolname='first_name']")
	WebElement firstname;
	
	@FindBy(xpath="//input[@formcontrolname='last_name']")
	WebElement lastname;
	
	@FindBy(xpath="//input[@formcontrolname='confirm_password']")
	WebElement confirmpassword;
	
	@FindBy(linkText="CREATE YOUR OWN")
	WebElement createyourown;
	
	@FindBys({
        @FindBy(xpath="//ngx-ecommerce-core-product-card//mat-card//a//div[1]//a"),
 })
	 List<WebElement>products;
	
	@FindBys({
        @FindBy(xpath="//ngx-ecommerce-core-product-card//mat-card//a//div[2]//div//div"),
 })
	 List<WebElement>productsstyle;
	
	@FindBys({
        @FindBy(xpath="//div[@class='tile']//img"),
 })
	 List<WebElement>productssize;
	
	@FindBys({
        @FindBy(xpath="//ngx-ecommerce-core-product-card//mat-card//div//span"),
 })
	 List<WebElement>productsname;
	
	@FindBys({
        @FindBy(xpath="//div[@class='preview-wrap d-inline-block ng-star-inserted']//img"),
 })
	 List<WebElement>colourswatch;
	
	@FindBy(xpath="//app-product-selector-detail//div//div[1]//div[2]//div[2]//p[2]")
	WebElement colourname;
	
	@FindBy(xpath="//span[contains(text(),' Select Product and Start Design Now ')]//parent::button")
	WebElement selectproductanddesignnow;
	
	@FindBy(xpath="//span[contains(text(),' ADD PRODUCTS ')]//parent::button")
	WebElement addproducts;
	
	@FindBy(xpath="//mat-card//span[contains(text(),' Add Product ')]//parent::button")
	WebElement addproduct;
	
	@FindBys({
	@FindBy(xpath="//div[@class='pct-add-product']//p//span")
	 })
	List<WebElement> stylee;
	
	
	@FindBy(xpath="//*[@id=\"app\"]/ngx-ecommerce-core-layout/mat-sidenav-container/mat-sidenav-content/div/app-raq-thankyou/div/div/h2")
	WebElement Thankyoupage;
	
	@FindBys({
        @FindBy(xpath="//span[contains(text(),' Place Order')]//parent::button"),
 })
	 List<WebElement>placeorder;
	
	@FindBy(xpath="//span[contains(text(),' Continue ')]//parent::span//parent::button")
	WebElement continuee;
	
	@FindBy(xpath="//span[contains(text(),' Create Account and Submit Quote Request ')]//parent::button")
	WebElement  CreateAccountandSubmitQuoteRequest ;
	
	@FindBy(xpath="//span[contains(text(),'Submit Quote Request as Guest')]//parent::span//parent::button")
	WebElement  SubmitQuoteRequestasGuest ;
	
	@FindBy(xpath="//div[@class='decoration-popup-wrap']//span[contains(text(),'Submit Quote Request as Guest')]//parent::span//parent::button")
	WebElement  popupSubmitQuoteRequestasGuest ;
	
	@FindBy(xpath="//app-raq-thankyou//a")
	WebElement  quoteno ;
	
	public threedpctquote() {
		
		 PageFactory.initElements(driver, this);
	}
	
	public boolean Quickquotetest1() throws InterruptedException, AWTException {
		Robot robot=new Robot();
		createyourown.click();
		products.get(2).click();
		threedembroidrey.click();
		colourswatch.get(1).click();
		selectproductanddesignnow.click();
		 Thread.sleep(10000);
		 addtext.click();
		 Thread.sleep(5000);
		 text.sendKeys("FAYA");
		 addtodesign.click();
		Thread.sleep(5000);
		 continuee.click();
		 int Sum=0;
		 Random rand = new Random();
		 int j=0;
		 Thread.sleep(5000);
		// Addanothercolurqty.click();
		 int i;
		 for(i=0;i<quantity.size();i++) {
			 quantity.get(i).clear();
			 quantity.get(i).sendKeys(String.valueOf(getRandomInteger(24, 25)));
			
		 }
		 continuee.click();
		 phone.sendKeys("(123) 333-3333 x33333");
		 zip.sendKeys("91761");
		 signinandsubmitquoterequestbutton.click();
		 Email.get(0).sendKeys(prop.getProperty("Emailid"));
		 password.get(0).sendKeys(prop.getProperty("password"));
		signinandsubmitquoterequestbutton.click();
		
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 return Thankyoupage.isDisplayed();

	}

	public boolean Quickquotetest2() throws InterruptedException, AWTException {
		Robot robot=new Robot();
		createyourown.click();
		products.get(2).click();
		threedembroidrey.click();
		colourswatch.get(1).click();
		selectproductanddesignnow.click();
		 Thread.sleep(10000);
		 addclipart.click();
		 Thread.sleep(1000);
		 clipartlib.get(3).click();
		 Thread.sleep(1000);
		 clipartlibrary.click();
		 Thread.sleep(1000);
		 clipartlib1.get(1).click();
		 Thread.sleep(10000);
		 continuee.click();
		 int Sum=0;
		 Random rand = new Random();
		 int j=0;
		 Thread.sleep(5000);
		// Addanothercolurqty.click();
		 int i;
		 for(i=0;i<quantity.size();i++) {
			 quantity.get(i).clear();
			 quantity.get(i).sendKeys(String.valueOf(getRandomInteger(24, 25)));
			
		 }
		 continuee.click();
		 phone.sendKeys("(123) 333-3333 x33333");
		 zip.sendKeys("91761");
		 
		 Thread.sleep(5000);
		 signinandsubmitquoterequestbutton.click();
		 firstname.sendKeys(prop.getProperty("firstname"));
		 lastname.sendKeys(prop.getProperty("lastname"));
		 Email.get(1).sendKeys(randomEmail());
		 password.get(1).sendKeys(prop.getProperty("password1"));
		 confirmpassword.sendKeys(prop.getProperty("confirmpassword"));
		 CreateAccountandSubmitQuoteRequest.click();
		
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 return Thankyoupage.isDisplayed();

	}

	public void Quickquotetest4() throws InterruptedException, AWTException {
		Robot robot=new Robot();
		createyourown.click();
		String productname=productsname.get(2).getText();
		products.get(2).click();
		threedembroidrey.click();
		String colour=colourname.getText();

		colourswatch.get(1).click();
		selectproductanddesignnow.click();
		 Thread.sleep(10000);
		 uploadpic.click();
		 Thread.sleep(5000);
		 upload.click();
		 Thread.sleep(5000);
		 robot.setAutoDelay(1000);
		 
		 StringSelection stringselection=new StringSelection(System.getProperty("user.dir")+"\\Testdatafolder\\logo_e6d3db04afcf4e7aad52fd6961724789.png");
		 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection,null);
		 robot.setAutoDelay(2000);
		 
		
		 
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_V);
		 
		 robot.setAutoDelay(2000);

		 robot.keyPress(KeyEvent.VK_ENTER);
		 robot.keyPress(KeyEvent.VK_ENTER);
		 Thread.sleep(10000);
		 Done.click();
		 Thread.sleep(10000);
		 continuee.click();
		
		 int Sum=0;
		 Random rand = new Random();
		 int j=0;
		 Thread.sleep(5000);
		// Addanothercolurqty.click();
		 int i;
		 for(i=0;i<quantity.size();i++) {
			 quantity.get(i).clear();
			 quantity.get(i).sendKeys(String.valueOf(getRandomInteger(24,25)));
			
		 }
		 continuee.click();
		
		 phone.sendKeys("(123) 333-3333 x33333");
		 zip.sendKeys("91761");
		
		 Thread.sleep(5000);
		submitquote.click();
		
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 Thankyoupage.isDisplayed();
		  String quotenum=quoteno.getText();
			 System.out.println(quotenum);

			 quoteno.click();
			 String quotenumdetail=driver.findElement(By.xpath("//h2[contains(text(),' Quote Details ')]//span//strong")).getText();
			 System.out.println(quotenumdetail);
			 Assert.assertTrue(quotenumdetail.contains(quotenum));
			 String status=driver.findElement(By.xpath("//h3//span[@class='fw-500 text-link']")).getText();
			 System.out.println(status);
			 Assert.assertEquals(status, "Quote - New");
			 driver.findElement(By.xpath("//div[@class='logo-information ng-star-inserted']//div[@class='dst-image']")).isDisplayed();	 
			 String dectype=driver.findElement(By.xpath("//div[@class='logo-details px-1 mb-3']//strong[contains(text(),'Decoration Type:')]//parent::p//parent::div//parent::div//div[2]")).getText();
			 System.out.println(dectype);
			 Assert.assertEquals(dectype, "3D Embroidery");
			 String style=driver.findElement(By.xpath("//div[@class='logo-style ng-star-inserted']//p[@class='fw-500 p-1']")).getText();
			 System.out.println(style);
			 System.out.println(productname);
			 Assert.assertTrue(style.contains(productname));
			 driver.findElement(By.xpath("//p[contains(text(),'Product Virtual Preview*')]//parent::div//div[@class='dst-image']")).isDisplayed();
			 
	}
	
	public void Quickquotetest5() throws InterruptedException, AWTException {
		Robot robot=new Robot();
		createyourown.click();
		String productname=productsname.get(2).getText();

		products.get(2).click();
		threedembroidrey.click();
		String colour=colourname.getText();

		colourswatch.get(1).click();
		selectproductanddesignnow.click();
		 Thread.sleep(10000);
		 uploadpic.click();
		 Thread.sleep(5000);
		 upload.click();
		 Thread.sleep(5000);
		 robot.setAutoDelay(1000);
		 
		 StringSelection stringselection=new StringSelection(System.getProperty("user.dir")+"\\Testdatafolder\\logo_e6d3db04afcf4e7aad52fd6961724789.png");
		 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection,null);
		 robot.setAutoDelay(2000);
		 
		
		 
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_V);
		 
		 robot.setAutoDelay(2000);

		 robot.keyPress(KeyEvent.VK_ENTER);
		 robot.keyPress(KeyEvent.VK_ENTER);
		 Thread.sleep(10000);
		 Done.click();
		 Thread.sleep(10000);
		 continuee.click();
		 int Sum=0;
		 Random rand = new Random();
		 int j=0;
		 Thread.sleep(5000);
		// Addanothercolurqty.click();
		 int i;
		 for(i=0;i<quantity.size();i++) {
			 quantity.get(i).clear();
			 quantity.get(i).sendKeys(String.valueOf(getRandomInteger(24,25)));
			
		 }
		 continuee.click();
		 phone.sendKeys("(123) 333-3333 x33333");
		 zip.sendKeys("91761");
		 SubmitQuoteRequestasGuest.click();
		 firstname.sendKeys("faya");
		 lastname.sendKeys("qa");
		 Emails.sendKeys(prop.getProperty("Emailid"));
		 popupSubmitQuoteRequestasGuest.click();
		 driver.findElement(By.xpath("//h3[contains(text(),' This email address is already registered, please sign in or click on Go Back and use another email address ')]")).isDisplayed();
		 Emails.clear();
		 Emails.sendKeys(prop.getProperty("Emailid"));
		 password.get(0).sendKeys(prop.getProperty("password"));
		signinandsubmitquoterequestbutton1.click();
		
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 Thankyoupage.isDisplayed();
		  String quotenum=quoteno.getText();
			 System.out.println(quotenum);

			 quoteno.click();
			 String quotenumdetail=driver.findElement(By.xpath("//h2[contains(text(),' Quote Details ')]//span//strong")).getText();
			 System.out.println(quotenumdetail);
			 Assert.assertTrue(quotenumdetail.contains(quotenum));
			 String status=driver.findElement(By.xpath("//h3//span[@class='fw-500 text-link']")).getText();
			 System.out.println(status);
			 Assert.assertEquals(status, "Quote - New");
			 driver.findElement(By.xpath("//div[@class='logo-information ng-star-inserted']//div[@class='dst-image']")).isDisplayed();	 
			 String dectype=driver.findElement(By.xpath("//div[@class='logo-details px-1 mb-3']//strong[contains(text(),'Decoration Type:')]//parent::p//parent::div//parent::div//div[2]")).getText();
			 System.out.println(dectype);
			 Assert.assertEquals(dectype, "3D Embroidery");
			 String style=driver.findElement(By.xpath("//div[@class='logo-style ng-star-inserted']//p[@class='fw-500 p-1']")).getText();
			 System.out.println(style);
			 System.out.println(productname);
			 Assert.assertTrue(style.contains(productname));
			 driver.findElement(By.xpath("//p[contains(text(),'Product Virtual Preview*')]//parent::div//div[@class='dst-image']")).isDisplayed();


	}
	
	public boolean Quickquotetest7() throws InterruptedException, AWTException {
		Robot robot=new Robot();
		createyourown.click();
		products.get(2).click();
		threedembroidrey.click();
		colourswatch.get(1).click();
		selectproductanddesignnow.click();
		 Thread.sleep(10000);
		 uploadpic.click();
		 Thread.sleep(5000);
		 upload.click();
		 Thread.sleep(5000);
		 robot.setAutoDelay(1000);
		 
		 StringSelection stringselection=new StringSelection(System.getProperty("user.dir")+"\\Testdatafolder\\logo_e6d3db04afcf4e7aad52fd6961724789.png");
		 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection,null);
		 robot.setAutoDelay(2000);
		 
		
		 
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_V);
		 
		 robot.setAutoDelay(2000);

		 robot.keyPress(KeyEvent.VK_ENTER);
		 robot.keyPress(KeyEvent.VK_ENTER);
		 Thread.sleep(10000);
		 Done.click();
		 Thread.sleep(10000);
		 continuee.click();
		 int Sum=0;
		 Random rand = new Random();
		 int j=0;
		 Thread.sleep(5000);
		// Addanothercolurqty.click();
		 int i;
		 for(i=0;i<quantity.size();i++) {
			 quantity.get(i).clear();
			 quantity.get(i).sendKeys(String.valueOf(getRandomInteger(24,25)));
			
		 }
		 continuee.click();
		 phone.sendKeys("(123) 333-3333 x33333");
		 zip.sendKeys("91761");
		 SubmitQuoteRequestasGuest.click();
		 firstname.sendKeys("faya");
		 lastname.sendKeys("qa");
		 Emails.sendKeys(randomEmail());
		popupSubmitQuoteRequestasGuest.click();
		
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 return Thankyoupage.isDisplayed();

	}
	public boolean Quickquotetest6() throws InterruptedException, AWTException {
		Robot robot=new Robot();
		createyourown.click();
		products.get(2).click();
		threedembroidrey.click();

		colourswatch.get(1).click();
		selectproductanddesignnow.click();
		addproducts.click();
		Thread.sleep(5000);
		String style=productsstyle.get(3).getText();
		products.get(3).click();
	
		System.out.println(style);
		Thread.sleep(10000);
		colourswatch.get(3).click();
		addproduct.click();		
		Thread.sleep(10000);
		int size=productssize.size();
		productssize.get(size-1).click();
		String sty=stylee.get(0).getText();
		//Assert.assertEquals(sty, style);
		uploadpic.click();
		 Thread.sleep(5000);
		 upload.click();
		 Thread.sleep(5000);
		 robot.setAutoDelay(1000);
		 
		 StringSelection stringselection=new StringSelection(System.getProperty("user.dir")+"\\Testdatafolder\\logo_e6d3db04afcf4e7aad52fd6961724789.png");
		 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection,null);
		 robot.setAutoDelay(2000);
		 
		
		 
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_V);
		 
		 robot.setAutoDelay(2000);

		 robot.keyPress(KeyEvent.VK_ENTER);
		 robot.keyPress(KeyEvent.VK_ENTER);
		 Thread.sleep(10000);
		 Done.click();
		 Thread.sleep(10000);
		 continuee.click();
		 
		 int Sum=0;
		 Random rand = new Random();
		 int j=0;
		 Thread.sleep(5000);
		// Addanothercolurqty.click();
		 int i;
		 for(i=0;i<quantity.size();i++) {
			 quantity.get(i).clear();
			 quantity.get(i).sendKeys(String.valueOf(getRandomInteger(24,25)));
			
		 }
		 continuee.click();
		 phone.sendKeys("(123) 333-3333 x33333");
		 zip.sendKeys("91761");
		 signinandsubmitquoterequestbutton.click();
		 Email.get(0).sendKeys(prop.getProperty("Emailid"));
		 password.get(0).sendKeys(prop.getProperty("password"));
		signinandsubmitquoterequestbutton.click();
		
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 return Thankyoupage.isDisplayed();

	}
	public boolean Quickquotetest3() throws InterruptedException, AWTException {
		Robot robot=new Robot();
		createyourown.click();
		products.get(2).click();
		threedembroidrey.click();
		colourswatch.get(1).click();
		selectproductanddesignnow.click();
		 Thread.sleep(10000);
		 addtext.click();
		 Thread.sleep(5000);
		 text.sendKeys("FAYA");
		 addtodesign.click();
		 Thread.sleep(5000);
		 addclipart.click();
		 Thread.sleep(1000);
		 clipartlib.get(5).click();
		 Thread.sleep(1000);
		 clipartlibrary.click();
		 Thread.sleep(1000);
		 clipartlib1.get(1).click();
		 Thread.sleep(5000);
		// back.click();
		 Thread.sleep(5000);
		 uploadpic.click();
		 Thread.sleep(5000);
		 upload.click();
		 Thread.sleep(5000);
		 robot.setAutoDelay(1000);
		 
		 StringSelection stringselection=new StringSelection(System.getProperty("user.dir")+"\\Testdatafolder\\logo_e6d3db04afcf4e7aad52fd6961724789.png");
		 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection,null);
		 robot.setAutoDelay(2000);
		 
		
		 
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_V);
		 
		 robot.setAutoDelay(2000);

		 robot.keyPress(KeyEvent.VK_ENTER);
		 robot.keyPress(KeyEvent.VK_ENTER);
		 Thread.sleep(10000);
	    
	   
    	 Done.click();
    	 Thread.sleep(10000);
    	 continuee.click();
		 int Sum=0;
		 Random rand = new Random();
		 int j=0;
		 Thread.sleep(5000);
		// Addanothercolurqty.click();
		 int i;
		 for(i=0;i<quantity.size();i++) {
			 quantity.get(i).clear();
			 quantity.get(i).sendKeys(String.valueOf(getRandomInteger(24,25)));
			
		 }
		 continuee.click();
		 phone.sendKeys("(123) 333-3333 x33333");
		 zip.sendKeys("91761");
		 signinandsubmitquoterequestbutton.click();
		 Email.get(0).sendKeys(prop.getProperty("Emailid"));
		 password.get(0).sendKeys(prop.getProperty("password"));
		 signinandsubmitquoterequestbutton.click();
		
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 return Thankyoupage.isDisplayed();

	}

}
