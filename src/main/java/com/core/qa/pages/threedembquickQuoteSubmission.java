package com.core.qa.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.core.qa.base.TestBase;
import com.paulhammant.ngwebdriver.NgWebDriver;

public class threedembquickQuoteSubmission extends TestBase {
	
	
	
	@FindBy(xpath="//span[contains(text(),'With Flat Embroidery')]//parent::div")
	WebElement withflatembroidrey;
	
	@FindBy(xpath="//span[contains(text(),'With 3D Embroidery')]//parent::div")
	WebElement threedembroidrey;
	
	@FindBy(xpath="//span[contains(text(),'Submit Quick Quote Request')]//parent::button")
	WebElement quickquote;
	
	@FindBys({
        @FindBy(xpath="//div[@class='product-detail']//input[@type='number']"),
 })
	 List<WebElement>quantity;
	
	
	@FindBy(xpath="//a[contains(text(),'Add Another Color & Quantity')]")
	WebElement Addanothercolurqty;
	
	@FindBy(xpath="//div[@class='upload-field w-100']//mat-form-field")
	WebElement Locationdropdown;
	
	@FindBy(xpath="//span[contains(text(),'Upload Artwork')]//parent::span//parent::div//parent::div//parent::mat-select")
	WebElement Artwork;
	
	
	@FindBys({
        @FindBy(xpath="//mat-option"),
 })
	 List<WebElement>dropdownoptions;
	
		
	@FindBy(xpath="//span[contains(text(),' Choose File ')]//parent::button")
	WebElement choosefile;
	
	@FindBy(xpath="//a[contains(text(),'Add another Design')]")
	WebElement Addanotherdesign;
	
	@FindBy(xpath="//input[@formcontrolname='text']")
	WebElement artworktext;
	
	@FindBy(xpath="//input[@formcontrolname='phone']")
	WebElement phone;
	
	@FindBy(xpath="//input[@formcontrolname='zip']")
	WebElement zip;
	
	@FindBy(xpath="//textarea[@formcontrolname='comments']")
	WebElement comments;
	
	@FindBy(xpath="//span[contains(text(),'Sign In and Submit Quote Request')]//parent::span//parent::button")
	WebElement signinandsubmitquoterequestbutton;
	
	@FindBys({
        @FindBy(xpath="//span[contains(text(),'Sign In and Submit Quote Request')]//parent::span//parent::button"),
 })
	 List<WebElement>signinandsubmitquoterequestbutton1;
	
	
	@FindBys({
        @FindBy(xpath="//input[@formcontrolname='email']"),
 })
	 List<WebElement>Email;
	
	@FindBys({
        @FindBy(xpath="//input[@formcontrolname='password']"),
 })
	 List<WebElement>password;
	
	@FindBy(xpath="//*[@id=\"app\"]/ngx-ecommerce-core-layout/mat-sidenav-container/mat-sidenav-content/div/app-raq-thankyou/div/div/h2")
	WebElement Thankyoupage;
	
	@FindBy(xpath="//input[@formcontrolname='first_name']")
	WebElement firstname;
	
	@FindBy(xpath="//input[@formcontrolname='last_name']")
	WebElement lastname;
	
	@FindBy(xpath="//input[@formcontrolname='confirm_password']")
	WebElement confirmpassword;
	
	@FindBy(xpath="//span[contains(text(),'Submit Quote Request')]//parent::span//parent::button")
	WebElement submitquotereq;
	
	@FindBy(xpath="//span[contains(text(),' Create Account and Submit Quote Request ')]//parent::button")
	WebElement  CreateAccountandSubmitQuoteRequest ;
	
	@FindBy(xpath="//span[contains(text(),'Submit Quote Request as Guest')]//parent::span//parent::button")
	WebElement  SubmitQuoteRequestasGuest ;
	
	@FindBy(xpath="//div[@class='decoration-popup-wrap']//span[contains(text(),'Submit Quote Request as Guest')]//parent::span//parent::button")
	WebElement  popupSubmitQuoteRequestasGuest ;
	
	@FindBy(xpath="//app-raq-thankyou//a")
	WebElement  quoteno ;
	
	public threedembquickQuoteSubmission() {
		
		 PageFactory.initElements(driver, this);
	}
	
	public void Quickquotetest1(String phones,String zips,String comment) throws InterruptedException, AWTException {
		String subString;
		Thread.sleep(5000);
		Robot robot=new Robot();
		String productname=driver.findElement(By.xpath("//div//h2")).getText();

		 String testString = productname;
		  int startIndex = testString.indexOf(":");
		  subString = testString.substring(startIndex+1);
		  System.out.println(subString);
		threedembroidrey.click();
		quickquote.click();
		int Sum=0;
		 Random rand = new Random();
		 int j=0;
		 Thread.sleep(5000);
		// Addanothercolurqty.click();
		 int i;
		 for(i=0;i<quantity.size();i++) {
			 quantity.get(i).sendKeys(String.valueOf(getRandomInteger(1, 1)));
			
		 }
		 
		 Locationdropdown.click();
		 dropdownoptions.get(1).click();
		 Artwork.click();
		 Thread.sleep(5000);
		 dropdownoptions.get(0).click();
		 Thread.sleep(5000);

		 choosefile.click();
		 robot.setAutoDelay(1000);
		 
		 StringSelection stringselection=new StringSelection(System.getProperty("user.dir")+"\\Testdatafolder\\US_Flag_4.DST");
		 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection,null);
		 robot.setAutoDelay(2000);
		 
		
		 
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_V);
		 
		 robot.setAutoDelay(2000);

		 robot.keyPress(KeyEvent.VK_ENTER);
		 robot.keyPress(KeyEvent.VK_ENTER);
		 Thread.sleep(1000);
		
		 phone.sendKeys(phones);
		 zip.sendKeys(zips);
		 comments.sendKeys(comment);
		 Thread.sleep(5000);
		 signinandsubmitquoterequestbutton.click();
		 Email.get(1).sendKeys(prop.getProperty("Emailid"));
		 password.get(0).sendKeys(prop.getProperty("password"));
		 signinandsubmitquoterequestbutton1.get(1).click();
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 Thankyoupage.isDisplayed();
		 String quotenum=quoteno.getText();
		 System.out.println(quotenum);

		 quoteno.click();
		 String quotenumdetail=driver.findElement(By.xpath("//h2[contains(text(),' Quote Details ')]//span//strong")).getText();
		 System.out.println(quotenumdetail);
		 Assert.assertTrue(quotenumdetail.contains(quotenum));
		 String status=driver.findElement(By.xpath("//h3//span[@class='fw-500 text-link']")).getText();
		 System.out.println(status);
		 Assert.assertEquals(status, "Quote - New");
		 driver.findElement(By.xpath("//div[@class='logo-information ng-star-inserted']//div[@class='dst-image']")).isDisplayed();	 
		 String dectype=driver.findElement(By.xpath("//div[@class='logo-details px-1 mb-3']//strong[contains(text(),'Decoration Type:')]//parent::p//parent::div//parent::div//div[2]")).getText();
		 System.out.println(dectype);
		 Assert.assertEquals(dectype, "3D Embroidery");
		 String style=driver.findElement(By.xpath("//div[@class='logo-style ng-star-inserted']//p[@class='fw-500 p-1']")).getText();
		 System.out.println(style);
		 System.out.println(subString);
		 Assert.assertTrue(style.contains(subString));
		 driver.findElement(By.xpath("//p[contains(text(),'Product Virtual Preview*')]//parent::div//div[@class='dst-image']")).isDisplayed();


	}
	
	public boolean Quickquotetest2() throws InterruptedException, AWTException {
		
		Robot robot=new Robot();
		threedembroidrey.click();
		quickquote.click();
		int Sum=0;
		 Random rand = new Random();
		 int j=0;
		 Thread.sleep(5000);
		// Addanothercolurqty.click();
		 int i;
		 for(i=0;i<quantity.size();i++) {
			 quantity.get(i).sendKeys(String.valueOf(getRandomInteger(1, 1)));
			
		 }
		 
		 Locationdropdown.click();
		 dropdownoptions.get(2).click();
		 Artwork.click();
		 Thread.sleep(1000);
		 dropdownoptions.get(1).click();
		 Thread.sleep(1000);
		 artworktext.sendKeys("FAYA");
		 phone.sendKeys("(123) 333-3333 x33333");
		 zip.sendKeys("91761");
		 comments.sendKeys("This is a test quote");
		 Thread.sleep(5000);
		 signinandsubmitquoterequestbutton.click();
		 firstname.sendKeys(prop.getProperty("firstname"));
		 lastname.sendKeys(prop.getProperty("lastname"));
		 Email.get(2).sendKeys(randomEmail());
		 password.get(1).sendKeys(prop.getProperty("password1"));
		 confirmpassword.sendKeys(prop.getProperty("confirmpassword"));
		 CreateAccountandSubmitQuoteRequest.click();
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 return Thankyoupage.isDisplayed();

	}
public boolean Quickquotetest3() throws InterruptedException, AWTException {
		
		Robot robot=new Robot();
		threedembroidrey.click();
		quickquote.click();
		int Sum=0;
		 Random rand = new Random();
		 int j=0;
		 Thread.sleep(5000);
		// Addanothercolurqty.click();
		 int i;
		 for(i=0;i<quantity.size();i++) {
			 quantity.get(i).sendKeys(String.valueOf(getRandomInteger(1, 1)));
			
		 }
		 
		 Locationdropdown.click();
		 dropdownoptions.get(3).click();
		 Artwork.click();
		 Thread.sleep(1000);
		 dropdownoptions.get(2).click();
		 Thread.sleep(1000);
		
		 phone.sendKeys("(123) 333-3333 x33333");
		 zip.sendKeys("91761");
		 comments.sendKeys("This is a test quote");
		 Thread.sleep(5000);
		 submitquotereq.click();
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 return Thankyoupage.isDisplayed();
	}
public boolean Quickquotetest4() throws InterruptedException, AWTException {
	
	Robot robot=new Robot();
	threedembroidrey.click();
	quickquote.click();
	int Sum=0;
	 Random rand = new Random();
	 int j=0;
	 Thread.sleep(5000);
	// Addanothercolurqty.click();
	 int i;
	 for(i=0;i<quantity.size();i++) {
		 quantity.get(i).sendKeys(String.valueOf(getRandomInteger(1, 1)));
		
	 }
	 
	 Locationdropdown.click();
	 dropdownoptions.get(1).click();
	 Artwork.click();
	 Thread.sleep(5000);
	 dropdownoptions.get(0).click();
	 Thread.sleep(5000);

	 choosefile.click();
	 robot.setAutoDelay(1000);
	 
	 StringSelection stringselection=new StringSelection(System.getProperty("user.dir")+"\\Testdatafolder\\logo_e6d3db04afcf4e7aad52fd6961724789.png");
	 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection,null);
	 robot.setAutoDelay(2000);
	 
	
	 
	 robot.keyPress(KeyEvent.VK_CONTROL);
	 robot.keyPress(KeyEvent.VK_V);
	 
	 robot.setAutoDelay(2000);

	 robot.keyPress(KeyEvent.VK_ENTER);
	 robot.keyPress(KeyEvent.VK_ENTER);
	 Thread.sleep(1000);
	
	 phone.sendKeys("(123) 333-3333 x33333");
	 zip.sendKeys("91761");
	 comments.sendKeys("This is a test quote");
	 Thread.sleep(5000);
	 SubmitQuoteRequestasGuest.click();
	 firstname.sendKeys("faya");
	 lastname.sendKeys("qa");
	 Email.get(1).sendKeys(prop.getProperty("Emailid"));
	 popupSubmitQuoteRequestasGuest.click();
	 driver.findElement(By.xpath("//h3[contains(text(),' This email address is already registered, please sign in or click on Go Back and use another email address ')]")).isDisplayed();
	 Email.get(1).sendKeys(prop.getProperty("Emailid"));
	 password.get(0).sendKeys(prop.getProperty("password"));
	 signinandsubmitquoterequestbutton1.get(1).click();
	 JavascriptExecutor jsDriver;
	 NgWebDriver ngWebDriver;
	 jsDriver = (JavascriptExecutor) driver;
	 ngWebDriver = new NgWebDriver(jsDriver);
	 try {
		 ngWebDriver.waitForAngularRequestsToFinish();

	 }
	 catch (Exception e) {

	}
	 return Thankyoupage.isDisplayed();


}

public boolean Quickquotetest5() throws InterruptedException, AWTException {
	
	Robot robot=new Robot();
	threedembroidrey.click();
	quickquote.click();
	int Sum=0;
	 Random rand = new Random();
	 int j=0;
	 Thread.sleep(5000);
	// Addanothercolurqty.click();
	 int i;
	 for(i=0;i<quantity.size();i++) {
		 quantity.get(i).sendKeys(String.valueOf(getRandomInteger(1, 1)));
		
	 }
	 
	 Locationdropdown.click();
	 dropdownoptions.get(1).click();
	 Artwork.click();
	 Thread.sleep(5000);
	 dropdownoptions.get(0).click();
	 Thread.sleep(5000);

	 choosefile.click();
	 robot.setAutoDelay(1000);
	 
	 StringSelection stringselection=new StringSelection(System.getProperty("user.dir")+"\\Testdatafolder\\logo_e6d3db04afcf4e7aad52fd6961724789.png");
	 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection,null);
	 robot.setAutoDelay(2000);
	 
	
	 
	 robot.keyPress(KeyEvent.VK_CONTROL);
	 robot.keyPress(KeyEvent.VK_V);
	 
	 robot.setAutoDelay(2000);

	 robot.keyPress(KeyEvent.VK_ENTER);
	 robot.keyPress(KeyEvent.VK_ENTER);
	 Thread.sleep(1000);
	
	 phone.sendKeys("(123) 333-3333 x33333");
	 zip.sendKeys("91761");
	 comments.sendKeys("This is a test quote");
	 Thread.sleep(5000);
	 SubmitQuoteRequestasGuest.click();
	 firstname.sendKeys("faya");
	 lastname.sendKeys("qa");
	 Email.get(1).sendKeys(randomEmail());
	 popupSubmitQuoteRequestasGuest.click();
	
	 JavascriptExecutor jsDriver;
	 NgWebDriver ngWebDriver;
	 jsDriver = (JavascriptExecutor) driver;
	 ngWebDriver = new NgWebDriver(jsDriver);
	 try {
		 ngWebDriver.waitForAngularRequestsToFinish();

	 }
	 catch (Exception e) {

	}
	 return Thankyoupage.isDisplayed();
}

}



