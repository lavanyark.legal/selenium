package com.core.qa.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import com.core.qa.base.TestBase;

public class Homepage extends TestBase {
	

	@FindBys({
    @FindBy(xpath="//a[@class='logo ng-star-inserted']"), })
	List<WebElement>Logo;
	
	@FindBy(linkText="Create an Account")
	WebElement createanaccount;
	
	@FindBy(xpath="//input[@title='search']")
	WebElement Search;
	
	@FindBy(xpath="//button[@class='mat-focus-indicator search-btn spinner-btn mat-elevation-z0 mat-flat-button mat-button-base mat-primary']")
	WebElement Searchbutton;
	
	@FindBy(xpath="//input[@formcontrolname='last_name']")
	WebElement lastname;
	
	@FindBy(linkText="Create an Account")
	WebElement createaccount;
	
	@FindBy(linkText="Sign In")
	WebElement SignIn;
	
	@FindBy(xpath="//div[@class='login-wrap quote-request-login-form']")
	WebElement SignInpopup;
	
	@FindBy(xpath="//div[@class='theme-container full-width menu-wrap pos-relative']")
	WebElement Megamenu;
	
	@FindBy(xpath="//ngx-ecommerce-core-main-carousel")
	WebElement Banner;
	
	
	@FindBys({
    @FindBy(xpath="//div[@class='text-uppercase product-title mr-1 fw-400 pl-2']"),})
	 List<WebElement>Widgetname;
	
	@FindBys({
	    @FindBy(xpath="//div[@class='products-filter ng-star-inserted']"),})
		 List<WebElement>Widget;
	
	@FindBys({
        @FindBy(xpath="//button[@class='mat-focus-indicator mat-button view-all-btn mat-button-base']"),
 })
	 List<WebElement>Viewall;

	@FindBy(linkText="About Us")
	WebElement AboutUs;
	
	@FindBy(xpath="//div[@class='cms-wrap fw-light ng-star-inserted']//img")
	WebElement AboutUsbanner;
	
	@FindBy(linkText="Contact Us")
	WebElement ContactUs ;
	
	@FindBy(linkText="Privacy Policy")
	WebElement PrivacyPolicy ;
	
	@FindBy(xpath="//div//h1[contains(text(),'PRIVACY POLICY')]")
	WebElement PrivacyPolicyheader ;
	
	@FindBy(linkText="Terms and Conditions")
	WebElement TermsandConditions ;
	
	@FindBy(xpath="//div[@class='terms']")
	WebElement TermsandConditionsheader ;

	@FindBy(xpath="//p//a[contains(text(),'Order Status')]")
	WebElement Orderstatus;
	
	@FindBy(xpath="//p//a[contains(text(),'FAQ')]")
	WebElement FAQ;
	
	@FindBy(xpath="//p//a[contains(text(),'Request A Quote')]")
	WebElement RequestAQuote;
	
	@FindBys({
        @FindBy(xpath="//input[@formcontrolname='email']"),
 })
	 List<WebElement>Email;
	
	@FindBy(xpath="//button[@aria-label='signup']")
	WebElement  SignUp ;
	
	@FindBy(xpath="//span[contains(text(),'Thank You for subscribing Newsletter!')]")
	WebElement Successsubcriptionmessage;
	
	@FindBy(xpath="//span[contains(text(),'You’re Already Subscribed!')]")
	WebElement youarealreadysubscribedmessage;
	
	@FindBy(xpath="//mat-error[@role='alert']")
	WebElement Error;
	
	@FindBy(xpath="//span[@class='filter-value bg-primary fw-300 ng-star-inserted']")
	WebElement Searchfilter;
	

	@FindBys({
        @FindBy(xpath="//div[@class='search-list-inner']//mat-option"),
 })
	 List<WebElement>Searchdropdown;
	
	
	@FindBys({
        @FindBy(xpath="//div//span[@class='product-name w-100 text-truncate']//em"),
 })
	 List<WebElement>productsname;
	
	@FindBys({
        @FindBy(xpath="//div[@class='banner-ads-placeholder ng-star-inserted']//a//img"),
 })
	 List<WebElement>adslink;
	
	@FindBys({
        @FindBy(xpath="//ngx-ecommerce-core-main-carousel//div//ul//li"),
 })
	 List<WebElement>Bannerlink;
	
	@FindBys({
        @FindBy(xpath="//div[@class='banners-container ng-star-inserted']//img"),
 })
	 List<WebElement>advlink;
	
	@FindBy(xpath="//a[contains(text(),'View All Brands')]")
	WebElement viewallbrands;
	
	@FindBy(xpath="//div[@class='brand-block text-center ng-star-inserted']")
	WebElement brands;
	
	@FindBy(xpath="//a[contains(text(),'Product search filter')]")
	WebElement Productsearchfilter ;
	
	@FindBy(linkText="CREATE YOUR OWN")
	WebElement CREATEYOUROWN ;
	
	@FindBy(linkText="ALL PRODUCTS")
	WebElement AllProducts ;
	
	@FindBy(xpath="//span[@class='mat-button-wrapper' and contains(text(),'5 Panel Cap ')]//parent::a")
	WebElement fivePanelCap   ;
	
	@FindBy(xpath="//span[@class='mat-button-wrapper' and contains(text(),'6 Panel Cap ')]//parent::a")
	WebElement sixPanelCap  ;
	
	@FindBy(xpath="//span[@class='mat-button-wrapper' and contains(text(),'Flat Visors')]//parent::a")
	WebElement FlatVisors  ;
	
	@FindBy(xpath="//span[@class='mat-button-wrapper' and contains(text(),'Trucker Caps ')]//parent::a")
	WebElement TruckerCaps  ;
	
	@FindBy(xpath="//span[@class='mat-button-wrapper' and contains(text(),'Camo Hats')]//parent::a")
	WebElement CamoHats  ;
	
	@FindBy(xpath="//span[@class='mat-button-wrapper' and contains(text(),'Performance')]//parent::a")
	WebElement Performance  ;
	
	@FindBy(linkText="ON SALE")
	WebElement onsale ;
	
	@FindBys({
	    @FindBy(xpath="//div[@class='products-filter ng-star-inserted']//img"),})
		 List<WebElement>products;
	
	@FindBys({
        @FindBy(xpath="//div[@class='products-wrapper mt-0 accelerate ng-star-inserted']//a"),
 })
	 List<WebElement>Products;
	public Homepage() {
		
		 PageFactory.initElements(driver, this);
	}
	
	public boolean logodisplayed() {
		return Logo.get(1).isDisplayed();
	}
	
	public boolean Searchdisplayed() {
		return Search.isDisplayed();
	}
	
	public String Searchbuttontest(String style) throws InterruptedException {
		Search.sendKeys(style);
		Thread.sleep(1000);
		Searchbutton.click();
		String searchtext=driver.findElement(By.xpath("//div//strong[@class='style-number fw-500 mr-3 font-sm ng-star-inserted']")).getText();
		return searchtext;
	
	}
	
	public String Searchdropdowntest(String style) {
		Search.sendKeys(style);
		String productname=productsname.get(0).getText();
		Searchdropdown.get(0).click();
		return productname;
	
	}
	
	public boolean Bannerchecking() throws InterruptedException {
		Thread.sleep(5000);
		return Banner.isDisplayed();
	}
	
	public boolean Bannerlinkchecking1() throws InterruptedException {
		Bannerlink.get(0).click();
		Banner.click();
		return Products.get(0).isDisplayed();
	}
	
	public boolean Bannerlinkchecking2() throws InterruptedException {
		Bannerlink.get(1).click();
		Banner.click();
		return Products.get(0).isDisplayed();
	}
	
	public boolean Bannerlinkchecking3() throws InterruptedException {
		Bannerlink.get(2).click();
		Banner.click();
		return Products.get(0).isDisplayed();
	}
	
	public boolean Bannerlinkchecking4() throws InterruptedException {
		Bannerlink.get(3).click();
		Banner.click();
		return Products.get(0).isDisplayed();
	}
	public boolean adslinkcheck1() throws InterruptedException {
		Thread.sleep(5000);
		adslink.get(0).click();
		Thread.sleep(5000);
		return Products.get(0).isDisplayed();
	}
	
	public boolean adslinkcheck2() throws InterruptedException {
		Thread.sleep(5000);
		adslink.get(1).click();
		Thread.sleep(5000);
		return Products.get(0).isDisplayed();
	}
	
	public boolean adslinkcheck3() throws InterruptedException {
		Thread.sleep(5000);
		advlink.get(0).click();
		Thread.sleep(5000);
		return Products.get(0).isDisplayed();
	}
	
	public boolean adslinkcheck4() throws InterruptedException {
		Thread.sleep(5000);
		advlink.get(1).click();
		Thread.sleep(5000);
		return Products.get(0).isDisplayed();
	}
	
	public boolean adslinkcheck5() throws InterruptedException {
		Thread.sleep(5000);
		advlink.get(2).click();
		Thread.sleep(5000);
		return Products.get(0).isDisplayed();
	}
	
	public boolean adslinkcheck6() throws InterruptedException {
		Thread.sleep(5000);
		advlink.get(3).click();
		Thread.sleep(5000);
		return Products.get(0).isDisplayed();
	}

	public boolean Widgetchecking() throws InterruptedException {
		Thread.sleep(5000);
		return Widget.get(0).isDisplayed();
	}
	
	public boolean megamenuchecking() {
		return Megamenu.isDisplayed();
	
	}
	public boolean productsearchfiltercheck() throws InterruptedException {
		Productsearchfilter.click();
		Thread.sleep(5000);
		return Products.get(0).isDisplayed();
		
	}
	public boolean createyourowncheck() throws InterruptedException {
		CREATEYOUROWN.click();
		Thread.sleep(5000);
		return Products.get(0).isDisplayed();
		
	}
	public boolean allproductscheck() throws InterruptedException {
		AllProducts.click();
		Thread.sleep(5000);
		return Products.get(0).isDisplayed();
		
	}
	public boolean fivePanelCapcheck() throws InterruptedException {
		fivePanelCap.click();
		Thread.sleep(5000);
		return Products.get(0).isDisplayed();
		
	}
	public boolean sixPanelCapcheck() throws InterruptedException {
		sixPanelCap.click();
		Thread.sleep(5000);
		return Products.get(0).isDisplayed();
		
	}
	public boolean FlatVisorscheck() throws InterruptedException {
		FlatVisors.click();
		Thread.sleep(5000);
		return Products.get(0).isDisplayed();
		
	}
	public boolean CamoHatscheck() throws InterruptedException {
		CamoHats.click();
		Thread.sleep(5000);
		return Products.get(0).isDisplayed();
		
	}
	public boolean TruckerCapscheck() throws InterruptedException {
		TruckerCaps.click();
		Thread.sleep(5000);
		return Products.get(0).isDisplayed();
		
	}
	public boolean Performancecheck() throws InterruptedException {
		Performance.click();
		Thread.sleep(5000);
		return Products.get(0).isDisplayed();
		
	}
	public boolean onsalecheck() throws InterruptedException {
		onsale.click();
		Thread.sleep(5000);
		return Products.get(0).isDisplayed();
		
	}
	
	public boolean menuslink() throws InterruptedException {
		//Thread.sleep(5000);
		int size=driver.findElements(By.xpath("//*[@id=\"app\"]/ngx-ecommerce-core-layout/mat-sidenav-container/mat-sidenav-content/ngx-ecommerce-core-menu/mat-toolbar/div/div")).size();
		System.out.println(size);
		for(int i=0;i<size;i++){
			//Thread.sleep(5000);

			driver.findElements(By.xpath("//*[@id=\"app\"]/ngx-ecommerce-core-layout/mat-sidenav-container/mat-sidenav-content/ngx-ecommerce-core-menu/mat-toolbar/div/div")).get(i).click();
			

			Products.get(0).isDisplayed();
			//Thread.sleep(5000);
			System.out.println(i);

			driver.navigate().back();
		}
		return true;
	}
	
	public boolean newsletterchecking1() {
		SignUp.click();
		return Error.isDisplayed();
	}
	
	public boolean newsletterchecking2(String email) throws InterruptedException {
		Email.get(0).sendKeys(email);
		Thread.sleep(1000);
		SignUp.click();
		return Successsubcriptionmessage.isDisplayed();
	}
	
	public boolean newsletterchecking3(String email) {
		Email.get(0).sendKeys(email);
		SignUp.click();
		return youarealreadysubscribedmessage.isDisplayed();
	}

	public String widgetcheck1() throws InterruptedException {
		Thread.sleep(5000);
		int size=Widgetname.size();
		System.out.println(size);
		Thread.sleep(5000);
		String text=Widgetname.get(0).getText();
		Viewall.get(0).click();
		return text;
		
	}
	
	public String widgetcheck2() throws InterruptedException {
		Thread.sleep(5000);
		String text=Widgetname.get(1).getText();
		Viewall.get(1).click();
		return text;
		
	}
	public String widgetcheck3() throws InterruptedException {
		Thread.sleep(5000);
		String text=Widgetname.get(2).getText();
		Viewall.get(2).click();
		return text;
		
	}
	public String widgetcheck4() throws InterruptedException {
		Thread.sleep(5000);
		String text=Widgetname.get(3).getText();
		Viewall.get(3).click();
		return text;
		
	}
	
	public boolean aboutuscheck() throws InterruptedException {
		AboutUs.click();
		Thread.sleep(5000);
		return AboutUsbanner.isDisplayed();
		
		
	}
	
	public String contactuscheck() throws InterruptedException {
		
		ContactUs.click();
		Thread.sleep(2000);
		String url=driver.getCurrentUrl();
		return url;
		
		
	}
	public boolean privacypolicycheck() throws InterruptedException {
		PrivacyPolicy.click();
		Thread.sleep(2000);
		return PrivacyPolicyheader.isDisplayed();
		
		
		
	}
	public String termsandconditioncheck() throws InterruptedException {
		TermsandConditions.click();
		Thread.sleep(2000);
		String url=driver.getCurrentUrl();
		return url;
	}
	
	public String orderstatuscheck() throws InterruptedException {
		Orderstatus.click();
		Thread.sleep(2000);
		String url=driver.getCurrentUrl();
		return url;
	}
	public String FAQcheck() throws InterruptedException {
		FAQ.click();
		Thread.sleep(5000);
		String url=driver.getCurrentUrl();
		return url;
	}
	public String RAQcheck() throws InterruptedException {
		RequestAQuote.click();
		Thread.sleep(2000);
		String url=driver.getCurrentUrl();
		return url;
	} 
	
	public String Createanaccountlinktest() throws InterruptedException {
		createaccount.click();
		Thread.sleep(2000);
		String url=driver.getCurrentUrl();
		return url;
	} 
	
	public boolean Signinlinktest() throws InterruptedException {
		SignIn.click();
		Thread.sleep(1000);
		return SignInpopup.isDisplayed();
	} 
	
	public boolean homepageproductschecking() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,400)", "");		
		return products.get(0).isDisplayed();
	
	}

}
