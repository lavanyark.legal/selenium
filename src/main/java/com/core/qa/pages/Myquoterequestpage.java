package com.core.qa.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.core.qa.base.TestBase;

public class Myquoterequestpage extends TestBase {
	

	@FindBys({
	    @FindBy(xpath="//mat-icon//parent::span[@class='mat-button-wrapper']//parent::a"),})
		 List<WebElement>accountdropdown;
	
	@FindBy(xpath="//a[contains(text(),'Account Dashboard')]//parent::div")
	WebElement Accountdashboard;
	
	@FindBy(xpath="//div[@class='account-dashboard-wrap']")
	WebElement Accountdashboardtable;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'My Orders')]")
	WebElement MyOrders;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'My Quote Requests')]")
	WebElement MyQuoteRequests;
	
	@FindBy(xpath="//h2[contains(text(),'My Quote Requests')]")
	WebElement header;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'My Wishlist')]")
	WebElement MyWishlist;
	
	@FindBy(xpath="//h2[contains(text(),'My Wishlist')]")
	WebElement wishlist;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'My Saved Cards')]")
	WebElement mysavedcards;
	
	
	@FindBy(xpath="//span[contains(text(),' Add New Card')]//parent::button")
	WebElement addnewcard;
	
	@FindBy(xpath="//input[@formcontrolname='cardNumber']")
	WebElement cardnumber;
	
	@FindBy(xpath="//input[@formcontrolname='cardHolderName']")
	WebElement cardHolderName;
	
	@FindBy(xpath="//mat-select[@formcontrolname='expirationMonth']")
	WebElement expirationmonth;
	
	@FindBy(xpath="//span[contains(text(),'June')]")
	WebElement june;
	
	@FindBy(xpath="//mat-select[@formcontrolname='expirationYear']")
	WebElement expirationYear;
	
	@FindBy(xpath="//span[contains(text(),'2029')]")
	WebElement year;
	
	@FindBy(xpath="//input[@formcontrolname='cvv']")
	WebElement cvv;
	
	@FindBys({
        @FindBy(xpath="//mat-radio-group"),
 })
	 List<WebElement>address;
	
	@FindBy(xpath="//a[contains(text(),'+Add New Address')]")
	WebElement addnewaddress;
	
	@FindBy(xpath="//input[@formcontrolname='name']")
	WebElement name;
	
	@FindBy(xpath="//div[@class='save-card-popup ng-star-inserted']//input[@formcontrolname='email']")
	WebElement email;
	
	@FindBy(xpath="//input[@formcontrolname='company_name']")
	WebElement company_name;
	
	@FindBy(xpath="//input[@formcontrolname='address_1']")
	WebElement address_1;
	
	@FindBy(xpath="//input[@formcontrolname='city']")
	WebElement city;
	
	@FindBy(xpath="//mat-select[@formcontrolname='state']")
	WebElement state;
	
	@FindBy(xpath="//span[contains(text(),'Louisiana')]")
	WebElement  Louisiana ;
	
	@FindBy(xpath="//input[@formcontrolname='zip']")
	WebElement zip;
	
	@FindBy(xpath="//input[@formcontrolname='phone']")
	WebElement phone;
	
	@FindBy(xpath="//span[contains(text(),'Save')]//parent::button")
	WebElement save;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'Account Information')]")
	WebElement AccountInformation;
		
	@FindBy(xpath="//h2[contains(text(),'Account Information')]")
	WebElement accountinfo ;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'Address Book')]")
	WebElement AddressBook ;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'FAQ - Customer Order Process')]")
	WebElement faq ;
	
	@FindBy(xpath="//h1[contains(text(),'FAQ - Custom Embroidery Order')]")
	WebElement faqheader ;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'Log Out')]")
	WebElement Logout ;
	
	@FindBys({
	@FindBy(xpath="//input[@placeholder='Search']"),
	 })
	List<WebElement> Search ;
	
	@FindBys({
		@FindBy(xpath="//quote-request-table//td"),
		 })
	List<WebElement> quotenumber ;
	
	@FindBys({
		@FindBy(xpath="//ngx-ecommerce-core-orders/quote-request-table/div/table/tbody/tr//td"),
		 })
	List<WebElement> blankdernumber ;
	
	@FindBys({
		@FindBy(xpath="//mat-icon[contains(text(),'search')]//parent::button"),
		 })
	List<WebElement> searchbutton ;
	
	@FindBys({
		@FindBy(xpath="//select[@class='form-control']//option"),
	 })
	List<WebElement> statusdropdown ;
	
	
	@FindBys({
		@FindBy(xpath="//option[@value='order__customer_orders']"),
		 })
	List<WebElement> orderprocessing ;
	
	@FindBys({
		@FindBy(xpath="//quote-request-table//tr"),
		 })
	List<WebElement> rows ;
	@FindBys({
		@FindBy(xpath="//ngx-ecommerce-core-orders/quote-request-table/div/table/tbody/tr"),
		 })
	List<WebElement> blankrows ;
	
		@FindBy(xpath="//div[@class='mat-paginator-range-label']")
	
	WebElement page ;
	
	@FindBys({
		@FindBy(xpath="//mat-icon[@mattooltip='View']"),
		 })
	List<WebElement> view ;
	

		@FindBy(xpath="//ngx-ecommerce-core-orders/quote-request-table/div/table/tbody/tr//td//mat-icon[@mattooltip='View']")
	
	WebElement blankview ;
	
	@FindBy(xpath="//h2//strong")
	WebElement ordernum ;
	
	@FindBy(xpath="//div//strong[contains(text(),'Quote Date:')]//parent::div//parent::div//h3")
	WebElement quotedate ;
	
	@FindBy(xpath="//div//strong[contains(text(),'Quote Status:')]//parent::div//parent::div//h3")
	WebElement quotestatus ;
	
	@FindBys({
		@FindBy(xpath="//tr[@class='total-quantity']//strong"),
		 })
	List<WebElement> qty ;
	
	@FindBys({
		@FindBy(xpath="//strong[@class='font-xl ng-star-inserted']"),
		 })
	List<WebElement> grandtotal ;
	
	public Myquoterequestpage() {
		
		 PageFactory.initElements(driver, this);
	}
	

	public boolean myquoterequesttest() throws InterruptedException {
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);
		MyQuoteRequests.click();
		Thread.sleep(5000);
		return header.isDisplayed();
	}
	
	public void Myquoterequesttablesearchtest() throws InterruptedException {
		String subString;
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);
		MyQuoteRequests.click();
		String ordernum=quotenumber.get(1).getText();
		Search.get(0).sendKeys(ordernum);
		Thread.sleep(5000);
		System.out.println(ordernum);
		Assert.assertTrue(quotenumber.get(1).isDisplayed());
	}
	public void Myquoterequeststatustest() throws InterruptedException {
		String subString;
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);
		MyQuoteRequests.click();
		Thread.sleep(5000);
		int size1=driver.findElements(By.xpath("//select[@class='form-control']//option")).size();
		for(int k=1;k<size1;k++) {
		statusdropdown.get(k).click();
		String quotestatus=statusdropdown.get(k).getText();
		Thread.sleep(5000);
		if(driver.findElements(By.xpath("//div[@class='mat-paginator-range-label']")).size()!=0) {
			String pagen=page.getText();
			  String testString = pagen;
			  int startIndex = testString.indexOf("of ");
			  subString = testString.substring(startIndex+3);
			  System.out.println(subString);
			  driver.findElement(By.xpath("//mat-select[@role='listbox']")).click();
			  driver.findElement(By.xpath("//span[contains(text(),'100')]")).click();
			  Thread.sleep(5000);
			  int size=Integer.parseInt(subString)*8;
			for(int i=3;i<size;i+=8) {
				String stat=quotenumber.get(i).getText();
				System.out.println(stat);
				Assert.assertTrue(stat.contains(quotestatus));
			}
			}
		else {
			driver.findElement(By.xpath("//p[contains(text(),'No quote requests have been submitted.')]")).isDisplayed();
		
		}
		
		}
		
				
	}
	public void Myquoterequestorderdetailstest() throws InterruptedException {
		String subString;
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);
		MyQuoteRequests.click();
		Thread.sleep(10000);
		int size1=driver.findElements(By.xpath("//select[@class='form-control']//option")).size();
		System.out.println(size1);
		Thread.sleep(5000);
		System.out.println("aa");
		for(int k=1;k<size1;k++) {
			System.out.println("bb");
			statusdropdown.get(k).click();
			String quotestatusss=statusdropdown.get(k).getText();
			Thread.sleep(5000);
			if(driver.findElements(By.xpath("//div[@class='mat-paginator-range-label']")).size()!=0) {
				String orderdates=quotenumber.get(0).getText();
				System.out.println(orderdates);
				String ordernumb=quotenumber.get(1).getText();
				System.out.println(ordernumb);
				String orderstat=quotenumber.get(3).getText();
				System.out.println(orderstat);
				if(view.size()==0) {
					driver.findElements(By.xpath("//mat-icon[@mattooltip='Approve']")).get(0).click();
				}
				else {
					view.get(0).click();
				}
				
				String date=quotedate.getText();
				Assert.assertEquals(orderdates,date);
				String orderno=ordernum.getText();
				 String testString1 = orderno;
				  int startIndex1 = testString1.indexOf("#");
				  String subString1 = testString1.substring(startIndex1+1);
				  System.out.println(subString1);
				Assert.assertEquals(ordernumb,subString1);
				Assert.assertEquals(orderstat,quotestatus.getText());
				driver.navigate().back();
				Thread.sleep(5000);
			}
			
			else {
				driver.findElement(By.xpath("//p[contains(text(),'No quote requests have been submitted.')]")).isDisplayed();
				
			}
		}
			
		

	}
	
	public void Myquoterequestpaginationtest() throws InterruptedException {
		String subString;
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);
		MyQuoteRequests.click();
		Thread.sleep(10000);
		int size1=driver.findElements(By.xpath("//select[@class='form-control']//option")).size();
		 driver.findElement(By.xpath("//mat-select[@role='listbox']")).click();
		  driver.findElement(By.xpath("//span[contains(text(),'100')]")).click();
		for(int k=1;k<size1;k++) {
			System.out.println("bb");
			statusdropdown.get(k).click();
			String quotestatusss=statusdropdown.get(k).getText();
			Thread.sleep(5000);
		if(driver.findElements(By.xpath("//div[@class='mat-paginator-range-label']")).size()!=0) {
		String pagen=page.getText();
		  String testString = pagen;
		  int startIndex = testString.indexOf("of ");
		  subString = testString.substring(startIndex+3);
		  System.out.println(subString);
		  int sizee=Integer.valueOf(subString);
		  Thread.sleep(5000);
		  System.out.println(sizee);
		 int size=rows.size();
		 size--;
		 System.out.println(size);
		 Assert.assertEquals(size, sizee);
		 
		}
		else {
			driver.findElement(By.xpath("//p[contains(text(),'No quote requests have been submitted.')]")).isDisplayed();
		}
		}
	}
	
	public void approvependingquote() throws InterruptedException{
		
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);
		MyQuoteRequests.click();
		Thread.sleep(10000);
		statusdropdown.get(2).click();
		if(driver.findElements(By.xpath("//div[@class='mat-paginator-range-label']")).size()!=0) {
			String quotedate=quotenumber.get(0).getText();
			System.out.println(quotedate);
			String quotenum=quotenumber.get(1).getText();
			System.out.println(quotenum);
			String contact=quotenumber.get(2).getText();
			System.out.println(contact);
			String quotestats=quotenumber.get(3).getText();
			System.out.println(quotestats);
			String totalqty=quotenumber.get(5).getText();
			System.out.println(totalqty);
			String totalprice=quotenumber.get(6).getText();
			System.out.println(totalprice);
			driver.findElements(By.xpath("//mat-icon[@mattooltip='Approve']")).get(0).click();
			String quotedatedetails=driver.findElement(By.xpath("//div//strong[contains(text(),'quote Date:')]//parent::div//parent::div//h3")).getText();
			Assert.assertEquals(quotedate, quotedatedetails);
			String quotestatus=driver.findElement(By.xpath("//div//strong[contains(text(),'quote Status:')]//parent::div//parent::div//h3")).getText();
			Assert.assertEquals(quotestats, quotestatus);
			Thread.sleep(7000);
			String qtydetails=qty.get(0).getText();
			Assert.assertEquals(qtydetails, totalqty);
			String totaldetails=driver.findElement(By.xpath("//tr[12]//td[3]/strong")).getText();
			String testString=totaldetails;
			 int startIndex = testString.indexOf("$");
			 String subString = testString.substring(startIndex+1);
			 System.out.println(subString);
			Assert.assertEquals(subString, totalprice);
			driver.findElement(By.xpath("//div[@class='dst-image']//img")).isDisplayed();
			String shippingmeth=driver.findElement(By.xpath("//div//strong[contains(text(),'Shipping Method:')]//parent::div//parent::div//h3")).getText();
			String ship=driver.findElements(By.xpath("//mat-select//span")).get(1).getText();
			String testString1=ship;
			 int startIndex1 = testString1.indexOf("($");
			  int endIndex = testString.indexOf(")");
			 subString = testString1.substring(startIndex+2, endIndex);
			 System.out.println(subString);
			Assert.assertEquals(shippingmeth, subString);
			
		}
		else {
			driver.findElement(By.xpath("//p[contains(text(),'No quote requests have been submitted.')]")).isDisplayed();

		}
	}


}
