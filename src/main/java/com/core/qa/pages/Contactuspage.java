package com.core.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.core.qa.base.TestBase;

public class Contactuspage extends TestBase {
	
	@FindBy(xpath="//input[@formcontrolname='name']")
	WebElement name;
	
	@FindBy(xpath="//input[@formcontrolname='email']")
	WebElement email;
	
	@FindBy(xpath="//input[@formcontrolname='phone']")
	WebElement phone;
	
	@FindBy(xpath="//textarea[@formcontrolname='message']")
	WebElement message;
	
	@FindBy(xpath="//mat-error")
	WebElement error;
	
	@FindBy(xpath="//span[@role='checkbox']")
	WebElement captcha;
	
	@FindBy(xpath="//button[@class='mat-focus-indicator w-100 spinner-btn mat-flat-button mat-button-base mat-primary']")
	WebElement submit;
	
	@FindBy(xpath="//span[contains(text(),'Thanks for reaching out to us! Our team will get back to you to discuss your inquiry')]")
	WebElement Successmessage;
	
	
	public Contactuspage() {
		
		 PageFactory.initElements(driver, this);
	}
	
	 public String contactusurl()
	  {
		 return driver.getCurrentUrl();

	  }
	 
	 public boolean contactusformemptycheck()
	  {
		submit.click();
		return error.isDisplayed();

	  }
	 public boolean contactusformcheck()
	  {
		name.sendKeys("fayaqa");
		email.sendKeys(randomEmail());
		phone.sendKeys("(232) 132-1321 x32131");
		message.sendKeys("faya");
		captcha.click();
		submit.click();
		return Successmessage.isDisplayed();
		

	  }

}
