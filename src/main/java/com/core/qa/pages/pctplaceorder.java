package com.core.qa.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.TextArea;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.security.Key;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Mouse;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import com.core.qa.base.TestBase;
import com.paulhammant.ngwebdriver.NgWebDriver;

public class pctplaceorder extends TestBase{
	
	@FindBy(xpath="//span[contains(text(),'With Flat Embroidery')]//parent::div")
	WebElement withflatembroidrey;
	
	@FindBy(xpath="//span[contains(text(),'With 3D Embroidery')]//parent::div")
	WebElement threedembroidrey;
	
	@FindBy(xpath="//span[contains(text(),'Start Designing Now ')]//parent::button")
	WebElement startdesigning;
	
	@FindBy(xpath="//a[contains(text(),'Add Text')]//parent::li")
	WebElement addtext;
	
	@FindBy(xpath="//textarea")
	WebElement text;
	
	@FindBy(xpath="//span[contains(text(),' Add to Design ')]//parent::button")
	WebElement addtodesign;
	
	@FindBy(xpath="//a[contains(text(),'Clipart Library')]//parent::li")
	WebElement addclipart;
	
	@FindBys({
        @FindBy(xpath="//cdk-virtual-scroll-viewport//div//div[@class='preview-wrap d-inline-block ng-star-inserted']"),
 })
	 List<WebElement>clipartlib;
	
	@FindBys({
        @FindBy(xpath="//mat-card-content//li"),
 })
	 List<WebElement>clipartlib1;
	
	
	@FindBy(xpath="//a[contains(text(),'Back')]")
	WebElement back;
	
	@FindBy(xpath="//a[contains(text(),'Upload Picture')]//parent::li")
	WebElement uploadpic;
	
	@FindBy(xpath="//div[@class='dropzone dz-single dz-clickable']")
	WebElement upload;
	
	@FindBy(xpath="//span[contains(text(),'Finish Select Color')]//parent::button")
	WebElement finishselectcolour;
	
	@FindBy(xpath="//span[contains(text(),' Continue to Place Order ')]//parent::button")
	WebElement continuetoplaceorder;
	
	@FindBy(xpath="//span[contains(text(),' Sign In and Place Order ')]//parent::button")
	WebElement signinandplaceorder;
	
	@FindBy(xpath="//textarea[@placeholder='Enter a location']")
	WebElement address;
	
	@FindBy(xpath="//span[contains(text(),' Get Estimated Delivery Date and Shipping Cost ')]//parent::button")
	WebElement estimatebutton;
	
	@FindBy(xpath="//span[contains(text(),' Proceed to Checkout')]//parent::button")
	WebElement proceedtocheck;
	

	@FindBy(xpath="//span[contains(text(),' Continue to Submit Quote Request ')]//parent::button")
	WebElement submitquotebutton;
	

     @FindBy(xpath="//input[@formcontrolname='email']")
	 WebElement Email;
	
	@FindBy(xpath="//input[@formcontrolname='password']")
	 WebElement password;
	
	@FindBy(xpath="//span[contains(text(),'Sign In and Submit Quote Request')]//parent::span//parent::button")
	WebElement signinandsubmitquoterequestbutton;
	
	@FindBys({
        @FindBy(xpath="//div[@class='product-detail']//input[@type='number']"),
 })
	 List<WebElement>quantity;
	
	
	@FindBy(xpath="//span[contains(text(),' Continue to Submit Quote Request ')]//parent::span//parent::button")
	WebElement continuetosubmit;

	
	@FindBy(xpath="//input[@formcontrolname='phone']")
	WebElement phone;
	
	@FindBy(xpath="//input[@formcontrolname='zip']")
	WebElement zip;
	
	@FindBy(xpath="//span[contains(text(),' Submit Quote Request ')]//parent::span//parent::button")
	WebElement submitquote;
	
	@FindBy(xpath="//*[@id=\"app\"]/ngx-ecommerce-core-layout/mat-sidenav-container/mat-sidenav-content/div/app-raq-thankyou/div/div/h2")
	WebElement Thankyoupage;
	
	@FindBys({
        @FindBy(xpath="//span[contains(text(),' Place Order')]//parent::button"),
 })
	 List<WebElement>placeorder;
	
	public pctplaceorder() {
		
		 PageFactory.initElements(driver, this);
	}
	
	public boolean PCTOrdertest() throws InterruptedException, AWTException {
		Robot robot=new Robot();
		withflatembroidrey.click();
		startdesigning.click();
		 Thread.sleep(10000);
		uploadpic.click();
		 Thread.sleep(5000);
		 upload.click();
		 Thread.sleep(5000);
		 robot.setAutoDelay(1000);
		 
		 StringSelection stringselection=new StringSelection(System.getProperty("user.dir")+"\\Testdatafolder\\US_Flag_4.DST");
		 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection,null);
		 robot.setAutoDelay(2000);
		 
		
		 
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_V);
		 
		 robot.setAutoDelay(2000);

		 robot.keyPress(KeyEvent.VK_ENTER);
		 robot.keyPress(KeyEvent.VK_ENTER);
		 Thread.sleep(1000);
		 finishselectcolour.click();
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 continuetoplaceorder.click();
		 Thread.sleep(5000);

		 Email.sendKeys("fayaqa@yopmail.com");
		 password.sendKeys("123456789");
		 Thread.sleep(10000);

		 signinandplaceorder.click();
		 int Sum=0;
		 Random rand = new Random();
		 int j=0;
		 Thread.sleep(5000);
		// Addanothercolurqty.click();
		 int i;
		 for(i=0;i<quantity.size();i++) {
			 quantity.get(i).clear(); 
			 quantity.get(i).sendKeys(String.valueOf(getRandomInteger(1, 1)));
			
		 }
		continuetoplaceorder.click();
		
		 address.sendKeys("2330 South Archibald Avenue, Ontario, CA, USA");
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_DOWN);
		 robot.keyPress(KeyEvent.VK_ENTER);
		 Thread.sleep(5000);
		 estimatebutton.click();
		 Thread.sleep(10000);

		 proceedtocheck.click();
		 
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 placeorder.get(0).click();
		 return Thankyoupage.isDisplayed();

	}
	public boolean PCTOrdertest2() throws InterruptedException, AWTException {
		Robot robot=new Robot();
		threedembroidrey.click();
		startdesigning.click();
		 Thread.sleep(10000);
		 uploadpic.click();
		 Thread.sleep(5000);
		 upload.click();
		 Thread.sleep(5000);
		 robot.setAutoDelay(1000);
		 
		 StringSelection stringselection=new StringSelection(System.getProperty("user.dir")+"\\Testdatafolder\\US_Flag_4.DST");
		 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringselection,null);
		 robot.setAutoDelay(2000);
		 
		
		 
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_V);
		 
		 robot.setAutoDelay(2000);

		 robot.keyPress(KeyEvent.VK_ENTER);
		 robot.keyPress(KeyEvent.VK_ENTER);
		 Thread.sleep(1000);
		 finishselectcolour.click();
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 continuetoplaceorder.click();
		 Thread.sleep(5000);

		 Email.sendKeys("fayaqa@yopmail.com");
		 password.sendKeys("123456789");
		 Thread.sleep(10000);

		 signinandplaceorder.click();
		 int Sum=0;
		 Random rand = new Random();
		 int j=0;
		 Thread.sleep(5000);
		// Addanothercolurqty.click();
		 int i;
		 for(i=0;i<quantity.size();i++) {
			 quantity.get(i).clear(); 
			 quantity.get(i).sendKeys(String.valueOf(getRandomInteger(1, 1)));
			
		 }
		continuetoplaceorder.click();
	      Actions ac = new Actions(driver);

		 address.sendKeys("2330 South Archibald Avenue, Ontario, CA, USA");
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_DOWN);
		 robot.keyPress(KeyEvent.VK_ENTER);

		 Thread.sleep(5000);
		 estimatebutton.click();
		 Thread.sleep(10000);

		 proceedtocheck.click();
		 
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 placeorder.get(0).click();
		 return Thankyoupage.isDisplayed();

	}
	
}
