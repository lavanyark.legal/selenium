package com.core.qa.pages;

import java.util.List;

import org.openqa.selenium.By;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import com.core.qa.base.TestBase;

public class Shoppingcartpage extends TestBase {
	
	@FindBy(xpath="//div[@class='top-cart-wrap']//label[@class='fw-500 text-danger cart-total ng-star-inserted']")
	WebElement Minicart;
	
	@FindBys({
        @FindBy(xpath="//div[@class='mat-table cart-table ng-star-inserted']"),
 })
	 List<WebElement>Productblocks;
	
	
	@FindBys({
	@FindBy(xpath="//div[@class='cart-list-destopview ng-star-inserted']//div[@class='cart-product border-bottom pb-1 ng-star-inserted']//p[@class='mt-1 ng-star-inserted'][2]")
	 })
	 List<WebElement> Baseprice;
	
	@FindBys({
		@FindBy(xpath="//div[@class='cart-list-destopview ng-star-inserted']//div[@class='cart-product border-bottom pb-1 ng-star-inserted']//span[@class='update-items fw-500 d-block ng-star-inserted']")
		 })
		 List<WebElement> Additionalprice;
	@FindBys({
	@FindBy(xpath="//div[@class='cart-list-destopview ng-star-inserted']//div[@class='cart-product border-bottom pb-1 ng-star-inserted']//p[@class='mt-1 ng-star-inserted'][1]")
	 })
	 List<WebElement> Colour;

	@FindBys({
		@FindBy(xpath="//div[@class='cart-list-destopview ng-star-inserted']//div[@class='cart-product border-bottom pb-1 ng-star-inserted']//a[@class='product-name fw-300 cursor-pointer']//span[1]")
		 })
		 List<WebElement> Style;
	
	@FindBys({
		@FindBy(xpath="//div[@class='cart-list-destopview ng-star-inserted']//div[@class='cart-product border-bottom pb-1 ng-star-inserted']//input")
		 })
		 List<WebElement> Quantity;
	

	@FindBys({
		@FindBy(xpath="//div[@class='cart-list-destopview ng-star-inserted']//div[@class='cart-product border-bottom pb-1 ng-star-inserted']//div[@class='mat-cell text-center fw-500 ng-star-inserted']//p")
		 })
		 List<WebElement> Sizeprice;
	
	@FindBys({
		@FindBy(xpath="//div[@class='cart-list-destopview ng-star-inserted']//div[@class='cart-product border-bottom pb-1 ng-star-inserted']//div[@class='mat-cell text-center cart-update ng-star-inserted']//p")
		 })
		 List<WebElement> qty;
	
	@FindBys({
		@FindBy(xpath="//div[@class='cart-list-destopview ng-star-inserted']//div[@class='cart-product border-bottom pb-1 ng-star-inserted']//div[@class='mat-cell text-right fw-500 ng-star-inserted']/p[@class='text-danger text-right w-100 ng-star-inserted']")
		 })
		 List<WebElement> total;
	
	@FindBy(xpath="//div[@class='grand-total pl-2']//span")
	WebElement Subtotalqty;
	
	@FindBy(xpath="//div[@class='grand-total']//span")
	WebElement subtotal;
	
	@FindBy(xpath="//div[@class='free-shipping-msg p-1 text-center ng-star-inserted']")
	WebElement freeshipping;
	
	
	public String[] ShoppingcartStylecolourtest() {
		int size=Productblocks.size();
		for(int i=0;i<size;i++) {
			String styleshoppingcart=Style.get(i).getText();
			String colourshoppingcart=Colour.get(i).getText();
			String baseprice=Baseprice.get(i).getText();
			
			return new String[] {styleshoppingcart,colourshoppingcart,baseprice};
			
			
		}
		return null;
	
	}
	
	public int[] qtycheck() {
		  String subString = null ;
		int sum=0;
		int q=0;
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		int size=Productblocks.size();
		System.out.println(size);
		for(int i=0;i<size;i++) {
			
			 q=Integer.parseInt(qty.get(i).getText());
			
			String qty1="//div[@class='cart-list-destopview ng-star-inserted']//div[@class='cart-product border-bottom pb-1 ng-star-inserted'][";
			String qty2="]//div//input";
			List<WebElement>QTY3=driver.findElements(By.xpath(qty1+i+qty2));
			
			for(int j=0;j<QTY3.size();j++) {
				
				sum=sum+Integer.parseInt(Quantity.get(j).getText());
			}
			
			String subtotalqt=Subtotalqty.getText();
			
			  String testString = subtotalqt;
			  int startIndex = testString.indexOf("(");
			  int endIndex = testString.indexOf(")");
			 subString = testString.substring(startIndex+1, endIndex);
			System.out.println(subString);
		}
		return new int[] {Integer.parseInt(subString),sum};
		
	}
	

}
