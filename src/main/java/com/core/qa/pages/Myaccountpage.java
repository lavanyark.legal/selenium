package com.core.qa.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import com.core.qa.base.TestBase;

public class Myaccountpage extends TestBase {
	
	@FindBys({
	    @FindBy(xpath="//mat-icon//parent::span[@class='mat-button-wrapper']//parent::a"),})
		 List<WebElement>accountdropdown;
	
	@FindBy(xpath="//a[contains(text(),'Account Dashboard')]//parent::div")
	WebElement Accountdashboard;
	
	@FindBy(xpath="//div[@class='account-dashboard-wrap']")
	WebElement Accountdashboardtable;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'My Orders')]")
	WebElement MyOrders;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'Subscription')]")
	WebElement Subscription;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'My Quote Requests')]")
	WebElement MyQuoteRequests;
	
	@FindBy(xpath="//h2[contains(text(),'My Quote Requests')]")
	WebElement header;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'My Wishlist')]")
	WebElement MyWishlist;
	
	@FindBy(xpath="//h2[contains(text(),'My Wishlist')]")
	WebElement wishlist;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'My Saved Cards')]")
	WebElement mysavedcards;
	
	
	@FindBy(xpath="//span[contains(text(),' Add New Card')]//parent::button")
	WebElement addnewcard;
	
	@FindBy(xpath="//input[@formcontrolname='cardNumber']")
	WebElement cardnumber;
	
	@FindBy(xpath="//input[@formcontrolname='cardHolderName']")
	WebElement cardHolderName;
	
	@FindBy(xpath="//mat-select[@formcontrolname='expirationMonth']")
	WebElement expirationmonth;
	
	@FindBy(xpath="//span[contains(text(),'June')]")
	WebElement june;
	
	@FindBy(xpath="//mat-select[@formcontrolname='expirationYear']")
	WebElement expirationYear;
	
	@FindBy(xpath="//span[contains(text(),'2029')]")
	WebElement year;
	
	@FindBy(xpath="//input[@formcontrolname='cvv']")
	WebElement cvv;
	
	@FindBys({
        @FindBy(xpath="//mat-radio-group"),
 })
	 List<WebElement>address;
	
	@FindBy(xpath="//a[contains(text(),'+Add New Address')]")
	WebElement addnewaddress;
	
	@FindBy(xpath="//input[@formcontrolname='name']")
	WebElement name;
	
	@FindBy(xpath="//div[@class='save-card-popup ng-star-inserted']//input[@formcontrolname='email']")
	WebElement email;
	
	@FindBy(xpath="//input[@formcontrolname='company_name']")
	WebElement company_name;
	
	@FindBy(xpath="//input[@formcontrolname='address_1']")
	WebElement address_1;
	
	@FindBy(xpath="//input[@formcontrolname='city']")
	WebElement city;
	
	@FindBy(xpath="//mat-select[@formcontrolname='state']")
	WebElement state;
	
	@FindBy(xpath="//span[contains(text(),'Louisiana')]")
	WebElement  Louisiana ;
	
	@FindBy(xpath="//input[@formcontrolname='zip']")
	WebElement zip;
	
	@FindBy(xpath="//input[@formcontrolname='phone']")
	WebElement phone;
	
	@FindBy(xpath="//span[contains(text(),'Save')]//parent::button")
	WebElement save;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'Account Information')]")
	WebElement AccountInformation;
		
	@FindBy(xpath="//h2[contains(text(),'Account Information')]")
	WebElement accountinfo ;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'Address Book')]")
	WebElement AddressBook ;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'FAQ - Customer Order Process')]")
	WebElement faq ;
	
	@FindBy(xpath="//h1[contains(text(),'FAQ - Custom Embroidery Order')]")
	WebElement faqheader ;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'Log Out')]")
	WebElement Logout ;
	
	@FindBys({
	@FindBy(xpath="//input[@placeholder='Search']"),
	 })
	List<WebElement> Search ;
	
	@FindBys({
		@FindBy(xpath="//h2[contains(text(),'My Custom Orders')]//parent::div//parent::ngx-ecommerce-core-orders//div[@class='pct-quote-request ng-star-inserted']//td"),
		 })
	List<WebElement> customordernumber ;
	
	@FindBys({
		@FindBy(xpath="//ngx-ecommerce-core-orders/quote-request-table/div/table/tbody/tr//td"),
		 })
	List<WebElement> blankdernumber ;
	
	@FindBys({
		@FindBy(xpath="//mat-icon[contains(text(),'search')]//parent::button"),
		 })
	List<WebElement> searchbutton ;
	
	@FindBys({
		@FindBy(xpath="//select[@class='form-control']"),
		 })
	List<WebElement> statusdropdown ;
	
	
	@FindBys({
		@FindBy(xpath="//option[@value='order__customer_orders']"),
		 })
	List<WebElement> orderprocessing ;
	
	@FindBys({
		@FindBy(xpath="//h2[contains(text(),'My Custom Orders')]//parent::div//parent::ngx-ecommerce-core-orders//div[@class='pct-quote-request ng-star-inserted']//tr"),
		 })
	List<WebElement> customrows ;
	@FindBys({
		@FindBy(xpath="//ngx-ecommerce-core-orders/quote-request-table/div/table/tbody/tr"),
		 })
	List<WebElement> blankrows ;
	@FindBys({
		@FindBy(xpath="//div[@class='mat-paginator-range-label']"),
		 })
	List<WebElement> page ;
	
	@FindBys({
		@FindBy(xpath="//mat-icon[@mattooltip='View']"),
		 })
	List<WebElement> view ;
	

		@FindBy(xpath="//ngx-ecommerce-core-orders/quote-request-table/div/table/tbody/tr//td//mat-icon[@mattooltip='View']")
	
	WebElement blankview ;
	
	@FindBy(xpath="//h2//strong")
	WebElement ordernum ;
	
	@FindBy(xpath="//div//strong[contains(text(),'Order Date:')]//parent::div//parent::div//h3")
	WebElement orderdate ;
	
	@FindBy(xpath="//div//strong[contains(text(),'Order Status:')]//parent::div//parent::div//h3")
	WebElement orderstatus ;
	
	@FindBy(xpath="//h2[contains(text(),'My Subscriptions')]")
	WebElement subscriptionsheader;
	
	@FindBys({
		@FindBy(xpath="//tr[@class='total-quantity']//strong"),
		 })
	List<WebElement> qty ;
	
	@FindBys({
		@FindBy(xpath="//strong[@class='font-xl ng-star-inserted']"),
		 })
	List<WebElement> grandtotal ;
	
	public Myaccountpage() {
		
		 PageFactory.initElements(driver, this);
	}
	
	public boolean accountdashboardtest() throws InterruptedException {
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);
		MyOrders.click();
		Accountdashboard.click();
		return Accountdashboardtable.isDisplayed();
	}

	public String myorderstest() throws InterruptedException {
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);
		MyOrders.click();
		Thread.sleep(5000);
		return driver.getCurrentUrl();
	}
	
	public boolean myquoterequesttest() throws InterruptedException {
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);

		MyQuoteRequests.click();
		Thread.sleep(5000);
		return header.isDisplayed();
	}
	
	public boolean mywishlist() throws InterruptedException {
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);
		MyWishlist.click();
		Thread.sleep(5000);
		return wishlist.isDisplayed();
	}
	
	public void mysavedcards() throws InterruptedException {
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);
		mysavedcards.click();
		addnewcard.click();
		cardnumber.sendKeys("4111111111111111");
		cardHolderName.sendKeys("fayaqa");
		expirationmonth.click();
		june.click();
		expirationYear.click();
		year.click();
		cvv.sendKeys("111");
		address.get(0).click();
		save.click();
		}
	
	public void mysavedcards2() throws InterruptedException {
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);
		mysavedcards.click();
		addnewcard.click();
		cardnumber.sendKeys("4111111111111111");
		cardHolderName.sendKeys("fayaqa");
		expirationmonth.click();
		june.click();
		expirationYear.click();
		year.click();
		cvv.sendKeys("111");
		addnewaddress.click();
		name.sendKeys("faya");
		email.sendKeys("fayaqa@yopmail.com");
		company_name.sendKeys("faya");
		address_1.sendKeys("155 Robert St");
		city.sendKeys("Slidell");
		state.click();
		Thread.sleep(5000);
		Louisiana.click();
		zip.sendKeys("70458");
		phone.sendKeys("(432) 432-4324 x23423");
		save.click();
		
		}
	
	public boolean accountinfotest() throws InterruptedException {
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);
		AccountInformation.click();
		return accountinfo.isDisplayed();
		
		}
	
	public void addresstest() throws InterruptedException {
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);
		AddressBook.click();
		addnewaddress.click();
		name.sendKeys("faya");
		company_name.sendKeys("faya");
		address_1.sendKeys("155 Robert St");
		city.sendKeys("Slidell");
		state.click();
		Thread.sleep(5000);
		Louisiana.click();
		zip.sendKeys("70458");
		phone.sendKeys("(432) 432-4324 x23423");
		Thread.sleep(5000);
		save.click();
		
	}
	
	public boolean subscriptiontest() throws InterruptedException {
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);
		Subscription.click();
		return subscriptionsheader.isDisplayed();
	}
	
	public boolean faqtest() throws InterruptedException {
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);
		faq.click();
		return faqheader.isDisplayed();
	}
	
	public void logouttest() throws InterruptedException {
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);
		Logout.click();
	}
}
