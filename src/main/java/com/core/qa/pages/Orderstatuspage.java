package com.core.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.core.qa.base.TestBase;

public class Orderstatuspage extends TestBase {
	
	@FindBy(linkText="Order Status")
	WebElement orderstatus;
	
	@FindBy(xpath="//input[@formcontrolname='orderId']")
	WebElement orderid;
	
	@FindBy(xpath="//input[@formcontrolname='zip']")
	WebElement zip;
	
	@FindBy(xpath="//span[contains(text(),' Check Status ')]//parent::button")
	WebElement checkstatusbutton;
	
	@FindBy(xpath="//span[contains(text(),' Check Status ')]//parent::button")
	WebElement orderinformation;
	
	@FindBy(xpath="//div[contains(text(),' Product Name & Descriptions')]")
	WebElement productnameanddescription;
	
	@FindBy(xpath="//mat-error")
	WebElement error;
	
	public Orderstatuspage() {
		
		 PageFactory.initElements(driver, this);
	}
	
	public boolean emptyformcheck() {
		orderstatus.click();
		checkstatusbutton.click();
		return error.isDisplayed();
	}
	
	public void orderdetailscheck(String orderids,String zipcode) throws InterruptedException {
		Thread.sleep(5000);
		orderstatus.click();
		orderid.sendKeys(orderids);
		zip.sendKeys(zipcode);
		//checkstatusbutton.click();
		orderinformation.isDisplayed();
		productnameanddescription.isDisplayed();
		
	}


}
