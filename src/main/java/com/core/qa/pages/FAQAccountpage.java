package com.core.qa.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.core.qa.base.TestBase;

public class FAQAccountpage extends TestBase {

	@FindBys({
	    @FindBy(xpath="//mat-icon//parent::span[@class='mat-button-wrapper']//parent::a"),})
		 List<WebElement>accountdropdown;
	
	@FindBy(xpath="//a[contains(text(),'Account Dashboard')]//parent::div")
	WebElement Accountdashboard;
	
	@FindBy(xpath="//div[@class='account-dashboard-wrap']")
	WebElement Accountdashboardtable;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'My Orders')]")
	WebElement MyOrders;
	
	@FindBy(xpath="//a[@role='menuitem']//span[contains(text(),'FAQ - Customer Order Process')]")
	WebElement faq ;
	
	@FindBy(xpath="//h1[contains(text(),'FAQ - Custom Embroidery Order')]")
	WebElement faqheader ;
	
	@FindBys({
	    @FindBy(xpath="//a[contains(text(),'Request a Quote')]"),})
		 List<WebElement>requestaquotelink;
	
	@FindBys({
	    @FindBy(xpath="//a[contains(text(),'Create Your Own')]"),})
		 List<WebElement>createyourown;
	
	@FindBy(xpath="//img[@title='Close']")
	WebElement close ;
	
	@FindBy(xpath="//h4[contains(text(),'Create Your Own - Step 1 Select Your Style')]")
	WebElement createyourownheader ;
	
	@FindBys({
	    @FindBy(xpath="//a[contains(text(),'My Quotes Requests')]"),})
		 List<WebElement>myquoterequestlink;
	
	@FindBy(xpath="//h2[contains(text(),'My Quote Requests')]")
	WebElement myquoterequestheader;
	
	@FindBys({
	    @FindBy(xpath="//a[contains(text(),'“My Orders”')]"),})
		 List<WebElement>myorderslink;
	
	@FindBy(xpath="//h2[contains(text(),'My Custom Orders')]")
	WebElement ordersheader;
	
	public FAQAccountpage() {
		
		 PageFactory.initElements(driver, this);
	}
	
	public boolean faqtest() throws InterruptedException {
		Thread.sleep(5000);
		accountdropdown.get(0).click();
		Thread.sleep(5000);
		faq.click();
		return faqheader.isDisplayed();
	}
	public void	 requestaquotelink() throws InterruptedException {
	
		int linksize=requestaquotelink.size();
		for(int i=0;i<linksize;i++) {
			requestaquotelink.get(i).click();
			Thread.sleep(5000);
			String url= driver.getCurrentUrl();
			Assert.assertEquals(url,prop.getProperty("url")+"raq");
			driver.navigate().back();
			 
		}
		
	}
	
	public void	 createyourownlink() throws InterruptedException {
		
		int linksize=createyourown.size();
		System.out.println(linksize);
		for(int i=0;i<linksize;i++) {
			createyourown.get(i).click();
			Thread.sleep(5000);
			createyourownheader.isDisplayed();
			 close.click();
		}
		
	}
	public void	 myquoterequestlink() throws InterruptedException {
		
		int linksize=myquoterequestlink.size();
		System.out.println(linksize);
		for(int i=0;i<linksize;i++) {
			myquoterequestlink.get(i).click();
			Thread.sleep(5000);
			myquoterequestheader.isDisplayed();
			driver.navigate().back();

		}
		
	}
public void	 myorderslink() throws InterruptedException {
		
		int linksize=myorderslink.size();
		System.out.println(linksize);
		for(int i=0;i<linksize;i++) {
			myorderslink.get(i).click();
			Thread.sleep(5000);
			ordersheader.isDisplayed();
			driver.navigate().back();

		}
		
	}
}
