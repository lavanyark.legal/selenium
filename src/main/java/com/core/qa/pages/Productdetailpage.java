package com.core.qa.pages;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import com.core.qa.base.TestBase;

public class Productdetailpage extends TestBase {
	
	@FindBy(xpath="//div[@class='review-wrapper']//div")
	WebElement StyleNo;
	
	@FindBy(xpath="//div[@class='review-wrapper']//h1")
	WebElement Stylename;
	
	@FindBy(xpath="//div[@class='login-wrap quote-request-login-form']")
	WebElement Signinpopup;
	
	@FindBy(xpath="//div[@class='details-icon font-xs fw-400 description-icon ng-star-inserted']")
	WebElement Viewfulldescription;
	
	@FindBy(xpath="//div[@class='details-icon mr-1']")
	WebElement Wishlist;
	
	@FindBys({
        @FindBy(xpath="//div[@class='details-icon mr-1 ng-star-inserted']"),
 })
	 List<WebElement>Compare;
	
	@FindBy(xpath="//div[@class='product-save_banner ng-star-inserted']//p")
	WebElement Price;
	
	@FindBys({
        @FindBy(xpath="//div[@class='color-list w-100']//div[@class='preview-wrap d-inline-block ng-star-inserted']"),
 })
	 List<WebElement>Colours;
	
	@FindBys({
	@FindBy(xpath="//div[@class='product-detail']//span[@class='float-left fw-500 font-lg mr-1']")
	})
	 List<WebElement>Colourtext;
	
	@FindBy(xpath="//a[contains(text(),'Change Color')]")
	WebElement Changecolour;
	
	@FindBys({
        @FindBy(xpath="//div[@class='product-detail']//input"),
 })
	 List<WebElement>quantity;
	
	@FindBy(xpath="//a[contains(text(),'Add Another Color & Quantity')]")
	WebElement Addanothercolurqty;
	
	@FindBys({
        @FindBy(xpath="//div[@class='color-dialog-wrap ng-star-inserted']//div[@class='color-list ng-star-inserted']//div"),
 })
	 List<WebElement>Changecolourpopupcolour;
	
	@FindBy(xpath="//div[@class='color-dialog-wrap ng-star-inserted']//span[@class='fw-500']")
	
	
	 WebElement Changecolourtext;
	
	@FindBy(xpath="//div[@class='color-dialog-wrap ng-star-inserted']//button")
	WebElement button;
	
	@FindBys({
        @FindBy(xpath="//table[@class='w-100 price-table fw-400']//td"),
 })
	 List<WebElement>tierprice;
	
	@FindBy(xpath="//div[@class='mt-1 mb-1 w-100']//button")
	WebElement Addtocart;
	
	@FindBy(xpath="//span[contains(text(),'Please enter Quantity')]")
	WebElement Errormessage;
	
	@FindBys({
	@FindBy(xpath="//span[@class='d-block fw-500 item-price ng-star-inserted']")
	 })
	List<WebElement> pricetier;
	
	@FindBys({
        @FindBy(xpath="//div[@class='add-to-cart-wrap']//div[@class='mr-2 size-label ng-star-inserted']"),
 })
	 List<WebElement>Addtocartqtypopup;
	
	@FindBys({
	@FindBy(xpath="//button[2]")
	 })
	List<WebElement>proceedtocheckout;
	
	@FindBy(xpath="//mat-form-field//input[@formcontrolname='email']")
	 WebElement Email;
	
	@FindBy(xpath="//input[@formcontrolname='password']")
	WebElement password;
	
	@FindBy(xpath="//button[@type='submit']")
	WebElement Signinbutton;
	
	@FindBy(xpath="//span[contains(text(),'Product has been added to Wishlist')]")
	WebElement Successmessage;
	
	@FindBy(xpath="//span[contains(text(),'Product url copied to clipboard')]")
	WebElement shareSuccessmessage;
	
	@FindBy(xpath="//div//mat-icon[contains(text(),'share')]//parent::div")
	WebElement share;
	
	 public Productdetailpage() {
		  PageFactory.initElements(driver, this);
	}
	 
	 public void Viewfulldescriptionlinktest() {
		 Viewfulldescription.click();
	 }
	 
	 public boolean Wishlisttest() {
		 Wishlist.click();
		return Signinpopup.isDisplayed();
	 }
	 
	 public boolean Wishlisttest2() {
		 Wishlist.click();
		 Email.sendKeys(prop.getProperty("Emailid"));
		 password.sendKeys(prop.getProperty("password"));
		 Signinbutton.click();
		 return Successmessage.isDisplayed();
	 }
	 
	 public boolean sharetest() {
		share.click();
		return shareSuccessmessage.isDisplayed();
	 }
	 
	 public void Specsheettest() {
			Compare.get(1).click();
		 }
	 public void Productmeasurementtest() {
			Compare.get(2).click();
		 }
	 
	 public String[] lowaspricetest() throws InterruptedException {
		 String tierpricelast;
		 String savebannerprice=Price.getText();
		 Thread.sleep(1000);
			 tierpricelast=tierprice.get(5).getText();
		 
	
		
		 return new String[] {savebannerprice,tierpricelast}; 
		 
	 }
	 
	 public String[] Selectedcolourtest() throws InterruptedException {
		 Colours.get(1).click();
		 String colurname=Colours.get(1).getText();
		 String colournameqtytab=Colourtext.get(0).getText();
		 
		 return new String[] {colurname,colournameqtytab}; 
		 
	 }
	 public String[] Changecolourtest() throws InterruptedException {
		 Changecolour.click();
		 Changecolourpopupcolour.get(3).click();
		 String colurname=Changecolourtext.getText();
		 button.click();
		 String colournamechangecolour=Colourtext.get(0).getText();
		 
		 return new String[] {colurname,colournamechangecolour}; 
		 
	 }
	 public String[] addanothercolurtest() throws InterruptedException {
		 Addanothercolurqty.click();
		 Changecolourpopupcolour.get(3).click();
		 String colurname=Changecolourtext.getText();
		 button.click();
		 String colournamechangecolour=Colourtext.get(1).getText();
		 
		 return new String[] {colurname,colournamechangecolour}; 
		 
	 }
	 
	 public String[] SingleProductdiffcoluraddtocart() throws InterruptedException {
		 int Sum=0;
		 Random rand = new Random();
		 int j=0;
		 Addtocart.click();
		 Errormessage.isDisplayed();
		 for(int i=0;i<quantity.size();i++) {
//			 While(driver.findElements(By.xpath("//span[@class='stock-details text-danger']")).get(i));
//			 {
//				 quantity.get(i).sendKeys(Keys.TAB);				 
//				 i++;
//
//			 }
			 quantity.get(i).sendKeys(String.valueOf(getRandomInteger(1, 3)));
			
	 
		 }
		 String Totalquantity=driver.findElement(By.xpath("//div[@class='mt-1 mb-1 w-100']//span[@class='fw-500 text-danger']")).getText();
		 System.out.println(Totalquantity);
		 Addtocart.click();

		 List<WebElement> total=driver.findElements(By.xpath("//div[@class='mr-2 size-label ng-star-inserted']//strong"));
		 Thread.sleep(5000);
		 System.out.println(total.size());
		 Sum=0;

		 for(int k=0;k<total.size();k++) {
			 
			 Sum=Sum+Integer.parseInt(String.valueOf(total.get(k).getText()));
			 System.out.println(Sum);
		 }
		
		 
		// proceedtocheckout.get(1).click();
		 return new String[] {String.valueOf(Sum),Totalquantity}; 
	 }


	 

}
