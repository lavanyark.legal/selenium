package com.core.qa.pages;


import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import com.core.qa.base.TestBase;


public class Signinpage extends TestBase {
	//PageFactory-OR:
	
		
		@FindBy(linkText="Sign In")
		WebElement SignIn;
		
		@FindBy(xpath="//mat-form-field//input[@formcontrolname='email']")
		 WebElement Email;
			
		@FindBy(xpath="//input[@formcontrolname='password']")
		WebElement password;
		
		@FindBy(xpath="//button[@type='submit']")
		WebElement Signinbutton;
		
		@FindBy(xpath="//span[contains(text(),'Logged in successfully')]")
		WebElement Successmessage;
		
		@FindBy(xpath="//span[contains(text(),'Invalid Email / Password')]")
		WebElement Errormessage;
		
		@FindBy(xpath="//span[@class='forgot-password-link fw-400 float-right']")
		WebElement Forgotpassword;
		
		@FindBy(xpath="//div[@class='sign-in-wrap']//button")
		WebElement Resetpasswordbutton;
		
		@FindBy(xpath="//span[contains(text(),'Password reset link has be sent to your email')]")
		WebElement passwordsenttomail;
		
		@FindBy(xpath="//span[contains(text(),'Your Email is not registered with us')]")
		WebElement Emailnotregistered;
		
		@FindBy(xpath="//mat-error")
		WebElement error;
		
		@FindBy(xpath="//a[contains(text(),'Back')]")
		WebElement Back;

		@FindBy(xpath="//span[contains(text(),'Sign In Or Create Your Account')]")
		WebElement signinpopup;
		
		@FindBy(xpath="//span[contains(text(),' Create Your Account ')]")
		WebElement createyouraccount;
		
		@FindBy(xpath="//h2[contains(text(),' Create New Account ')]")
		WebElement createnewaccountpage;
	
	  public Signinpage() {
		  PageFactory.initElements(driver, this);
	}
	  
	  public boolean Signin1(String un,String password2) throws InterruptedException
	  {
		  SignIn.click();
		  Email.clear();
		  password.clear();
		  Email.sendKeys(un);
		  password.sendKeys(password2);
		  Signinbutton.click();
		  Thread.sleep(1000);
		  return Successmessage.isDisplayed();	  
	  }
	  
	  public boolean Signin2(String un,String pwd) throws InterruptedException
	  {
		  SignIn.click();
		  Email.clear();
		  password.clear();
		  Email.sendKeys(un);
		  password.sendKeys(pwd);
		  Signinbutton.click();
		  Thread.sleep(1000);
		  return Errormessage.isDisplayed();	  
	  }
	  
	  public boolean Signin3(String un) throws InterruptedException
	  {
		  SignIn.click();
		  Forgotpassword.click();
		  Email.clear();
		  Email.sendKeys(un);
		  Resetpasswordbutton.click();
		  return passwordsenttomail.isDisplayed();
	  }
	  
	  public boolean Signin4(String un) throws InterruptedException
	  {
		  SignIn.click();
		  Forgotpassword.click();
		  Email.clear();
		  Email.sendKeys(un);
		  Resetpasswordbutton.click();
		  return Emailnotregistered.isDisplayed();
	  }
	  
	  public boolean Signin5() throws InterruptedException
	  {
		  SignIn.click();
		  Email.clear();
		  password.clear(); 
		  Signinbutton.click();
		  Thread.sleep(1000);
		  return error.isDisplayed();	  
	  }
	  
	  public boolean Signin6() throws InterruptedException
	  {
		  SignIn.click();
		  Forgotpassword.click();
		  Back.click();
		  return signinpopup.isDisplayed();
		  	  
	  }
	  
	  public boolean Signin7() throws InterruptedException
	  {
		  SignIn.click();
		  createyouraccount.click();
		  return createnewaccountpage.isDisplayed();
		  	  
	  }

}
