package com.core.qa.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import com.core.qa.base.TestBase;

public class Createanaccountpage extends TestBase{
	//PageFactory-OR:
	
	
			@FindBy(linkText="Create an Account")
			WebElement createanaccount;
			
			@FindBy(xpath="//input[@formcontrolname='first_name']")
			WebElement firstname;
			
			@FindBy(xpath="//input[@formcontrolname='last_name']")
			WebElement lastname;
			
			@FindBys({
		        @FindBy(xpath="//input[@formcontrolname='email']"),
		 })
			 List<WebElement>Email;
			
			
			@FindBy(xpath="//input[@formcontrolname='password']")
			WebElement password;
			
			@FindBy(xpath="//input[@formcontrolname='confirm_password']")
			WebElement confirmpassword;
			
		
		      @FindBy(xpath="//button[@class='mat-focus-indicator mat-flat-button mat-button-base mat-primary']")
			 WebElement createaccountbutton;
			

			@FindBy(xpath="//span[contains(text(),'You registered successfully!')]")
			WebElement Successmessage;
			
			@FindBy(xpath="//span[contains(text(),'Email already registered. Please login.')]")
			WebElement Alreadyregistered;
			
			@FindBy(xpath="//span[@class='mat-checkbox-label']")
			WebElement Newslettercheckbox;
			
			@FindBy(xpath="//mat-error[@role='alert']")
			WebElement Error;
			
			@FindBy(xpath="//span[contains(text(),'Thank You for subscribing!')]")
			WebElement Successsubcriptionmessage;
			
			
			public Createanaccountpage() {
				
				 PageFactory.initElements(driver, this);
			}
			
			  
			  public boolean createaccount1(String fname,String lname,String mail,String pass,String cfmpass)
			  {
				  createanaccount.click();
				  firstname.clear();
				  lastname.clear();
				  Email.clear();
				  password.clear();
				  confirmpassword.clear();
				  firstname.sendKeys(fname);
				  lastname.sendKeys(lname);
				  Email.get(0).sendKeys(mail);
				  password.sendKeys(pass);
				  confirmpassword.sendKeys(cfmpass);
				  createaccountbutton.click();
				  return Successmessage.isDisplayed();
	
			  }
			  
			  public boolean createaccount2(String fname,String lname,String mail,String pass,String cfmpass) throws InterruptedException
			  {
				  Thread.sleep(5000);
				  createanaccount.click();
				  firstname.clear();
				  lastname.clear();
				  Email.clear();
				  password.clear();
				  confirmpassword.clear();
				  firstname.sendKeys(fname);
				  lastname.sendKeys(lname);
				  Email.get(0).sendKeys(mail);
				  password.sendKeys(pass);
				  confirmpassword.sendKeys(cfmpass);
				  createaccountbutton.click();
				  Thread.sleep(1000);
				  return Alreadyregistered.isDisplayed();
	
			  }
			  public boolean createaccount3(String fname,String lname,String mail,String pass,String cfmpass)
			  {
				  createanaccount.click();
				  firstname.clear();
				  lastname.clear();
				  Email.clear();
				  password.clear();
				  confirmpassword.clear();
				  firstname.sendKeys(fname);
				  lastname.sendKeys(lname);
				  Email.get(0).sendKeys(mail);
				  password.sendKeys(pass);
				  confirmpassword.sendKeys(cfmpass);
				  createaccountbutton.click();
				  return Error.isDisplayed();
	
			  }
			  public boolean createaccount4(String fname,String lname,String mail,String pass,String cfmpass) throws InterruptedException
			  {
				  createanaccount.click();
				  firstname.clear();
				  lastname.clear();
				  Email.clear();
				  password.clear();
				  confirmpassword.clear();
				  firstname.sendKeys(fname);
				  lastname.sendKeys(lname);
				  Email.get(0).sendKeys(mail);
				  password.sendKeys(pass);
				  confirmpassword.sendKeys(cfmpass);
				  Newslettercheckbox.click();
				  createaccountbutton.click();
				  Thread.sleep(3000);
				  return Successsubcriptionmessage.isDisplayed();
	
			  }
			  
			  public boolean createaccount5()
			  {
				  createanaccount.click();
				  firstname.clear();
				  lastname.clear();
				  Email.clear();
				  password.clear();
				  confirmpassword.clear();
				  createaccountbutton.click();
				  return Error.isDisplayed();
	
			  }

}

