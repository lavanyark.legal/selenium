package com.core.qa.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.math.RoundingMode;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.DataProvider;

import com.core.qa.base.TestBase;
import com.core.qa.util.TestUtil;
import com.paulhammant.ngwebdriver.NgWebDriver;

public class GuestAddtocartBlank extends TestBase {
	
	@FindBy(xpath="//div[@class='review-wrapper']//div")
	WebElement StyleNo;
	
	@FindBy(xpath="//div[@class='review-wrapper']//h1")
	WebElement Stylename;
	
	@FindBy(xpath="//div[@class='login-wrap quote-request-login-form']")
	WebElement Signinpopup;
	
	@FindBy(xpath="//div[@class='details-icon font-xs fw-400 description-icon ng-star-inserted']")
	WebElement Viewfulldescription;
	
	@FindBy(xpath="//div[@class='details-icon mr-1']")
	WebElement Wishlist;
	
	@FindBys({
        @FindBy(xpath="//div[@class='details-icon mr-1 ng-star-inserted']"),
 })
	 List<WebElement>Compare;
	
	@FindBy(xpath="//div[@class='product-save_banner ng-star-inserted']//p")
	WebElement Price;
	
	@FindBys({
        @FindBy(xpath="//div[@class='color-list w-100']//div[@class='preview-wrap d-inline-block ng-star-inserted']"),
 })
	 List<WebElement>Colours;
	
	@FindBys({
	@FindBy(xpath="//div[@class='product-detail']//span[@class='float-left fw-500 font-lg mr-1']")
	})
	 List<WebElement>Colourtext;
	
	@FindBy(xpath="//a[contains(text(),'Change Color')]")
	WebElement Changecolour;
	
	@FindBys({
        @FindBy(xpath="//div[@class='product-detail']//input"),
 })
	 List<WebElement>quantity;
	
	@FindBy(xpath="//a[contains(text(),'Add Another Color & Quantity')]")
	WebElement Addanothercolurqty;
	
	@FindBys({
        @FindBy(xpath="//div[@class='color-dialog-wrap ng-star-inserted']//div[@class='color-list ng-star-inserted']//div"),
 })
	 List<WebElement>Changecolourpopupcolour;
	
	@FindBy(xpath="//div[@class='color-dialog-wrap ng-star-inserted']//span[@class='fw-500']")
	
	
	 WebElement Changecolourtext;
	
	@FindBy(xpath="//div[@class='color-dialog-wrap ng-star-inserted']//button")
	WebElement button;
	
	@FindBys({
        @FindBy(xpath="//table[@class='w-100 price-table fw-400']//td"),
 })
	 List<WebElement>tierprice;
	
	@FindBy(xpath="//div[@class='mt-1 mb-1 w-100']//button")
	WebElement Addtocart;
	
	@FindBy(xpath="//span[contains(text(),'Please Enter Quantity')]")
	WebElement Errormessage;
	
	@FindBys({
        @FindBy(xpath="//div[@class='add-to-cart-wrap']//div[@class='mr-2 size-label ng-star-inserted']"),
 })
	 List<WebElement>Addtocartqtypopup;
	

	@FindBy(xpath="//span[contains(text(),' Proceed to Checkout')]//parent::button")
	
	WebElement proceedtocheckout;
	

	@FindBys({
        @FindBy(xpath="//div[@class='mat-table cart-table shadow ng-star-inserted']//div[@class='cart-product border-bottom pb-1 ng-star-inserted']"),
 })
	 List<WebElement>Productblocks;
	
	@FindBys({
		@FindBy(xpath="//div[@class='cart-list-destopview ng-star-inserted']//div[@class='cart-product border-bottom pb-1 ng-star-inserted']//input")
		 })
		 List<WebElement> Quantity;
	
	@FindBys({
		@FindBy(xpath="//div[@class='cart-list-destopview ng-star-inserted']//div[@class='cart-product border-bottom pb-1 ng-star-inserted']//div[@class='mat-cell text-center cart-update ng-star-inserted']//p[@class='d-block text-center pb-1 ng-star-inserted']")
		 })
		 List<WebElement> qty;
	
	@FindBy(xpath="//div[@class='grand-total pl-2']//span")
	WebElement Subtotalqty;
	
	@FindBys({
		@FindBy(xpath="//div[@class='cart-list-destopview ng-star-inserted']//div[@class='cart-product border-bottom pb-1 ng-star-inserted']//div[@class='mat-cell text-right fw-500 ng-star-inserted']//p[@class='text-danger text-right w-100 pb-1 ng-star-inserted']")
		 })
		 List<WebElement> total;
	
	@FindBy(xpath="//div[@class='grand-total pl-2']//span[@class='fw-500 text-danger total-price-wrapper']")
	WebElement subtotal;
	
	@FindBy(xpath="//div//a[@class='fw-500 text-danger thankyou-links']")
	WebElement ordernum;

	@FindBys({
		@FindBy(xpath="//div[@class='cart-list-destopview ng-star-inserted']//div[@class='cart-product border-bottom pb-1 ng-star-inserted']//span[@class='update-items fw-500 d-block ng-star-inserted']")
		 })
		 List<WebElement> Additionalprice;

		@FindBy(xpath="//div[@class='cart-list-destopview ng-star-inserted']//div[@class='cart-product border-bottom pb-1 ng-star-inserted']//p[@class='mt-1 ng-star-inserted'][2]//span[2]")
		
		 WebElement Baseprice;
	 public GuestAddtocartBlank() {
		  PageFactory.initElements(driver, this);
	}
	 
	 public void Viewfulldescriptionlinktest() {
		 Viewfulldescription.click();
	 }
	 
	 public boolean Wishlisttest() {
		 Wishlist.click();
		return Signinpopup.isDisplayed();
	 }
	 
	 public void comparetest() {
		Compare.get(0).click();
	 }
	 
	 public void Specsheettest() {
			Compare.get(1).click();
		 }
	 public void Productmeasurementtest() {
			Compare.get(2).click();
		 }
	 
	 public String[] lowaspricetest() throws InterruptedException {
		 String savebannerprice=Price.getText();
		 Thread.sleep(1000);
		 String tierpricelast=tierprice.get(5).getText();
		 return new String[] {savebannerprice,tierpricelast}; 
		 
	 }
	 
	 public String[] Selectedcolourtest() throws InterruptedException {
		 Colours.get(1).click();
		 String colurname=Colours.get(1).getText();
		 String colournameqtytab=Colourtext.get(0).getText();
		 return new String[] {colurname,colournameqtytab}; 
		 
	 }
	 public String[] Changecolourtest() throws InterruptedException {
		 Changecolour.click();
		 Changecolourpopupcolour.get(3).click();
		 String colurname=Changecolourtext.getText();
		 button.click();
		 String colournamechangecolour=Colourtext.get(0).getText();		 
		 return new String[] {colurname,colournamechangecolour}; 
		 
	 }
	 public String[] addanothercolurtest() throws InterruptedException {
		 Addanothercolurqty.click();
		 Changecolourpopupcolour.get(0).click();
		 String colurname=Changecolourtext.getText();
		 button.click();
		 String colournamechangecolour=Colourtext.get(1).getText();
		 for(int i=0;i<quantity.size();i++) {
			 quantity.get(i).sendKeys(String.valueOf(getRandomInteger(1, 1)));
		 }
		 Addtocart.click();
		 Thread.sleep(5000);
		 proceedtocheckout.click();
		 return new String[] {colurname,colournamechangecolour}; 
		 
	 }
	 
//*****************Test Scenario: This function is checking the Addtocart pop up that Qty added in the Product details page *******************
	 
	 public String[] AddtocartQtytest() throws InterruptedException {
		 int Sum=0;
		 Random rand = new Random();
		 int j=0;
		 Addtocart.click();
		 Errormessage.isDisplayed();
		 for(int i=0;i<quantity.size();i++) {
			 quantity.get(i).sendKeys(String.valueOf(getRandomInteger(1, 1)));
			
		 }
		 
		 String Totalquantity=driver.findElement(By.xpath("//div[@class='mt-1 mb-1 w-100']//span[@class='fw-500 text-danger']")).getText();
		 System.out.println(Totalquantity);
		 Addtocart.click();
		
		 List<WebElement> total=driver.findElements(By.xpath("//div[@class='mr-2 size-label ng-star-inserted']//strong"));
		// Thread.sleep(5000);
		 System.out.println(total.size());
		 for(int k=0;k<total.size();k++) {
			
			 
			 Sum=Sum+Integer.parseInt(String.valueOf(total.get(k).getText()));
			 System.out.println(Sum);
		 }
		 proceedtocheckout.click();
		 return new String[] {String.valueOf(Sum),Totalquantity}; 
	 }
	 
	 
//Checking the Qty and subqty in the Shopping cart page	 
	public int[] qtytest() {
		 int sum=0;
			int q=0;
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			int size=Productblocks.size();
			System.out.println(size);
			String subString = null;
			for(int i=0;i<size;i++) {
				
				for (int m=0;m<qty.size();m++) {
					
					 q=Integer.parseInt(qty.get(m).getText());
					sum=sum+q;
				}
					
				String subtotalqt=Subtotalqty.getText();
				
				  String testString = subtotalqt;
				  int startIndex = testString.indexOf("(");
				  int endIndex = testString.indexOf(" items)");
				  subString = testString.substring(startIndex+1, endIndex);
				  System.out.println(subString);
			}
			return new int[] {Integer.parseInt(subString),sum};

		
	 }

//Checking the Inputqty and subqty in shopping cart page
	 public int[] qtytest2() {
		 int sum=0;
			int q=0;
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			int size=Productblocks.size();
			System.out.println(size);
			String subString = null;
			for(int i=0;i<size;i++) {
				
				for (int m=0;m<Quantity.size();m++) {
					
					 q=Integer.parseInt(qty.get(m).getText());
					sum=sum+q;
				}
					
				String subtotalqt=Subtotalqty.getText();
				
				  String testString = subtotalqt;
				  int startIndex = testString.indexOf("(");
				  int endIndex = testString.indexOf(" items)");
				  subString = testString.substring(startIndex+1, endIndex);
				  System.out.println(subString);
			}
			return new int[] {Integer.parseInt(subString),sum};
			
		
		 
	 }
//Checking the Price and Subtotalprice in the Shopping cart page	 
	 public float[] pricetest1() {
		 
		 float sum=0;
			String q;
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			int size=Productblocks.size();
			System.out.println(size);
			String subString = null;
			for(int i=0;i<size;i++) {
				
				for (int m=0;m<total.size();m++) {
					
					q=total.get(m).getText();
					  String testString = q;
					  int startIndex = testString.indexOf("$");
					 // int endIndex = testString.indexOf(" items)");
					  subString = testString.substring(startIndex+1);
					  System.out.println(subString);
					sum=sum+Float.parseFloat(subString);
				}
				System.out.println(sum);	
				String subtotalqt=subtotal.getText();
				
				  String testString = subtotalqt;
				  int startIndex = testString.indexOf("$");
				  //int endIndex = testString.indexOf(" items)");
				  subString = testString.substring(startIndex+1);
				  System.out.println(subString);
			}
			return new float[] {Float.parseFloat(subString),Float.parseFloat(String.format("%.2f", sum))};

	 }
	 
	 public float[] pricetest2() {
		 float pricetot=0;
		 float sum=0;
			int q;
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			int size=Productblocks.size();
			System.out.println(size);
			String subString = null;
			String subString2 = null;
			for(int i=0;i<size;i++) {
				
				for (int m=0;m<Quantity.size();m++) {
					
					q=Integer.parseInt(qty.get(m).getText());
					String basepri=Baseprice.getText();
					String testString = basepri;
					 int startIndex = testString.indexOf("+$");
					//int endIndex = testString.indexOf(" items)");
					 subString = testString.substring(startIndex+1);
					 System.out.println(subString);
					if(Additionalprice.size()>0) {
						
							int k=0;
							String additionalprice=Additionalprice.get(k).getText();
							String testString1 = additionalprice;
							 int startIndex1 = testString1.indexOf("+ $");
							int endIndex1 = testString1.indexOf("/item");
							 subString2 = testString1.substring(startIndex1+1, endIndex1);
							 System.out.println(subString2);
							float price2=Float.parseFloat(subString2)+Float.parseFloat(subString);
							k++;
							subString=String.valueOf(price2);
							
					}
					
					 pricetot=Float.parseFloat(subString)*q;
					 System.out.println(pricetot);
				}
				System.out.println(sum);	
				String subtotalqt=subtotal.getText();
				
				  String testString = subtotalqt;
				  int startIndex = testString.indexOf("$");
				  //int endIndex = testString.indexOf(" items)");
				  subString = testString.substring(startIndex+1);
				  System.out.println(subString);
			}
			return new float[] {Float.parseFloat(subString),pricetot};

		 
	 }
	//Checking the Clearcart functionality 
	 public boolean clearcarttest() throws InterruptedException {
			
			Thread.sleep(5000);
			JavascriptExecutor jse = (JavascriptExecutor)driver;

	        jse.executeScript("scroll(0, 250);");
			driver.findElement(By.xpath("//span[contains(text(),'Clear Cart')]//parent::div")).click();
			driver.findElement(By.xpath("//span[contains(text(),' Ok')]//parent::button")).click();
		Thread.sleep(10000);
			return	driver.findElement(By.xpath("//img[@alt='Cart is empty']")).isDisplayed();
			
			 
	 }
	 
	 //Checking the Freeshipping functionality below 69.00
	 public boolean freeshippingtest() {
		 String subtotalqt=subtotal.getText();
			
		  String testString = subtotalqt;
		  int startIndex = testString.indexOf("$");
		  //int endIndex = testString.indexOf(" items)");
		  String subString = testString.substring(startIndex+1);
		  System.out.println(subString);
		  
		  if(Float.parseFloat(subString)>69.00) {
			  
			 return  driver.findElement(By.xpath("//div//strong[@class='text-success fw-500 ng-star-inserted']")).isDisplayed();  
		  }
		  
		  else {
			  String price=driver.findElement(By.xpath("//div[@class='free_shipping_msg p-1 text-center ng-star-inserted']//span")).getText();
			  String testString1 = price;
			  int startIndex1 = testString.indexOf("$");
			  String subString2 = testString1.substring(startIndex1+1);
			  System.out.println(subString2);
			  String total=driver.findElement(By.xpath("//span[@class='total-price fw-500 text-danger total-price-wrapper']")).getText();
			  String testString2 = total;
			  int startIndex2 = testString2.indexOf("$");
			  String subString3 = testString2.substring(startIndex2+1);
			  System.out.println(subString3);
			  double free=69.00-Float.parseFloat(subString3);
			  Assert.assertEquals(String.format("%.2f", free), subString2);
			  return driver.findElement(By.xpath("//p//strong")).isDisplayed();
		  }
	 }
	 
	 //Checking the Qty after updating the Qty of the Shopping cart
	 
	 public int[] cartupdation() throws InterruptedException {
		 int sum=0;
			int q=0;
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			int size=Productblocks.size();
			System.out.println(size);
			String subString = null;
			for(int i=0;i<size;i++) {
				
				for (int m=0;m<Quantity.size();m++) {
				
					
					Quantity.get(m).clear();
					Quantity.get(m).sendKeys(String.valueOf(getRandomInteger(3,3)));
					
				}
			}
			
			Thread.sleep(5000);

			for(int i=0;i<size;i++) {
				
				for (int m=0;m<Quantity.size();m++) {
					
					 q=Integer.parseInt(qty.get(m).getText());
					sum=sum+q;
				}
					
				String subtotalqt=Subtotalqty.getText();			
				  String testString = subtotalqt;
				  int startIndex = testString.indexOf("(");
				  int endIndex = testString.indexOf(" items)");
				  subString = testString.substring(startIndex+1, endIndex);
				  System.out.println(subString);
			}
			return new int[] {Integer.parseInt(subString),sum};
				 
	 }
	 
	 //Checking the price after updating the Qty of the Shopping cart
	 
	 public float[] cartupdationpricetest1() throws InterruptedException {
		 
		 float sum=0;
			String q;
//			try {
//				Thread.sleep(5000);
//			} catch (InterruptedException e) {
//				
//				e.printStackTrace();
//			}
			int size=Productblocks.size();
			System.out.println(size);
			String subString = null;
			for(int i=0;i<size;i++) {
				
				for (int m=0;m<Quantity.size();m++) {
					
					Quantity.get(m).clear();
					Quantity.get(m).sendKeys(String.valueOf(getRandomInteger(3,3)));
					
				}
			}
			
		//	Thread.sleep(5000);
			

				
				for (int m=0;m<total.size();m++) {
					
					q=total.get(m).getText();
					  String testString = q;
					  int startIndex = testString.indexOf("$");
					 // int endIndex = testString.indexOf(" items)");
					  subString = testString.substring(startIndex+1);
					  System.out.println(subString);
						System.out.println(sum);	

					sum=sum+Float.parseFloat(subString);

				}
				System.out.println(sum);	
				String subtotalqt=subtotal.getText();
				  String testString = subtotalqt;
				  int startIndex = testString.indexOf("$");
				  //int endIndex = testString.indexOf(" items)");
				  subString = testString.substring(startIndex+1);
				  System.out.println(subString);
			
			return new float[] {Float.parseFloat(subString),Float.parseFloat(String.format("%.2f",sum))};

	 }
	 
	 //Checking the Remove icon functionality and checking qty updated
 
	 public int[] Removeiconclick() throws InterruptedException {
		 
		 int sum=0;
			int q=0;
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			int size=Productblocks.size();
			System.out.println(size);
			String subString = null;
		
			driver.findElements(By.xpath("//i[@mattooltip='Remove']")).get(0).click();
			Thread.sleep(5000);
			for(int i=0;i<size;i++) {
				
				for (int m=0;m<Quantity.size();m++) {
					
					 q=Integer.parseInt(qty.get(m).getText());
					sum=sum+q;
				}
					
				String subtotalqt=Subtotalqty.getText();
				
				  String testString = subtotalqt;
				  int startIndex = testString.indexOf("(");
				  int endIndex = testString.indexOf(" items)");
				  subString = testString.substring(startIndex+1, endIndex);
				  System.out.println(subString);
			}
			return new int[] {Integer.parseInt(subString),sum};
					 
	 }
	 
	 public void Estimatedeliverydatecheck() throws InterruptedException {
		 
		WebElement name= driver.findElement(By.xpath("//textarea[@placeholder='Enter a location']"));
		 name.sendKeys("2330 South Archibald Avenue, Ontario, CA, USA");
		 driver.findElement(By.xpath("//h4[contains(text(),'Estimate Shipping and tax')]")).click();
		
		 Thread.sleep(5000);
		 int size=driver.findElements(By.xpath("//div[@class='mat-radio-label-content']")).size();
		 List<WebElement>radiobutton=driver.findElements(By.xpath("//div[@class='mat-radio-label-content']"));
		 List<WebElement>radiobuttontext=driver.findElements(By.xpath("//div[@class='mat-radio-label-content']//span[2]"));
		 
		 for(int i=0;i<size;i++) {
			 
			  radiobutton.get(i).click();
			  String Deliverymethod= radiobuttontext.get(i).getText();
			  String testString = Deliverymethod;
			  int startIndex = testString.indexOf("($");
			  int endIndex = testString.indexOf(")");
			  String subString = testString.substring(startIndex+1, endIndex);
			  System.out.println(subString);
			  String ordersummarydeliverymethod=driver.findElement(By.xpath("//div[@class='order-summary-wrap']//div//span[contains(text(),'Shipping ')]//parent::div//span[2]")).getText();
			  Assert.assertEquals(subString, ordersummarydeliverymethod);
			  String ordersummarysubtotal=driver.findElement(By.xpath("//div[@class='order-summary-wrap']//div//span[@class='fw-500'][2]")).getText();
			  String testString1 = ordersummarysubtotal;
			  int startIndex1 = testString1.indexOf("$");
			  String subString1 = testString1.substring(startIndex1+1);
			  System.out.println(subString1);
			  String ordersummarytax=driver.findElement(By.xpath("//div[@class='order-summary-wrap']//div//span[contains(text(),'Tax')]//parent::div//span[2]")).getText();
			  String testString2 = ordersummarytax;
			  int startIndex2 = testString2.indexOf("$");
			  String subString2 = testString2.substring(startIndex2+1);
			  System.out.println(subString2);
			  String grandtotal=driver.findElement(By.xpath("//div[@class='order-summary-wrap']//div//label//parent::div//span")).getText();
			  String testString3 = grandtotal;
			  int startIndex3 = testString3.indexOf("$");
			  String subString3 = testString3.substring(startIndex3+1);
			  System.out.println(subString3);
			  Float total1=Float.parseFloat(subString)+Float.parseFloat(subString1)+Float.parseFloat(subString2);
			  Assert.assertEquals(subString3, total1);
			  
			  
				  
		 }
		 
		 
	 }
	 
	 //Checking the checkoutbutton functionality
	 
	 public void checkoutclick() throws InterruptedException {
		 driver.findElements(By.xpath("//span[contains(text(),'Checkout')]//parent::button")).get(1).click();
		 if(driver.findElements(By.xpath("//span[contains(text(),' Checkout As Guest ')]//parent::button")).size()!=0) {
			 driver.findElement(By.xpath("//span[contains(text(),' Checkout As Guest ')]//parent::button")).click();
			 
		 }
		 
		 else {
			// placeorderbuttonclick();
		 }
	 }
	 
	public void checkoutbuttonclick() {
		 driver.findElements(By.xpath("//span[contains(text(),'Checkout')]//parent::button")).get(1).click();

	}
	 
	 
	 //Adding the Shipping address function
	 public void Shippingaddress(String fname,String email,String companyname,String address,String city,String state,String zipcode,String phonenumber) throws InterruptedException  {
		 
		 driver.findElements(By.xpath("//span[contains(text(),' Use this Shipping Address')]//parent::button")).get(0).click();
		 driver.findElement(By.xpath("//mat-error")).isDisplayed();
		 driver.findElement(By.xpath("//input[@formcontrolname='name']")).clear();
		 driver.findElement(By.xpath("//input[@formcontrolname='name']")).sendKeys(fname);
		 driver.findElement(By.xpath("//input[@formcontrolname='email']")).clear();
		 driver.findElement(By.xpath("//input[@formcontrolname='email']")).sendKeys(email);
		 driver.findElement(By.xpath("//input[@formcontrolname='company_name']")).clear();
		 driver.findElement(By.xpath("//input[@formcontrolname='company_name']")).sendKeys(companyname);
		 driver.findElement(By.xpath("//input[@formcontrolname='address_1']")).clear();
		 driver.findElement(By.xpath("//input[@formcontrolname='address_1']")).sendKeys(address);
		 driver.findElement(By.xpath("//input[@formcontrolname='city']")).clear();
		 driver.findElement(By.xpath("//input[@formcontrolname='city']")).sendKeys(city);
		//Select select = new Select(driver.findElement(By.xpath("//mat-select[@formcontrolname='state']")));
		driver.findElement(By.xpath("//mat-select[@formcontrolname='state']")).click();
		 Thread.sleep(2000);
		//select.selectByVisibleText(state);
		 driver.findElement(By.xpath("//mat-option//span[contains(text(),'"+state+"')]")).click();
		 driver.findElement(By.xpath("//input[@formcontrolname='zip']")).clear();
		 driver.findElement(By.xpath("//input[@formcontrolname='zip']")).sendKeys(zipcode);
		 driver.findElement(By.xpath("//input[@formcontrolname='phone']")).clear();
		 driver.findElement(By.xpath("//input[@formcontrolname='phone']")).sendKeys(phonenumber);
		 driver.findElements(By.xpath("//span[contains(text(),' Use this Shipping Address')]//parent::button")).get(0).click();
//		
//		 JavascriptExecutor jsDriver;
//		 NgWebDriver ngWebDriver;
//		 jsDriver = (JavascriptExecutor) driver;
//		 ngWebDriver = new NgWebDriver(jsDriver);
//		 try {
//			 ngWebDriver.waitForAngularRequestsToFinish();
//
//		 }
//		 catch (Exception e) {
//
//		}
		 Thread.sleep(5000);
	 }
	 //Adding the payment  function
	 public void paymenttest1() throws InterruptedException {
		 
		 driver.findElement(By.xpath("//input[@formcontrolname='cardNumber']")).sendKeys("4111111111111111");
		 driver.findElement(By.xpath("//input[@formcontrolname='cardHolderName']")).sendKeys("FAYA");
		 driver.findElement(By.xpath("//mat-select[@formcontrolname='expirationMonth']")).click();
		 driver.findElement(By.xpath("//mat-option//span[contains(text(),'June')]")).click();
		 driver.findElement(By.xpath("//mat-select[@formcontrolname='expirationYear']")).click();
		 driver.findElement(By.xpath("//mat-option//span[contains(text(),'2024')]")).click();
		 driver.findElement(By.xpath("//input[@formcontrolname='cvv']")).sendKeys("123");
		 driver.findElements(By.xpath("//span[contains(text(),' Use this Payment Option ')]//parent::button")).get(0).click();
//		 JavascriptExecutor jsDriver;
//		 NgWebDriver ngWebDriver;
//		 jsDriver = (JavascriptExecutor) driver;
//		 ngWebDriver = new NgWebDriver(jsDriver);
//		 try {
//			 ngWebDriver.waitForAngularRequestsToFinish();
//
//		 }
//		 catch (Exception e) {
//
//		}
		 Thread.sleep(2000);
	 }
 public void paymenttest2() {
		 
		 driver.findElement(By.xpath("//input[@formcontrolname='cardNumber']")).sendKeys("4111111111111111");
		 driver.findElement(By.xpath("//input[@formcontrolname='cardHolderName']")).sendKeys("FAYA");
		 driver.findElement(By.xpath("//mat-select[@formcontrolname='expirationMonth']")).click();
		 driver.findElement(By.xpath("//mat-option//span[contains(text(),'June')]")).click();
		 driver.findElement(By.xpath("//mat-select[@formcontrolname='expirationYear']")).click();
		 driver.findElement(By.xpath("//mat-option//span[contains(text(),'2024')]")).click();
		 driver.findElement(By.xpath("//input[@formcontrolname='cvv']")).sendKeys("123");
		//driver.findElements(By.xpath("//span[contains(text(),' Use this Payment Option ')]//parent::button")).get(0).click();
//		 JavascriptExecutor jsDriver;
//		 NgWebDriver ngWebDriver;
//		 jsDriver = (JavascriptExecutor) driver;
//		 ngWebDriver = new NgWebDriver(jsDriver);
//		 try {
//			 ngWebDriver.waitForAngularRequestsToFinish();
//
//		 }
//		 catch (Exception e) {
//
//		}
 }
	
 
//Checking the Qty in the checkout page Items and Shipping section 
 	public int[] qtytestcheckout() throws InterruptedException {
		 
		 
		 List<WebElement> qty=driver.findElements(By.xpath("//div[@class='mat-cell fw-500 text-center cart-update font-sm ng-star-inserted']//p[@class='fw-500 ng-star-inserted']"));
		 
		 int sum=0;
			int q=0;
			
				//Thread.sleep(5000);
			
			int size=driver.findElements(By.xpath("//div[@class='mat-table cart-table mb-1 ng-star-inserted']")).size();
			System.out.println(size);
			String subString = null;
			for(int i=0;i<size;i++) {
				
				for (int m=0;m<qty.size();m++) {
					
					 q=Integer.parseInt(qty.get(m).getText());
					 sum=sum+q;
				}
					
					String subtotalqt=driver.findElement(By.xpath("//div[@class='text-right grand-total mob-sub-total pr-1']//span[@class='fw-500 total-price']")).getText();
				
				  String testString = subtotalqt;
				  int startIndex = testString.indexOf("(");
				  int endIndex = testString.indexOf(" items)");
				  subString = testString.substring(startIndex+1, endIndex);
				  System.out.println(subString);
			}
		
			return new int[] {Integer.parseInt(subString),sum};
			
}

 	//Checking the price in the checkout page Items and Shipping section 
 		public float[] checkoutprice() throws InterruptedException {

			float sum1=0;
			String q1;
//			try {
//				Thread.sleep(5000);
//			} catch (InterruptedException e) {
//				
//				e.printStackTrace();
//			}
			int size1=driver.findElements(By.xpath("//div[@class='mat-table cart-table mb-1 ng-star-inserted']")).size();
			System.out.println(size1);
			
			List<WebElement>total1=driver.findElements(By.xpath("//p[contains(text(),'Total')]//parent::div//p[@class='text-danger w-100 ng-star-inserted']"));
			String subString2 = null;
			
			String subString4 = null;
			for(int i=0;i<size1;i++) {
				
				for (int m=0;m<total1.size();m++) {
					
					q1=total1.get(m).getText();
					  String testString = q1;
					  int startIndex = testString.indexOf("$");
					 // int endIndex = testString.indexOf(" items)");
					  subString2 = testString.substring(startIndex+1);
					  System.out.println(subString2);
					sum1=sum1+Float.parseFloat(subString2);
				}
				System.out.println(sum1);	
				String subtotalqt=driver.findElement(By.xpath("//div[@class='text-right grand-total mob-sub-total pr-1']//span[@class='total-price text-danger fw-500']")).getText();
				
				  String testString4 = subtotalqt;
				  int startIndex = testString4.indexOf("$");
				  //int endIndex = testString.indexOf(" items)");
				 subString4 = testString4.substring(startIndex+1);
				  System.out.println(subString4);
			}
//			Thread.sleep(5000);
//			
//			Thread.sleep(5000);
			return new float[] {Float.parseFloat(subString4),Float.parseFloat(String.format("%.2f", sum1))};
			
	 }
	 
	 public void Deliverymethodtest() throws InterruptedException, AWTException {
		 Thread.sleep(5000);
			//driver.findElement(By.xpath("//mat-form-field")).click();
			 List<WebElement> dropdown=driver.findElements(By.xpath("//div[@class='shipping-address-wrap px-2 py-2 mb-2']//mat-radio-button"));
			 
			 for(int i=0;i<dropdown.size();i++) {
				Thread.sleep(5000);
				 String shippmethod=driver.findElements(By.xpath("//div[@class='shipping-address-wrap px-2 py-2 mb-2']//mat-radio-button")).get(i).getText();

				 driver.findElements(By.xpath("//div[@class='shipping-address-wrap px-2 py-2 mb-2']//mat-radio-button")).get(i).click();
				Thread.sleep(2000);
				 String testString = shippmethod;
				 System.out.println(testString);
				 String subString1;
				 if(testString.contains("Economy Ground")) {
					float m=0;
					 subString1=String.valueOf(m);
				 }
				 else {
					 int startIndex = testString.indexOf("($");
					  int endIndex = testString.indexOf(")");
					  String subString = testString.substring(startIndex+2, endIndex);
					  System.out.println(subString);
					  
					  String ordersummarydeliverymethod=driver.findElement(By.xpath("//span[@class='fw-500 font-lg total-wrapper mob-pr-10 text-danger']")).getText();
					  int startIndex1 = ordersummarydeliverymethod.indexOf("$");
					  
					  subString1 = ordersummarydeliverymethod.substring(startIndex1+1);
					  System.out.println(subString1);
					  Thread.sleep(2000);
					  Assert.assertEquals(subString, subString1);
				 }

				  String ordersummarysubtotal=driver.findElement(By.xpath("//span[contains(text(),'Subtotal')]//parent::div//span[2]")).getText();
				  String testString2 = ordersummarysubtotal;
				  int startIndex3 = testString2.indexOf("$");
				  String subString3 = testString2.substring(startIndex3+1);
				  System.out.println(subString3);
				  
				  
				  String ordersummarytax=driver.findElement(By.xpath("//span[@class='fw-500 text-danger font-lg total-wrapper mob-pr-10']//parent::div//span[contains(text(),'Tax')]//parent::div//span[2]")).getText();
				  String testString3 = ordersummarytax;
				  int startIndex4 = testString3.indexOf("$");
				  String subString4 = testString3.substring(startIndex4+1);
				  System.out.println(subString4);
				  
				  String grandtotal=driver.findElement(By.xpath("//span[@class='fw-500 total-price']//parent::div[@class='text-right grand-total pr-1']//span[2]")).getText();
				  String testString5 = grandtotal;
				  int startIndex5 = testString5.indexOf("$");
				  String subString5 = testString5.substring(startIndex5+1);
				  System.out.println(subString5);
				  Float total1=Float.parseFloat(subString1)+Float.parseFloat(subString3)+Float.parseFloat(subString4);
				  System.out.println(Math.round(total1));
				  Assert.assertEquals(Float.parseFloat(subString5),Float.parseFloat(String.format("%.2f", total1)));
				  //driver.findElement(By.xpath("//ngx-ecommerce-core-layout/mat-sidenav-container/mat-sidenav-content/div/ngx-ecommerce-core-checkout/div/div[1]/div[4]/div/div[1]/mat-form-field")).click();
				 Thread.sleep(5000);	
			 }
				//driver.findElements(By.xpath("//span[contains(text(),' Place Order')]//parent::button")).get(0).click();
				Robot robot=new Robot();

				robot.keyPress(KeyEvent.VK_ESCAPE);
		}
		
	 
	 public void Deliverymethodtest2() throws InterruptedException, AWTException {
			Thread.sleep(5000);
		 driver.findElement(By.xpath("//mat-form-field")).click();
		 List<WebElement> dropdown=driver.findElements(By.xpath("//mat-option//span[@class='mat-option-text']"));
		 
		 for(int i=0;i<dropdown.size();i++) {
			 Thread.sleep(5000);
			 String shippmethod=driver.findElements(By.xpath("//mat-option//span[@class='mat-option-text']")).get(i).getText();

			 driver.findElements(By.xpath("//mat-option")).get(i).click();
			 Thread.sleep(10000);
			 String testString = shippmethod;
			 System.out.println(testString);
			
			int startIndex = testString.indexOf("($");
				  int endIndex = testString.indexOf(")");
				  String subString = testString.substring(startIndex+2, endIndex);
				  System.out.println(subString);
				  
				  String ordersummarydeliverymethod=driver.findElement(By.xpath("//span[@class='fw-500 font-lg total-wrapper mob-pr-10 text-danger']")).getText();
				  int startIndex1 = ordersummarydeliverymethod.indexOf("$");
				  
				  String subString1 = ordersummarydeliverymethod.substring(startIndex1+1);
				  System.out.println(subString1);
				  Thread.sleep(5000);
				 Assert.assertEquals(subString, subString1);
			  String ordersummarysubtotal=driver.findElement(By.xpath("//span[@class='fw-500 text-danger font-lg total-wrapper mob-pr-10']//parent::div//span[contains(text(),'Subtotal')]//parent::div//span[2]")).getText();
			  String testString2 = ordersummarysubtotal;
			  int startIndex3 = testString2.indexOf("$");
			  String subString3 = testString2.substring(startIndex3+1);
			  System.out.println(subString3);
			  
			  
			  String ordersummarytax=driver.findElement(By.xpath("//span[@class='fw-500 text-danger font-lg total-wrapper mob-pr-10']//parent::div//span[contains(text(),'Tax')]//parent::div//span[2]")).getText();
			  String testString3 = ordersummarytax;
			  int startIndex4 = testString3.indexOf("$");
			  String subString4 = testString3.substring(startIndex4+1);
			  System.out.println(subString4);
			  
			  String grandtotal=driver.findElement(By.xpath("//span[@class='fw-500 total-price']//parent::div[@class='text-right grand-total pr-1']//span[2]")).getText();
			  String testString5 = grandtotal;
			  int startIndex5 = testString5.indexOf("$");
			  String subString5 = testString5.substring(startIndex5+1);
			  System.out.println(subString5);
			  Float total1=Float.parseFloat(subString1)+Float.parseFloat(subString3)+Float.parseFloat(subString4);
			  System.out.println(Math.round(total1));
			  
			  
			  Assert.assertEquals(Float.parseFloat(subString5),Float.parseFloat(String.format("%.2f", total1)));
			  driver.findElement(By.xpath("//mat-form-field")).click();
			  Thread.sleep(5000);	
		 }
			//driver.findElements(By.xpath("//span[contains(text(),' Place Order')]//parent::button")).get(0).click();
		 	Robot robot=new Robot();

			robot.keyPress(KeyEvent.VK_ESCAPE);
	 }
	 
	 
	 
	 public void Billingaddresstest() throws InterruptedException {
		 driver.findElement(By.xpath("//mat-checkbox")).click();
		 driver.findElements(By.xpath("//span[contains(text(),' Use this Payment Option ')]//parent::button")).get(0).click();
		 driver.findElement(By.xpath("//mat-error")).isDisplayed();
		 
		 driver.findElement(By.xpath("//input[@formcontrolname='name']")).clear();
		 driver.findElement(By.xpath("//input[@formcontrolname='name']")).sendKeys("Billingaddress");
		 driver.findElement(By.xpath("//input[@formcontrolname='email']")).clear();
		 driver.findElement(By.xpath("//input[@formcontrolname='email']")).sendKeys("fayaqa@yopmail.com");
		 driver.findElement(By.xpath("//input[@formcontrolname='company_name']")).clear();
		 driver.findElement(By.xpath("//input[@formcontrolname='company_name']")).sendKeys("FAYA");
		 driver.findElement(By.xpath("//input[@formcontrolname='address_1']")).clear();
		 driver.findElement(By.xpath("//input[@formcontrolname='address_1']")).sendKeys("347 Don Shula Dr");
		 driver.findElement(By.xpath("//input[@formcontrolname='city']")).clear();
		 driver.findElement(By.xpath("//input[@formcontrolname='city']")).sendKeys("Miami Gardens");
		 driver.findElement(By.xpath("//mat-select[@formcontrolname='state']")).click();
		  driver.findElement(By.xpath("//mat-option//span[contains(text(),'Florida')]")).click();
		 driver.findElement(By.xpath("//input[@formcontrolname='zip']")).clear();
		 driver.findElement(By.xpath("//input[@formcontrolname='zip']")).sendKeys("33056");
		 Thread.sleep(2000);
		 driver.findElement(By.xpath("//input[@formcontrolname='phone']")).clear();
		 driver.findElement(By.xpath("//input[@formcontrolname='phone']")).sendKeys("(432) 432-4234 x23423");	
		// driver.findElement(By.xpath("//*[@id=\"cdk-step-content-1-1\"]/div/div/ngx-ecommerce-core-checkout-payment-method/form/div/div[4]/button[1]")).click();
		 driver.findElements(By.xpath("//span[contains(text(),' Use this Payment Option')]//parent::button")).get(2).click();
//		 JavascriptExecutor jsDriver;
//		 NgWebDriver ngWebDriver;
//		 jsDriver = (JavascriptExecutor) driver;
//		 ngWebDriver = new NgWebDriver(jsDriver);
//		 try {
//			 ngWebDriver.waitForAngularRequestsToFinish();
//
//		 }
//		 catch (Exception e) {
//
//		}
	 }
	 
	 public void newshippingaddressadd(String fname,String email,String companyname,String address,String city,String state,String zipcode,String phonenumbe) throws InterruptedException {
		 Thread.sleep(10000);
		 driver.findElements(By.xpath("//a[contains(text(),'Change')]")).get(0).click();
		 driver.findElement(By.xpath("//a[contains(text(),'+ Add Shipping Address')]")).click();
		 JavascriptExecutor jse = (JavascriptExecutor)driver;

	        jse.executeScript("scroll(500, 0);");
		Shippingaddress(fname, email, companyname, address, city, state, zipcode, phonenumbe);
	 }
	 
	 public void newcardadd() throws InterruptedException {
		 driver.findElements(By.xpath("//a[contains(text(),'Change')]")).get(1).click();
		 driver.findElement(By.xpath("//a[contains(text(),'+ Add New Card')]")).click();
		 paymenttest1();
		 
	 }
	 public void newbillingaddressadd() throws InterruptedException {
		 Thread.sleep(5000);
		 driver.findElements(By.xpath("//a[contains(text(),'Change')]")).get(2).click();
		 driver.findElement(By.xpath("//a[contains(text(),'+ Add Billing Address')]")).click();
		
		 driver.findElement(By.xpath("//input[@formcontrolname='name']")).clear();
		 driver.findElement(By.xpath("//input[@formcontrolname='name']")).sendKeys("Billingaddress");
		 driver.findElement(By.xpath("//input[@formcontrolname='email']")).clear();
		 driver.findElement(By.xpath("//input[@formcontrolname='email']")).sendKeys("fayaqa@yopmail.com");
		 driver.findElement(By.xpath("//input[@formcontrolname='company_name']")).clear();
		 driver.findElement(By.xpath("//input[@formcontrolname='company_name']")).sendKeys("FAYA");
		 driver.findElement(By.xpath("//input[@formcontrolname='address_1']")).clear();
		 driver.findElement(By.xpath("//input[@formcontrolname='address_1']")).sendKeys("347 Don Shula Dr");
		 driver.findElement(By.xpath("//input[@formcontrolname='city']")).clear();
		 driver.findElement(By.xpath("//input[@formcontrolname='city']")).sendKeys("Miami Gardens");
		 driver.findElement(By.xpath("//mat-select[@formcontrolname='state']")).click();
		  driver.findElement(By.xpath("//mat-option//span[contains(text(),'Florida')]")).click();
		 driver.findElement(By.xpath("//input[@formcontrolname='zip']")).clear();
		 driver.findElement(By.xpath("//input[@formcontrolname='zip']")).sendKeys("33056");
		 driver.findElement(By.xpath("//input[@formcontrolname='phone']")).clear();
		 driver.findElement(By.xpath("//input[@formcontrolname='phone']")).sendKeys("(432) 432-4234 x23423");	
		// driver.findElement(By.xpath("//*[@id=\"cdk-step-content-1-1\"]/div/div/ngx-ecommerce-core-checkout-payment-method/form/div/div[4]/button[1]")).click();
		 driver.findElements(By.xpath("//span[contains(text(),' Use this Billing Address ')]//parent::button")).get(2).click();
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}

	
	 }
	 
	 public void shippingaddresschange() throws InterruptedException {
		 Thread.sleep(5000);
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 driver.findElements(By.xpath("//a[contains(text(),'Change')]")).get(0).click();
		List<WebElement> shippingaddresssize= driver.findElements(By.xpath("//div[@class='w-100 ng-star-inserted']//li"));
		int size= shippingaddresssize.size();
		shippingaddresssize.get(1).click();	
		 driver.findElements(By.xpath("//span[contains(text(),' Use this Shipping Address')]//parent::button")).get(0).click();
		 Thread.sleep(5000);
	 }
	 public void paymentmethodchange() throws InterruptedException {
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 driver.findElements(By.xpath("//a[contains(text(),'Change')]")).get(1).click();
		List<WebElement> paymentcardsize= driver.findElements(By.xpath("//div[@class='mat-table card-list-table mb-1']//mat-radio-group"));
		int size= paymentcardsize.size();
		paymentcardsize.get(0).click();	
		 driver.findElements(By.xpath("//span[contains(text(),' Use this Payment Option')]//parent::button")).get(2).click();
		 Thread.sleep(5000);

	 }
	 public void billingaddresschange() throws InterruptedException {
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}

		 driver.findElements(By.xpath("//a[contains(text(),'Change')]")).get(2).click();
		List<WebElement> billingaddresssize= driver.findElements(By.xpath("//div[@class='ng-star-inserted']//li"));
		int size= billingaddresssize.size();
		billingaddresssize.get(1).click();	
		 driver.findElements(By.xpath("//span[contains(text(),' Use this Billing Address ')]//parent::button")).get(2).click();
		 Thread.sleep(5000);

	 }
	 
	 public boolean placeorderbuttonclick() throws InterruptedException {
			driver.findElements(By.xpath("//span[contains(text(),' Place Order')]//parent::button")).get(1).click();
			Thread.sleep(5000);
			return driver.findElement(By.xpath("//img[@alt='Thankyou']")).isDisplayed();
			 
		
	 }
	 
	 public void orderdetailspage() {
		 String subString;
		 String subString1;
		 String subString2;
		 String subString3;
		 ordernum.click();
		 String orderno=ordernum.getText();
		 String ordernumdetailpage=driver.findElement(By.xpath("//span[@class='ml-1 border text-capitalize order-id']//strong")).getText();
		 Assert.assertTrue(orderno.contains(ordernumdetailpage));
		 String status=driver.findElement(By.xpath("//h3//span[@class='fw-500 text-link']")).getText();
		 System.out.println(status);
		 Assert.assertEquals(status, "Order Processing");
		 driver.findElement(By.xpath("//p[contains(text(),'Product Virtual Preview*')]//parent::div//div[@class='dst-image']")).isDisplayed();
		 String subtotal=driver.findElement(By.xpath("//app-order-pricing-summary//table//tr[5]//td[4]//strong")).getText();
		 String testString = subtotal;
		  int startIndex = testString.indexOf("$");
		  subString = testString.substring(startIndex+1);
		  System.out.println(subString);
		  String shipping= driver.findElement(By.xpath("//app-order-pricing-summary//table//tr[6]//td[3]//strong")).getText();
		  String testString1 = shipping;
		  int startIndex1 = testString1.indexOf("$");
		  subString1 = testString1.substring(startIndex1+1);
		  System.out.println(subString1);
		  String tax=driver.findElement(By.xpath("//app-order-pricing-summary//table//tr[7]//td[4]//strong")).getText();
		  String testString2 = tax;
		  int startIndex2 = testString2.indexOf("$");
		  subString2 = testString2.substring(startIndex2+1);
		  System.out.println(subString2);
		  String grandtotal=driver.findElement(By.xpath("//app-order-pricing-summary//table//tr[8]//td[3]//strong")).getText();
		  String testString3 = grandtotal;
		  int startIndex3 = testString3.indexOf("$");
		  subString3 = testString3.substring(startIndex3+1);
		  System.out.println(subString3);
		  float total=Float.parseFloat(subString)+Float.parseFloat(subString1)+Float.parseFloat(subString2);
		  System.out.println(total);
		  Assert.assertEquals(total, subString3);
		  
		  
	 }

	 public void Deliverymethodconftest() throws InterruptedException, AWTException {
		 Thread.sleep(5000);
		 paymenttest1();
			//driver.findElement(By.xpath("//mat-form-field")).click();
			 List<WebElement> dropdown=driver.findElements(By.xpath("//div[@class='shipping-address-wrap px-2 py-2 mb-2']//mat-radio-button"));
			 
			 for(int i=0;i<dropdown.size();i++) {
				 Thread.sleep(5000);
				 String shippmethod=driver.findElements(By.xpath("//div[@class='shipping-address-wrap px-2 py-2 mb-2']//mat-radio-button")).get(i).getText();

				 driver.findElements(By.xpath("//div[@class='shipping-address-wrap px-2 py-2 mb-2']//mat-radio-button")).get(i).click();
				 Thread.sleep(5000);
				 String testString = shippmethod;
				 System.out.println(testString);
				 String subString1;
				 if(testString.contains("FREE")) {
					float m=0;
					 subString1=String.valueOf(m);
				 }
				 else {
					 int startIndex = testString.indexOf("($");
					  int endIndex = testString.indexOf(")");
					  String subString = testString.substring(startIndex+2, endIndex);
					  System.out.println(subString);
					  
					  String ordersummarydeliverymethod=driver.findElement(By.xpath("//span[@class='fw-500 font-lg total-wrapper mob-pr-10 text-danger']")).getText();
					  int startIndex1 = ordersummarydeliverymethod.indexOf("$");
					  
					  subString1 = ordersummarydeliverymethod.substring(startIndex1+1);
					  System.out.println(subString1);
					  Assert.assertEquals(subString, subString1);
				 }

				  String ordersummarysubtotal=driver.findElement(By.xpath("//div//span[contains(text(),'Subtotal')]//parent::div//span[2]")).getText();
				  String testString2 = ordersummarysubtotal;
				  int startIndex3 = testString2.indexOf("$");
				  String subString3 = testString2.substring(startIndex3+1);
				  System.out.println(subString3);
				  
				  
				  String ordersummarytax=driver.findElement(By.xpath("//span[@class='fw-500 text-danger font-lg total-wrapper mob-pr-10']//parent::div//span[contains(text(),'Tax')]//parent::div//span[2]")).getText();
				  String testString3 = ordersummarytax;
				  int startIndex4 = testString3.indexOf("$");
				  String subString4 = testString3.substring(startIndex4+1);
				  System.out.println(subString4);
				  
				  String grandtotal=driver.findElement(By.xpath("//span[@class='fw-500 total-price']//parent::div[@class='text-right grand-total pr-1']//span[2]")).getText();
				  String testString5 = grandtotal;
				  int startIndex5 = testString5.indexOf("$");
				  String subString5 = testString5.substring(startIndex5+1);
				 // String.format("%.2f",subString5);
				  System.out.println(subString5);
				  Float total1=Float.parseFloat(subString1)+Float.parseFloat(subString3)+Float.parseFloat(subString4);
				  System.out.println(total1);
				  Assert.assertEquals(Float.parseFloat(subString5),Float.parseFloat(String.format("%.2f",total1)));
				  //driver.findElement(By.xpath("//*[@id=\"app\"]/ngx-ecommerce-core-layout/mat-sidenav-container/mat-sidenav-content/div/ngx-ecommerce-core-configurator-checkout/div/div[1]/div[4]/div/div[1]/mat-form-field")).click();
				//  Thread.sleep(5000);	
			 }
				//driver.findElements(By.xpath("//span[contains(text(),' Place Order')]//parent::button")).get(0).click();
				Robot robot=new Robot();

				robot.keyPress(KeyEvent.VK_ESCAPE);
		}
}
