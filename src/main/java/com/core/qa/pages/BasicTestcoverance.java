package com.core.qa.pages;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import com.core.qa.base.TestBase;

public class BasicTestcoverance extends TestBase {
	
	@FindBys({
	    @FindBy(xpath="//a[@class='logo ng-star-inserted']"), })
		List<WebElement>Logo;
	
	@FindBy(linkText="Create an Account")
	WebElement createanaccount;
	
	@FindBy(xpath="//input[@title='search']")
	WebElement Search;
	
	@FindBy(xpath="//button[@class='mat-focus-indicator search-btn spinner-btn mat-elevation-z0 mat-flat-button mat-button-base mat-primary']")
	WebElement Searchbutton;
	
	@FindBy(xpath="//input[@formcontrolname='last_name']")
	WebElement lastname;
	
	@FindBy(linkText="Create an Account")
	WebElement createaccount;
	
	@FindBy(linkText="Sign In")
	WebElement SignIn;
	
	@FindBy(xpath="//div[@class='login-wrap quote-request-login-form']")
	WebElement SignInpopup;
	
	@FindBy(xpath="//div[@class='theme-container full-width menu-wrap pos-relative']")
	WebElement Megamenu;
	
	@FindBy(xpath="//img[@alt='Banner Images']")
	WebElement Banner;
	
	@FindBys({
	    @FindBy(xpath="//div[@class='products-filter ng-star-inserted']"),})
		 List<WebElement>Widget;
	
	@FindBys({
	    @FindBy(xpath="//div[@class='swiper-container product-carousel ng-tns-c174-3 ng-star-inserted']//img"),})
		 List<WebElement>products;
	
	@FindBy(linkText="ALL PRODUCTS")
	WebElement AllProducts ;
	
	@FindBys({
        @FindBy(xpath="//div[@class='products-wrapper mt-0 accelerate ng-star-inserted']//a"),
 })
	 List<WebElement>Products;
	
	
	@FindBy(xpath="//input[@formcontrolname='first_name']")
	WebElement firstname;
	
	
	@FindBys({
        @FindBy(xpath="//input[@formcontrolname='email']"),
 })
	 List<WebElement>Email;
	
	
	@FindBy(xpath="//input[@formcontrolname='password']")
	WebElement password;
	
	@FindBy(xpath="//input[@formcontrolname='confirm_password']")
	WebElement confirmpassword;
	
	
        @FindBy(xpath="//span[contains(text(),' Create Account ')]//parent::button")

	 WebElement createaccountbutton;
	

	@FindBy(xpath="//span[contains(text(),'You registered successfully!')]")
	WebElement Successmessage;
	
	@FindBy(xpath="//span[contains(text(),'Email already registered. Please login.')]")
	WebElement Alreadyregistered;
	
	@FindBy(xpath="//span[@class='mat-checkbox-label']")
	WebElement Newslettercheckbox;
	
	@FindBy(xpath="//mat-error[@role='alert']")
	WebElement Error;
	
	@FindBy(xpath="//span[contains(text(),'Thank You for subscribing!')]")
	WebElement Successsubcriptionmessage;
		
	@FindBy(xpath="//span[contains(text(),'Logged in successfully')]")
	WebElement signinSuccessmessage;
	
	@FindBy(xpath="//button[@type='submit']")
	WebElement Signinbutton;
	
	@FindBy(xpath="//span[@class='filter-value bg-primary fw-300 ng-star-inserted']")
	WebElement Searchfilter;
	

        @FindBy(xpath="//div//span[@class='product-name w-100 text-truncate']//em")
	
        WebElement productsname;
	
	
        @FindBy(xpath="//div[@class='search-list-inner']//mat-option")

	WebElement Searchdropdown;
	
	
	public BasicTestcoverance() {
		
		 PageFactory.initElements(driver, this);
	}
	
	public boolean logodisplayed() {
		return Logo.get(1).isDisplayed();
	}

	public boolean Bannerchecking() throws InterruptedException {
		Thread.sleep(5000);
		return Banner.isDisplayed();
	}
	
	public boolean Widgetchecking() throws InterruptedException {
		Thread.sleep(5000);
		return Widget.get(0).isDisplayed();
	}
	
	public boolean homepageproductschecking() {
	
		return products.get(0).isDisplayed();
	
	}
	
	public boolean megamenuchecking() {
		return Megamenu.isDisplayed();
	
	}
	
	public boolean allproductscheck() throws InterruptedException {
		AllProducts.click();
		Thread.sleep(5000);
		return  Products.get(0).isDisplayed();
		
	}
	
	public String Searchdropdowntest(String style) {
		Search.sendKeys(prop.getProperty("styleno"));
		String productname=productsname.getText();
		Searchdropdown.click();
		return productname;
	
	}
	
	 public boolean createaccount1(String fname,String lname,String mail,String pass,String cfmpass) throws InterruptedException
	  {
		  createanaccount.click();
		  firstname.clear();
		  lastname.clear();
		  Email.clear();
		  password.clear();
		  confirmpassword.clear();
		  firstname.sendKeys(fname);
		  lastname.sendKeys(lname);
		  Email.get(0).sendKeys(mail);
		  Thread.sleep(5000);
		  password.sendKeys(pass);
		  confirmpassword.sendKeys(cfmpass);
		  Thread.sleep(5000);
		  createaccountbutton.click();
		  
		  return Successmessage.isDisplayed();
	  }
	  public boolean Signin1(String un,String password2) throws InterruptedException
	  {
		  SignIn.click();
		  Email.clear();
		  password.clear();
		  Email.get(1).sendKeys(un);
		  password.sendKeys(password2);
		  Signinbutton.click();
		  Thread.sleep(1000);
		  return signinSuccessmessage.isDisplayed();	  
	  }
	  
	 
}
