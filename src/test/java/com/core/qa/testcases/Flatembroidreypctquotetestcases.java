package com.core.qa.testcases;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.GuestAddtocartBlank;
import com.core.qa.pages.flatPCTQuote;
import com.core.qa.pages.ProductListingpage;
import com.core.qa.pages.Signinpage;

public class Flatembroidreypctquotetestcases extends TestBase {
	ProductListingpage productlistingpage;
	GuestAddtocartBlank productdetailpage;
	flatPCTQuote pctquote;
	Signinpage signinpage;
	@BeforeMethod
		public void setup() throws InterruptedException {
			initialization();
			
			productlistingpage=new 	ProductListingpage();
			productdetailpage=new GuestAddtocartBlank();
			productlistingpage.randomproductclick();
			pctquote=new flatPCTQuote();
		}
	 
//	 @Test(priority=44)
//		public void quickquotetest1() throws InterruptedException, AWTException {
//		 pctquote.Quickquotetest1();
//		 
//		}
	 
	 @Test(priority=45)
		public void quickquotetest2() throws InterruptedException, AWTException {
		 pctquote.Quickquotetest2();
		 
		}
//	 @Test(priority=46)
//		public void quickquotetest4() throws InterruptedException, AWTException {
//		 signinpage=new Signinpage();
//		 signinpage.Signin1(prop.getProperty("Emailid"),prop.getProperty("password"));
//		 boolean flag=pctquote.Quickquotetest4();
//		 Assert.assertTrue(flag);
//		}
//	 @Test(priority=46)
//		public void quickquotetest5() throws InterruptedException, AWTException {
//		boolean flag=pctquote.Quickquotetest5();
//		 Assert.assertTrue(flag);
//		}
//	 @Test(priority=47)
//		public void quickquotetest6() throws InterruptedException, AWTException {
//		boolean flag=pctquote.Quickquotetest6();
//		 Assert.assertTrue(flag);
//		}
//	 @Test(priority=48)
//		public void quickquotetest7() throws InterruptedException, AWTException {
//		boolean flag=pctquote.Quickquotetest7();
//		 Assert.assertTrue(flag);
//		}
//	 
//	 @Test(priority=49)
//		public void quickquotetest3() throws InterruptedException, AWTException {
//		boolean flag=pctquote.Quickquotetest3();
//		 Assert.assertTrue(flag);
//		}
		@AfterMethod
		public void teardown() {
			driver.quit();
		}

}
