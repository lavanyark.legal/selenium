package com.core.qa.testcases;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.GuestAddtocartBlank;
import com.core.qa.pages.flatPCTQuote;
import com.core.qa.pages.ProductListingpage;
import com.core.qa.pages.flatembquickQuoteSubmission;
import com.core.qa.pages.pctplaceorder;

public class pctplaceordertestcases extends TestBase {
	
	ProductListingpage productlistingpage;
	GuestAddtocartBlank productdetailpage;
	flatPCTQuote pctquote;
	pctplaceorder PCTplaceorder;
	 @BeforeMethod
		public void setup() throws InterruptedException {
			initialization();
			
			productlistingpage=new 	ProductListingpage();
			productdetailpage=new GuestAddtocartBlank();
			productlistingpage.randomproductclick();
			pctquote=new flatPCTQuote();
			PCTplaceorder=new pctplaceorder();
		}
	 

	 @Test(priority=45)
		public void pctorder() throws InterruptedException, AWTException {
		 boolean flag=PCTplaceorder.PCTOrdertest();
		 Assert.assertTrue(flag);
		}

	 @Test(priority=45)
		public void pctorder2() throws InterruptedException, AWTException {
		 boolean flag=PCTplaceorder.PCTOrdertest2();
		 Assert.assertTrue(flag);
		}
	 
		@AfterMethod
		public void teardown() {
			driver.quit();
		}
}
