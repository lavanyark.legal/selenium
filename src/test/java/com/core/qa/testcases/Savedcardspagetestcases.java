package com.core.qa.testcases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.Myaccountpage;
import com.core.qa.pages.Savedcardspage;
import com.core.qa.pages.Signinpage;

public class Savedcardspagetestcases extends TestBase {
	Myaccountpage myaccount;
	Signinpage signinpage;
	Savedcardspage savedcards;
	public Savedcardspagetestcases() {
		 super();
	}
	
	 @BeforeMethod
		public void setup() throws InterruptedException {
			initialization();
			myaccount=new 	Myaccountpage();
			signinpage=new Signinpage();
			savedcards=new Savedcardspage();
			signinpage.Signin1(prop.getProperty("Emailid"), prop.getProperty("password"));
			
		}
	 
	 @Test(priority=1)
		public void mysavedcardstest() throws InterruptedException {
		 savedcards.mysavedcards();
		
		}
	 @Test(priority=2)
		public void mysavedcardstest2() throws InterruptedException {
		 savedcards.mysavedcards2();
		
		}
	 
	 @Test(priority=3)
		public void deletecardtest() throws InterruptedException {
		 savedcards.deletecard();
		}
	 
	 @Test(priority=4)
		public void deletecancelcardtest() throws InterruptedException {
		 savedcards.deletecancelcard();
		}
		@AfterMethod
		public void teardown() {
			driver.quit();
		}
}
