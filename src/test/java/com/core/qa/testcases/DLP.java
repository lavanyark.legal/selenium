package com.core.qa.testcases;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;



import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.core.qa.util.TestUtil;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DLP {
	



		
//		@Test
//		public void warehouseadding() throws InterruptedException {
//			WebDriver driver;
//			WebDriverManager.chromedriver().setup();
//			driver = new ChromeDriver(); 
//			driver.get("https://pod2-dlp.fayastage.com:7002/login");
//			driver.manage().window().maximize();
//			driver.findElement(By.xpath("//input[@formcontrolname='username']")).sendKeys("nithin@fayausa.com");
//			driver.findElement(By.xpath("//input[@formcontrolname='password']")).sendKeys("swordfish");
//			driver.findElement(By.xpath("//button[@type='submit']")).click();
//			Thread.sleep(5000);
//			driver.get("https://pod2-dlp.fayastage.com:7002/warehouse/warehouse-management/all-warehouse/ea49174f-b209-4372-8af0-010631c64447");
//			
//			driver.findElement(By.xpath("//a[@id='warehouse']")).click();
//			
//			
//			for(int i=2;i<=36;i++) {
//				Thread.sleep(2000);
//				driver.findElement(By.xpath("//button[contains(text(),'Add Warehouse')]")).click();
//
//				driver.findElement(By.xpath("//input[@formcontrolname='code']")).sendKeys("W0"+i);
//				driver.findElement(By.xpath("//input[@formcontrolname='name']")).sendKeys("WareHouse"+i);
//				Select select=new Select(driver.findElement(By.xpath("//Select[@formcontrolname='type']")));
//				select.selectByVisibleText("Small-Size");
//				driver.findElement(By.xpath("//input[@formcontrolname='total_capacity']")).sendKeys("1200");
//				Select select1=new Select(driver.findElement(By.xpath("//select[@formcontrolname='status']")));
//				select1.selectByVisibleText("Active");
//				driver.findElement(By.xpath("//input[@id='address_1']")).sendKeys("address"+i);
//				driver.findElement(By.xpath("//input[@id='city']")).sendKeys("Ontario");
//				driver.findElement(By.xpath("//*[@id=\"warehouse-panel\"]/div/div/div/app-warehouse-list/app-modal-popup/div/div/div[2]/app-warehouse-form/form/app-shared-address-form/div[2]/div[2]/div/select")).sendKeys("CA - California");
//				driver.findElement(By.xpath("//input[@id='zip']")).sendKeys("91761");
//				driver.findElement(By.xpath("//input[@id='phone']")).sendKeys("(232) 132-1321 x31231");
//				driver.findElement(By.xpath("//input[@id='email']")).sendKeys("fayaqa@yopmail.com");
//				//Thread.sleep(5000);
//				driver.findElements(By.xpath("//button[@type='submit']")).get(1).click();
//
//				
//
//			}
//	}
	
	

			@Test(dataProvider="gettestData")
			public void skuadding(String SKU,String Itemname,String Description,String Barcode,String EAN,String UPC,String FNSKU,String Insurencevalue,
					String itemvalue,String length,String Width,String height,String weight,String lowstock,String Battery,
					String status,String itemdetails,String Packageboxname,String dl,String dw,String dh,String totalitemcapacity) throws InterruptedException {
				WebDriver driver;
				WebDriverManager.chromedriver().setup();
				driver = new ChromeDriver(); 
				driver.get("https://pod2-dlp.fayastage.com:7001/");
				driver.manage().window().maximize();
				driver.findElement(By.xpath("//input[@formcontrolname='username']")).sendKeys("faya007@yopmail.com");
				driver.findElement(By.xpath("//input[@formcontrolname='password']")).sendKeys("Faya@123");
				driver.findElement(By.xpath("//button[@type='submit']")).click();
				Thread.sleep(5000);
				driver.get("https://pod2-dlp.fayastage.com:7001/inventory/sku-management/all-sku");
				
				
				
			
					Thread.sleep(2000);
					driver.findElement(By.xpath("//button[contains(text(),' Create New SKU ')]")).click();

					driver.findElement(By.xpath("//input[@formcontrolname='sku']")).sendKeys(SKU);
					driver.findElement(By.xpath("//input[@formcontrolname='name']")).sendKeys(Itemname);
					driver.findElement(By.xpath("//input[@formcontrolname='short_description']")).sendKeys(Description);
					driver.findElement(By.xpath("//input[@formcontrolname='barcode']")).sendKeys(Barcode);
					
					driver.findElement(By.xpath("//input[@formcontrolname='ean']")).sendKeys(EAN);
					driver.findElement(By.xpath("//input[@formcontrolname='upc']")).sendKeys(UPC);
					driver.findElement(By.xpath("//input[@formcontrolname='fnsku']")).sendKeys(FNSKU);
					driver.findElement(By.xpath("//input[@formcontrolname='insurance_value']")).sendKeys(Insurencevalue);
					driver.findElement(By.xpath("//input[@formcontrolname='cost_price']")).sendKeys(itemvalue);
					driver.findElement(By.xpath("//input[@formcontrolname='length']")).sendKeys(length);
					driver.findElement(By.xpath("//input[@formcontrolname='width']")).sendKeys(Width);
					driver.findElement(By.xpath("//input[@formcontrolname='height']")).sendKeys(height);
					driver.findElement(By.xpath("//input[@formcontrolname='weight']")).sendKeys(weight);
					driver.findElement(By.xpath("//input[@formcontrolname='lowstock_alert_qty']")).sendKeys(lowstock);
					Select select =new Select(driver.findElement(By.xpath("//select[@formcontrolname='battery']")));
					select.selectByVisibleText(Battery);
					Select select1 =new Select(driver.findElement(By.xpath("//select[@formcontrolname='status']")));
					select1.selectByVisibleText(status);
					
					driver.findElement(By.xpath("//textarea[@formcontrolname='description']")).sendKeys(itemdetails);
					driver.findElement(By.xpath("//button[contains(text(),' Add New Package Box ')]")).click();
					driver.findElements(By.xpath("//input[@formcontrolname='name']")).get(1).sendKeys(Packageboxname);
					driver.findElements(By.xpath("//input[@formcontrolname='length']")).get(1).sendKeys(dl);
					driver.findElements(By.xpath("//input[@formcontrolname='width']")).get(1).sendKeys(dw);
					driver.findElements(By.xpath("//input[@formcontrolname='height']")).get(1).sendKeys(dh);
					driver.findElement(By.xpath("//input[@formcontrolname='qty']")).sendKeys(totalitemcapacity);
//
					driver.findElements(By.xpath("//button[contains(text(),'Add')]")).get(1).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//button[contains(text(),'Create New SKU')]")).click();		

			


		}
			
//			@Test
//			public void dlptest3() throws InterruptedException {
//				WebDriver driver;
//				WebDriverManager.chromedriver().setup();
//				driver = new ChromeDriver(); 
//				driver.get("https://pod2-dlp.fayastage.com:7001/");
//				driver.manage().window().maximize();
//				driver.findElement(By.xpath("//input[@formcontrolname='username']")).sendKeys("faya007@yopmail.com");
//				driver.findElement(By.xpath("//input[@formcontrolname='password']")).sendKeys("123456");
//				driver.findElement(By.xpath("//button[@type='submit']")).click();
//				Thread.sleep(5000);
//				driver.get("https://pod2-dlp.fayastage.com:7001/shipment/inbound-shipment-management/all-inbound-shipment");
//				
//				
//				
//				for(int i=1;i<=20;i++) {
//					Thread.sleep(2000);
//					driver.findElement(By.xpath("//a[contains(text(),'Create New Shipment')]")).click();
//
//					driver.findElement(By.xpath("//input[@formcontrolname='reference_1']")).sendKeys("Reference1");
//					driver.findElement(By.xpath("//input[@formcontrolname='reference_2']")).sendKeys("Reference2");
//					driver.findElement(By.xpath("//input[@formcontrolname='reference_3']")).sendKeys("Reference3");
//					Select select=new Select(driver.findElement(By.xpath("//select[@formcontrolname='warehouse']")));
//					select.selectByVisibleText("Warehouse 1");
//					
//					
//					driver.findElement(By.xpath("//input[@formcontrolname='estimated_arrival_date']")).sendKeys("01/2/2022");
//					Select select1=new Select(driver.findElement(By.xpath("//select[@formcontrolname='signature_type']")));
//					select1.selectByVisibleText("Direct Signature Required");
//					
//					
//					driver.findElement(By.xpath("//input[@formcontrolname='declared_value']")).sendKeys("2500");
//					
//					Select select2=new Select(driver.findElement(By.xpath("//select[@class='form-control']")));
//					select2.selectByValue("aa06f7ef-ae75-4efb-aa67-b3841cc03cff");
//					
//					Select select3=new Select(driver.findElement(By.xpath("//select[@formcontrolname='shipping_method']")));
//					select3.selectByVisibleText("Truck");
//					
//					driver.findElement(By.xpath("//button[contains(text(),'Save')]")).click();
//					
//					Thread.sleep(5000);;
//
//					driver.findElement(By.xpath("//button[contains(text(),'Add Pallet')]")).click();
//				
//					Select select4=new Select(driver.findElement(By.xpath("//select[@formcontrolname='pallet_type']")));
//					select4.selectByVisibleText("Pallet ISO 0 - 1/2 EURO Pallet (GS1 Code)");
//
//					Select select5=new Select(driver.findElement(By.xpath("//select[@formcontrolname='sku']")));
//					select5.selectByVisibleText("Itemname2");
//
//					Thread.sleep(3000);
//					Select select6=new Select(driver.findElement(By.xpath("//select[@formcontrolname='carton_type']")));
//					select6.selectByVisibleText("Box2");
//					
//					driver.findElement(By.xpath("//input[@formcontrolname='number_of_cartons']")).sendKeys("10");
//					driver.findElement(By.xpath("//input[@formcontrolname='quantity_per_carton']")).sendKeys("10");
//					
//					driver.findElement(By.xpath("//input[@formcontrolname='total_quantity']")).click();
//
//					driver.findElement(By.xpath("//button[contains(text(),'Save')]")).click();
//
//					
//					Thread.sleep(2000);
//					driver.findElement(By.xpath("//button[contains(text(),'Create New Shipment')]")).click();		
//
//				}
//			
//		
//		
//	}
			
//			@Test
//			public void dlptest4() throws InterruptedException {
//				WebDriver driver;
//				WebDriverManager.chromedriver().setup();
//				driver = new ChromeDriver(); 
//				driver.get("https://pod2-dlp.fayastage.com:7001/");
//				driver.manage().window().maximize();
//				driver.findElement(By.xpath("//input[@formcontrolname='username']")).sendKeys("fayaqa007@yopmail.com");
//				driver.findElement(By.xpath("//input[@formcontrolname='password']")).sendKeys("Test@123");
//				driver.findElement(By.xpath("//button[@type='submit']")).click();
//				Thread.sleep(5000);
//				driver.get("https://pod2-dlp.fayastage.com:7001/shipment/inbound-shipment-management/all-inbound-shipment");
//				
//				
//				
//				for(int i=1;i<=10;i++) {
//					Thread.sleep(2000);
//					driver.findElement(By.xpath("//a[contains(text(),'Create New Shipment')]")).click();
//
//					driver.findElement(By.xpath("//input[@formcontrolname='reference_1']")).sendKeys("Reference1");
//					driver.findElement(By.xpath("//input[@formcontrolname='reference_2']")).sendKeys("Reference2");
//					driver.findElement(By.xpath("//input[@formcontrolname='reference_3']")).sendKeys("Reference3");
//					Select select=new Select(driver.findElement(By.xpath("//select[@formcontrolname='warehouse']")));
//					select.selectByVisibleText("Warehouse 3");
//					
//					
//					driver.findElement(By.xpath("//input[@formcontrolname='estimated_arrival_date']")).sendKeys("01/12/2021");
//					Select select1=new Select(driver.findElement(By.xpath("//select[@formcontrolname='signature_type']")));
//					select1.selectByVisibleText("In Direct Signature Required");
//					
//					
//					driver.findElement(By.xpath("//input[@formcontrolname='declared_value']")).sendKeys("2500");
//					
//					Select select2=new Select(driver.findElement(By.xpath("//select[@class='form-control']")));
//					select2.selectByValue("25afa527-8063-4ab9-8af6-391dda9a7a1e");
//					
//					Select select3=new Select(driver.findElement(By.xpath("//select[@formcontrolname='shipping_method']")));
//					select3.selectByVisibleText("Truck");
//					
//					driver.findElement(By.xpath("//button[contains(text(),'Save')]")).click();
//					
//					Thread.sleep(5000);
//
//					driver.findElement(By.xpath("//button[contains(text(),'Add Carton')]")).click();
//				
//					
//
//					Select select5=new Select(driver.findElement(By.xpath("//select[@formcontrolname='sku']")));
//					select5.selectByVisibleText("Itemname3");
//
//					Thread.sleep(3000);
//					Select select6=new Select(driver.findElement(By.xpath("//select[@formcontrolname='carton_type']")));
//					select6.selectByVisibleText("Box3");
//					
//					driver.findElement(By.xpath("//input[@formcontrolname='number_of_cartons']")).sendKeys("5");
//					driver.findElement(By.xpath("//input[@formcontrolname='quantity_per_carton']")).sendKeys("5");
//					
//					driver.findElement(By.xpath("//input[@formcontrolname='total_quantity']")).click();
//
//					driver.findElement(By.xpath("//button[contains(text(),'Save')]")).click();
//
//					
//					Thread.sleep(2000);
//					driver.findElement(By.xpath("//button[contains(text(),'Create New Shipment')]")).click();		
//
//				}
//			
//		
//		
//	}
			
//			@Test
//			public void dlptest5() throws InterruptedException, AWTException {
//				Robot robot=new Robot();
//
//				WebDriver driver;
//				WebDriverManager.chromedriver().setup();
//				driver = new ChromeDriver(); 
//				driver.get("https://pod2-dlp.fayastage.com:7001/");
//				driver.manage().window().maximize();
//				driver.findElement(By.xpath("//input[@formcontrolname='username']")).sendKeys("fayaqa007@yopmail.com");
//				driver.findElement(By.xpath("//input[@formcontrolname='password']")).sendKeys("Test@123");
//				driver.findElement(By.xpath("//button[@type='submit']")).click();
//				Thread.sleep(5000);
//				driver.get("https://pod2-dlp.fayastage.com:7001/shipment/inbound-shipment-management/all-inbound-shipment");
//				
//				
//				
//				for(int i=1;i<=10;i++) {
//					Thread.sleep(2000);
//					driver.findElement(By.xpath("//a[contains(text(),'Create New Shipment')]")).click();
//
//					driver.findElement(By.xpath("//input[@formcontrolname='reference_1']")).sendKeys("Reference1");
//					driver.findElement(By.xpath("//input[@formcontrolname='reference_2']")).sendKeys("Reference2");
//					driver.findElement(By.xpath("//input[@formcontrolname='reference_3']")).sendKeys("Reference3");
//					Select select=new Select(driver.findElement(By.xpath("//select[@formcontrolname='warehouse']")));
//					select.selectByVisibleText("Warehouse 3");
//					
//					
//					driver.findElement(By.xpath("//input[@formcontrolname='estimated_arrival_date']")).sendKeys("01/12/2021");
//					Select select1=new Select(driver.findElement(By.xpath("//select[@formcontrolname='signature_type']")));
//					select1.selectByVisibleText("In Direct Signature Required");
//					
//					
//					driver.findElement(By.xpath("//input[@formcontrolname='declared_value']")).sendKeys("2500");
//					
//					Select select2=new Select(driver.findElement(By.xpath("//select[@class='form-control']")));
//					select2.selectByValue("873d64e1-83b9-4af6-9954-c4b7bb6cd759");
//					
//					Select select3=new Select(driver.findElement(By.xpath("//select[@formcontrolname='shipping_method']")));
//					select3.selectByVisibleText("Truck");
//					
//					driver.findElement(By.xpath("//button[contains(text(),'Save')]")).click();
//					
//					Thread.sleep(5000);
//					
//					
//					for(int j=0;j<30;j++) {
//						
//						driver.findElement(By.xpath("//button[contains(text(),'Add Pallet')]")).click();
//						
//						Select select4=new Select(driver.findElement(By.xpath("//select[@formcontrolname='pallet_type']")));
//						select4.selectByVisibleText("10*10*10 box");
//			
//						Select select5=new Select(driver.findElement(By.xpath("//select[@formcontrolname='sku']")));
//						select5.selectByIndex(j);
//			
//						Thread.sleep(3000);
//						Select select6=new Select(driver.findElement(By.xpath("//select[@formcontrolname='carton_type']")));
//						select6.selectByIndex(0);
//								
//						driver.findElement(By.xpath("//input[@formcontrolname='number_of_cartons']")).sendKeys("1");
//						driver.findElement(By.xpath("//input[@formcontrolname='quantity_per_carton']")).sendKeys("2");
//								
//						driver.findElement(By.xpath("//input[@formcontrolname='total_quantity']")).click();
//			
//						driver.findElement(By.xpath("//button[contains(text(),'Save')]")).click();
//			
//								
//						Thread.sleep(2000);
//					}
//					
//					
//					
//					for(int k=0;k<50;k++) {
//						driver.findElement(By.xpath("//button[contains(text(),'Upload Document')]")).click();
//						Thread.sleep(2000);
//						
//						Select select6=new Select(driver.findElement(By.xpath("//select[@formcontrolname='type']")));
//						select6.selectByVisibleText("Insurance Document");
//						driver.findElement(By.xpath("//input[@type='file']")).sendKeys(System.getProperty("user.dir")+"\\Testdatafolder\\logo_e6d3db04afcf4e7aad52fd6961724789.png");
//						Thread.sleep(5000);
//						driver.findElement(By.xpath("//button[@type='submit']")).click();
//						Thread.sleep(2000);
//
//					}
//					
//
//					driver.findElement(By.xpath("//button[contains(text(),'Create New Shipment')]")).click();	
//					
//
//				}
//			
//		
//		
//	}
	
	
//	@Test
//	public void dlptest6() throws InterruptedException, AWTException {
//
//		WebDriver driver;
//		WebDriverManager.chromedriver().setup();
//		driver = new ChromeDriver(); 
//		driver.get("https://pod2-dlp.fayastage.com:7001/");
//		driver.manage().window().maximize();
//		driver.findElement(By.xpath("//input[@formcontrolname='username']")).sendKeys("fayaqa007@yopmail.com");
//		driver.findElement(By.xpath("//input[@formcontrolname='password']")).sendKeys("Test@123");
//		driver.findElement(By.xpath("//button[@type='submit']")).click();
//		Thread.sleep(5000);
//		driver.get("https://pod2-dlp.fayastage.com:7001/inventory/sku-management/edit-sku/1af6a9e0-dca9-4b39-b79c-7e2d86fdd86b");
//		
//		for(int i=1;i<30;i++) {
//			driver.findElement(By.xpath("//button[contains(text(),' Add New Package Box ')]")).click();
//
//			driver.findElements(By.xpath("//input[@formcontrolname='name']")).get(1).sendKeys("Box"+i);
//			driver.findElements(By.xpath("//input[@formcontrolname='length']")).get(1).sendKeys("10");
//			driver.findElements(By.xpath("//input[@formcontrolname='width']")).get(1).sendKeys("10");
//			driver.findElements(By.xpath("//input[@formcontrolname='height']")).get(1).sendKeys("10");
//			driver.findElement(By.xpath("//input[@formcontrolname='qty']")).sendKeys("100");
//			driver.findElements(By.xpath("//button[contains(text(),'Add')]")).get(1).click();
//
//
//		}
//		driver.findElement(By.xpath("//button[contains(text(),'Update SKU')]")).click();
//
//		
			//}


//@Test
//public void dlptest6() throws InterruptedException, AWTException {
//
//	WebDriver driver;
//	WebDriverManager.chromedriver().setup();
//	driver = new ChromeDriver(); 
//	driver.get("https://pod2-dlp.fayastage.com:7001/");
//	driver.manage().window().maximize();
//	driver.findElement(By.xpath("//input[@formcontrolname='username']")).sendKeys("faya007@yopmail.com");
//	driver.findElement(By.xpath("//input[@formcontrolname='password']")).sendKeys("123456");
//	driver.findElement(By.xpath("//button[@type='submit']")).click();
//	Thread.sleep(5000);
//	driver.get("https://pod2-dlp.fayastage.com:7001/shipment/parcel-outbound-management/all-parcel-outbound");
//	
//	for(int i=1;i<10;i++) {
//		
//		driver.findElement(By.xpath("//a[contains(text(),'Create New Parcel Order')]")).click();
//
//		driver.findElement(By.xpath("//input[@formcontrolname='customer_po']")).sendKeys("PO"+i);
//		driver.findElement(By.xpath("//input[@formcontrolname='reference_1']")).sendKeys("REF"+i);
//		driver.findElement(By.xpath("//input[@formcontrolname='reference_2']")).sendKeys("REF"+i);
//		driver.findElement(By.xpath("//input[@formcontrolname='reference_3']")).sendKeys("REF"+i);
//		Select select=new Select(driver.findElement(By.xpath("//select[@formcontrolname='signature_type']")));
//		select.selectByVisibleText("Direct Signature Required");
//		
//		driver.findElement(By.xpath("//input[@formcontrolname='declared_value']")).sendKeys("1500");
//		Thread.sleep(5000);
//		//driver.findElements(By.xpath("//label[contains(text(),'Yes')]//parent::div[@class='custom-control custom-radio mr-4']")).get(0).click();
//		//driver.findElement(By.xpath("//input[@formcontrolname='is_rush_order']")).click();
//		driver.findElement(By.xpath("//input[@formcontrolname='to_name']")).sendKeys("name"+i);
//		driver.findElement(By.xpath("//input[@formcontrolname='to_address_1']")).sendKeys("address"+i);
//		driver.findElement(By.xpath("//input[@formcontrolname='to_city']")).sendKeys("Ontario");
//		Select select1=new Select(driver.findElement(By.xpath("//select[@formcontrolname='to_state']")));
//		select1.selectByVisibleText("CA - California");
//		driver.findElement(By.xpath("//input[@formcontrolname='to_zip']")).sendKeys("91761");
//		driver.findElement(By.xpath("//input[@formcontrolname='to_phone']")).sendKeys("(321) 321-3123 x12313");
//	
//		Select select3=new Select(driver.findElement(By.xpath("//select[@formcontrolname='shipping_method']")));
//		select3.selectByVisibleText("UPS-Normal");
//		driver.findElement(By.xpath("//input[@formcontrolname='estimated_shipping_date']")).sendKeys("01/31/2022");
//		driver.findElement(By.xpath("//button[contains(text(),'Save')]")).click();
//		
//		Thread.sleep(2000);
//		driver.findElement(By.xpath("//div[@id='product_information']//button")).click();
//		
//		Select select4=new Select(driver.findElement(By.xpath("//select[@formcontrolname='product']")));
//		select4.selectByValue("067d45c2-fc90-4dab-bc33-43e09170b573");
//		
//		driver.findElement(By.xpath("//input[@formcontrolname='qty']")).sendKeys("5");
//		driver.findElement(By.xpath("//button[contains(text(),'Save')]")).click();
//		Thread.sleep(2000);
//		
//		driver.findElement(By.xpath("//div[@id='shipment_info']//button[2]")).click();
//		
//		Thread.sleep(2000);
//		driver.findElement(By.xpath("//button[contains(text(),'Create New Parcel Order')]")).click();
//		Thread.sleep(2000);
//	}
//
//	
//		}
}

