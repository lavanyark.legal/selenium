package com.core.qa.testcases;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.GuestAddtocartBlank;
import com.core.qa.pages.flatPCTQuote;
import com.core.qa.pages.ProductListingpage;
import com.core.qa.pages.Signinpage;
import com.core.qa.pages.threedpctquote;

public class threedembpctquotetestcases extends TestBase{
	ProductListingpage productlistingpage;
	GuestAddtocartBlank productdetailpage;
	threedpctquote threepct;
	Signinpage signinpage;
	@BeforeMethod
		public void setup() throws InterruptedException {
			initialization();
			
			productlistingpage=new 	ProductListingpage();
			productdetailpage=new GuestAddtocartBlank();
			productlistingpage.randomproductclick();
			threepct=new threedpctquote();
		}
//	 
//	 @Test(priority=44)
//		public void quickquotetest1() throws InterruptedException, AWTException {
//		 boolean flag=threepct.Quickquotetest1();
//		 Assert.assertTrue(flag);
//		}
//	 
//	 @Test(priority=45)
//		public void quickquotetest2() throws InterruptedException, AWTException {
//		 boolean flag=threepct.Quickquotetest2();
//		 Assert.assertTrue(flag);
//		}
	 @Test(priority=46)
		public void quickquotetest4() throws InterruptedException, AWTException {
		 signinpage=new Signinpage();
		 signinpage.Signin1(prop.getProperty("Emailid"),prop.getProperty("password"));
		 threepct.Quickquotetest4();
		
		}
	 @Test(priority=46)
		public void quickquotetest5() throws InterruptedException, AWTException {
		threepct.Quickquotetest5();
		
		}
	 @Test(priority=47)
		public void quickquotetest6() throws InterruptedException, AWTException {
		boolean flag=threepct.Quickquotetest6();
		 Assert.assertTrue(flag);
		}
	 @Test(priority=48)
		public void quickquotetest7() throws InterruptedException, AWTException {
		boolean flag=threepct.Quickquotetest7();
		 Assert.assertTrue(flag);
		}
	 
	 @Test(priority=49)
		public void quickquotetest3() throws InterruptedException, AWTException {
		boolean flag=threepct.Quickquotetest3();
		 Assert.assertTrue(flag);
		}
		@AfterMethod
		public void teardown() {
			driver.quit();
		}

}
