package com.core.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.Accountdashboardpage;
import com.core.qa.pages.Myaccountpage;
import com.core.qa.pages.Signinpage;

public class AccountDashboardtestcases extends TestBase {
	Myaccountpage myaccount;
	Signinpage signinpage;
	Accountdashboardpage accountdashboard;
	 public AccountDashboardtestcases() {
		 super();
	}
	 @BeforeMethod
		public void setup() throws InterruptedException {
			initialization();
			myaccount=new 	Myaccountpage();
			signinpage=new Signinpage();
			accountdashboard=new Accountdashboardpage();
			signinpage.Signin1(prop.getProperty("Emailid"), prop.getProperty("password"));
			
		}
//	 @Test(priority=1)
//		public void accountdashboardtestable() throws InterruptedException {
//		 	boolean flag =accountdashboard.accountdashboardtest();
//		 	Assert.assertTrue(flag);
//			
//		}
//	 @Test(priority=2)
//		public void accountdashboardcustomsearchtest() throws InterruptedException {
//		 accountdashboard.accountdashboardcustomtablesearchtest();
//		 
//			
//			
//		}
//	 @Test(priority=3)
//		public void accountdashboardcustomstatustest() throws InterruptedException {
//		 accountdashboard.accountdashboardcustomstatustest();
//		 	
//			
//			
//		}
//	 @Test(priority=4)
//		public void accountdashboardcustomorderdetailstest() throws InterruptedException {
//		 accountdashboard.accountdashboardcustomorderdetailstest();
//		 	
//			
//			
//		}
//	 
//	 @Test(priority=5)
//		public void accountdashboardcustompaginationstest() throws InterruptedException {
//		 accountdashboard.accountdashboardcustompaginationtest();
//		 	
//			
//			
//		}
	 @Test(priority=6)
		public void accountdashboardblanksearchtest() throws InterruptedException {
		 accountdashboard.accountdashboardblanktablesearchtest();
		 	
			
			
		}
	 
	 @Test(priority=7)
		public void accountdashboardblankstatustest() throws InterruptedException {
		 accountdashboard.accountdashboardblankstatustest();
		 	
			
			
		}
	 @Test(priority=8)
		public void accountdashboardblankorderdetailtest() throws InterruptedException {
		 accountdashboard.accountdashboardblankorderdetailstest();
		 	
			
			
		}
	 @Test(priority=9)
		public void accountdashboardblankpaginationstest() throws InterruptedException {
		 accountdashboard.accountdashboardblankpaginationtest();
		 	
			
			
		}
	 

		@AfterMethod
		public void teardown() {
			driver.quit();
		}

}
