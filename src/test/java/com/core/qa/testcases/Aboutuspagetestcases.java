package com.core.qa.testcases;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.Aboutuspage;
import com.core.qa.pages.Homepage;

public class Aboutuspagetestcases extends TestBase {
	
	Aboutuspage aboutpage;
	Homepage homepage;
	 
	public Aboutuspagetestcases() {
		 super();
	}
	@BeforeMethod
	public void setup() throws InterruptedException {
		initialization();
		aboutpage=new Aboutuspage();
		homepage=new Homepage();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		}

	@Test(priority=1)
	public void abouturltest() throws InterruptedException {
		String url=aboutpage.aboutusurl();
		Assert.assertEquals(url, prop.getProperty("url")+"about");
	}
	
	@Test(priority=2)
	public void aboutbannertest() throws InterruptedException {
		boolean flag=aboutpage.aboutusbannercheck();
		Assert.assertTrue(flag);
	}
	
	@Test(priority=3)
	public void aboutusheadercheck() throws InterruptedException {
		boolean flag=aboutpage.aboutusheadercheck();
		Assert.assertTrue(flag);
	}
	@AfterMethod
	public void teardown() {
		driver.quit();
	}

}
