package com.core.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.FAQAccountpage;
import com.core.qa.pages.Myaccountpage;
import com.core.qa.pages.Signinpage;

public class faqaccountpagetestcases extends TestBase {
	
	FAQAccountpage faq; 
	Signinpage signinpage;
	public faqaccountpagetestcases() {
		 super();
	}

	 @BeforeMethod
		public void setup() throws InterruptedException {
			initialization();
			faq=new FAQAccountpage();
			signinpage=new Signinpage();
			signinpage.Signin1(prop.getProperty("Emailid"), prop.getProperty("password"));
			faq.faqtest();
		}
	
//	 
//	 @Test(priority=2)
//		public void requestaquotelinktest() throws InterruptedException {
//		 faq.requestaquotelink();
//		
//		}
	 
//	 @Test(priority=3)
//		public void createyourownlinktest() throws InterruptedException {
//		 faq.createyourownlink();
//		
//		}
//	 @Test(priority=4)
//		public void myquoterequestkinktest() throws InterruptedException {
//		 faq.myquoterequestlink();
//		
//		}
	 
	 @Test(priority=5)
		public void myorderslinktest() throws InterruptedException {
		 faq.myorderslink();
		
		}
	 @AfterMethod
		public void teardown() {
			driver.quit();
		}
}
