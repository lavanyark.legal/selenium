package com.core.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.ProductListingpage;
import com.core.qa.pages.Productdetailpage;
import com.core.qa.pages.Shoppingcartpage;

public class Shoppingcarpagetestcases extends TestBase {
	
	ProductListingpage productlistingpage;
	Productdetailpage productdetailpage;
	Shoppingcartpage shoppingcartpage;
	 public Shoppingcarpagetestcases() {
		 super();
	}

	 @BeforeMethod
		public void setup() throws InterruptedException {
			initialization();
			productlistingpage=new 	ProductListingpage();
			productdetailpage=new Productdetailpage();
			productlistingpage.randomproductclick();
			shoppingcartpage=new Shoppingcartpage();
			
		}
	 
	 @Test
		public void pricetest() throws InterruptedException {
		 productdetailpage.SingleProductdiffcoluraddtocart();
			int qty[]=shoppingcartpage.qtycheck();
			int qty1=qty[0];
			int qty2=qty[1];
			
			Assert.assertEquals(qty1, qty2);
			
		}
}
