package com.core.qa.testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.Homepage;
import com.core.qa.pages.Signinpage;

public class HomepageTestcases extends TestBase {
	

	Homepage homepage;
	 
	public HomepageTestcases() {
		 super();
	}
	
	@BeforeMethod
	public void setup() {
		initialization();
		homepage=new Homepage();
		
	}
	
//	@Test(priority=1)
//	public void Logotest() throws InterruptedException {
//		logger.info("Checking the Logo found or not");
//		boolean flag =homepage.logodisplayed();	
//		logger.info("Searching for the Logo");
//		Assert.assertTrue(flag);
//		logger.info("Logo displayed in the homepage ");
//	}
//	
//	@Test(priority=2)
//	public void Searchtest() throws InterruptedException {
//		logger.info("Checking the Search textbox found or not");
//		boolean flag =homepage.Searchdisplayed();
//		logger.info("Searching for the Search textbox");
//		Assert.assertTrue(flag);
//		logger.info("Search textbox displayed in the homepage ");
//	}
//	
//	@Test(priority=3)
//	public void Searchdropdown() throws InterruptedException {
//		String productname =homepage.Searchdropdowntest(prop.getProperty("styleno"));
//		String productnamedetailpage=driver.findElement(By.xpath("//div//h2[@class='style-number fw-500 mr-3 font-sm']")).getText();
//		String testString = productnamedetailpage;
//		  int startIndex = testString.indexOf(":");
//		//  int endIndex = testString.indexOf(":");
//		  String subString = testString.substring(startIndex+1);
//		  System.out.println(subString);
//		
//		System.out.println(productnamedetailpage);
//		Assert.assertTrue(subString.contains(productname));
//	}
//	
//	@Test(priority=4)
//	public void Searchbutton() throws InterruptedException {
//		String productname =homepage.Searchbuttontest((prop.getProperty("styleno")));
//		Assert.assertTrue(productname.contains((prop.getProperty("styleno"))));
//	}
//	
//	@Test(priority=5)
//	public void Bannerpresent() throws InterruptedException {
//		
//		boolean flag=homepage.Bannerchecking();
//		Assert.assertTrue(flag);
//	}
//	@Test(priority=6)
//	public void Banner1linktest() throws InterruptedException {
//		
//		boolean actualurl=homepage.Bannerlinkchecking1();
//		Assert.assertTrue(actualurl);	
//	}
//	@Test(priority=7)
//	public void Banner2linktest() throws InterruptedException {
//		
//		boolean actualurl=homepage.Bannerlinkchecking2();
//		Assert.assertTrue(actualurl);	
//	}
//	@Test(priority=8)
//	public void Banner3linktest() throws InterruptedException {
//		
//		boolean actualurl=homepage.Bannerlinkchecking3();
//		Assert.assertTrue(actualurl);	
//	}
//	@Test(priority=9)
//	public void Banner4linktest() throws InterruptedException {
//		
//		boolean actualurl=homepage.Bannerlinkchecking4();
//		Assert.assertTrue(actualurl);	
//	}
//	@Test(priority=10)
//	public void ads1linktest() throws InterruptedException {
//		
//		boolean actualurl=homepage.adslinkcheck1();
//		Assert.assertTrue(actualurl);	
//	}
//	@Test(priority=11)
//	public void ads2linktest() throws InterruptedException {
//		
//		boolean actualurl=homepage.adslinkcheck2();
//		Assert.assertTrue(actualurl);	
//	}
//	@Test(priority=12)
//	public void ads3linktest() throws InterruptedException {
//		JavascriptExecutor js = (JavascriptExecutor) driver;
//		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
//		boolean actualurl=homepage.adslinkcheck3();
//		Assert.assertTrue(actualurl);	
//	}
//	@Test(priority=13)
//	public void ads4linktest() throws InterruptedException {
//		JavascriptExecutor js = (JavascriptExecutor) driver;
//		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
//		boolean actualurl=homepage.adslinkcheck4();
//		Assert.assertTrue(actualurl);	
//	}
//	@Test(priority=14)
//	public void ads5linktest() throws InterruptedException {
//		JavascriptExecutor js = (JavascriptExecutor) driver;
//		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
//		boolean actualurl=homepage.adslinkcheck5();
//		Assert.assertTrue(actualurl);	
//	}
//	@Test(priority=15)
//	public void ads6linktest() throws InterruptedException {
//		JavascriptExecutor js = (JavascriptExecutor) driver;
//		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
//		boolean actualurl=homepage.adslinkcheck6();
//		Assert.assertTrue(actualurl);	
//	}
//
//	
//	@Test(priority=17)
//	public void Widgetpresent() throws InterruptedException {
//		boolean flag=homepage.Widgetchecking();
//		Assert.assertTrue(flag);
//	}
//	
//	@Test(priority=18)
//	public void Megamenupresent() throws InterruptedException {
//		boolean flag=homepage.megamenuchecking();
//		Assert.assertTrue(flag);
//	}
//	
//	@Test(priority=19)
//	public void productsearchfilterlinktest() throws InterruptedException {
//		boolean currenturl=homepage.productsearchfiltercheck();
//		Assert.assertTrue(currenturl);		
//		}
//	@Test(priority=20)
//	public void createyourownlinktest() throws InterruptedException {
//		boolean currenturl=homepage.createyourowncheck();
//		Assert.assertTrue(currenturl);	
//	}
//	
//	@Test(priority=21)
//	public void Allproductslinktest() throws InterruptedException {
//		boolean currenturl=homepage.allproductscheck();
//		Assert.assertTrue(currenturl);	
//	}
//	
//	@Test(priority=22)
//	public void fivePanelCapchecklinktest() throws InterruptedException {
//		boolean currenturl=homepage.fivePanelCapcheck();
//		Assert.assertTrue(currenturl);	
//	}
//	@Test(priority=23)
//	public void sixPanelCapchecktest() throws InterruptedException {
//		boolean currenturl=homepage.sixPanelCapcheck();
//		Assert.assertTrue(currenturl);	
//		}
//	
//	@Test(priority=24)
//	public void FlatVisorschecklinktest() throws InterruptedException {
//		boolean currenturl=homepage.FlatVisorscheck();
//		Assert.assertTrue(currenturl);
//		}
//	
//	@Test(priority=25)
//	public void CamoHatschecklinktest() throws InterruptedException {
//		boolean currenturl=homepage.CamoHatscheck();
//		Assert.assertTrue(currenturl);		
//	}
//	
//	@Test(priority=25)
//	public void TruckerCapschecklinktest() throws InterruptedException {
//		boolean currenturl=homepage.TruckerCapscheck();
//		Assert.assertTrue(currenturl);		
//	}
//	
//	@Test(priority=25)
//	public void Performancechecklinktest() throws InterruptedException {
//		boolean currenturl=homepage.Performancecheck();
//		Assert.assertTrue(currenturl);		
//	}
//	@Test(priority=26)
//	public void onsalelinktest() throws InterruptedException {
//		boolean currenturl=homepage.onsalecheck();
//		Assert.assertTrue(currenturl);			
//	}
//	
//	@Test(priority=27)
//	public void emptymailidnewslettertest() throws InterruptedException {
//		JavascriptExecutor js = (JavascriptExecutor) driver;
//		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
//			boolean flag=homepage.newsletterchecking1();
//			Assert.assertTrue(flag);
//		
//	}
//	
//	@Test(priority=28)
//	public void randommailidnewslettertest() throws InterruptedException {
//		JavascriptExecutor js = (JavascriptExecutor) driver;
//		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
//			boolean flag=homepage.newsletterchecking2(randomEmail());
//			Assert.assertTrue(flag);
//		
//	}
//	
//	@Test(priority=29)
//	public void alreadysubscribedmailnewslettertest() throws InterruptedException {
//		JavascriptExecutor js = (JavascriptExecutor) driver;
//		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
//			boolean flag=homepage.newsletterchecking3("fayaqa@yopmail.com");
//			Assert.assertTrue(flag);
//		
//	}
//
//	@Test(priority=30)
//	public void widgettest1() throws InterruptedException {
//			String text=homepage.widgetcheck1();
//			String listingpagetext=driver.findElements(By.xpath("//span[@class='breadcrumb-title active']//strong")).get(1).getText();
//			System.out.println(text);
//			System.out.println(listingpagetext);
//			Assert.assertTrue(listingpagetext.equalsIgnoreCase(text));
//
//		
//	}
//	@Test(priority=31)
//	public void widgettest2() throws InterruptedException {
//		JavascriptExecutor js = (JavascriptExecutor) driver;
//		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
//			String text=homepage.widgetcheck2();
//			String listingpagetext=driver.findElements(By.xpath("//span[@class='breadcrumb-title active']//strong")).get(1).getText();
//			Assert.assertTrue(listingpagetext.equalsIgnoreCase(text));
//		
//		
//	}
//	@Test(priority=32)
//	public void widgettest3() throws InterruptedException {
//		JavascriptExecutor js = (JavascriptExecutor) driver;
//		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
//			String text=homepage.widgetcheck3();
//			String listingpagetext=driver.findElements(By.xpath("//span[@class='breadcrumb-title active']//strong")).get(1).getText();
//			Assert.assertTrue(listingpagetext.equalsIgnoreCase(text));
//			
//		
//	}
//	@Test(priority=33)
//	public void widgettest4() throws InterruptedException {
//		JavascriptExecutor js = (JavascriptExecutor) driver;
//		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
//			String text=homepage.widgetcheck4();
//			String listingpagetext=driver.findElements(By.xpath("//span[@class='breadcrumb-title active']//strong")).get(1).getText();
//			Assert.assertTrue(listingpagetext.equalsIgnoreCase(text));
//					
//	}
//	
//	@Test(priority=34)
//	public void AboutuspageTest() throws InterruptedException {
//		JavascriptExecutor js = (JavascriptExecutor) driver;
//		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
//			boolean flag=homepage.aboutuscheck();
//			Assert.assertTrue(flag);
//		
//	}
//	@Test(priority=35)
//	public void ContactuspageTest() throws InterruptedException {
//		JavascriptExecutor js = (JavascriptExecutor) driver;
//		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
//			String text=homepage.contactuscheck();
//			Assert.assertEquals(text, prop.getProperty("url")+"contact");
//		
//	}
	@Test(priority=36)
	public void PrivacypolicypageTest() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			boolean text=homepage.privacypolicycheck();
			Assert.assertTrue(text);
		
	}
	@Test(priority=37)
	public void Termsandconditionstest() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			String text=homepage.termsandconditioncheck();
			Assert.assertEquals(text, prop.getProperty("url")+"terms");
		
	}
	@Test(priority=38)
	public void Orderstatustest() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			String text=homepage.orderstatuscheck();
			Assert.assertEquals(text, prop.getProperty("url")+"order-status");
		
	}
	@Test(priority=39)
	public void FAQtest() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			String text=homepage.FAQcheck();
			Assert.assertEquals(text, prop.getProperty("url")+"faq");
		
	}
	@Test(priority=40)
	public void RAQtest() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			String text=homepage.RAQcheck();
			Assert.assertEquals(text, prop.getProperty("url")+"raq");
		
	}
	
	@Test(priority=41)
	public void Createaccounttest() throws InterruptedException {
		String text=homepage.Createanaccountlinktest();
		Assert.assertEquals(text, prop.getProperty("url")+"create-account");
		
	}
	@Test(priority=42)
	public void Signintest() throws InterruptedException {
		boolean flag=homepage.Signinlinktest();
		Assert.assertTrue(flag);
	}
	@Test(priority=43)
	public void homeproductstest() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		Thread.sleep(5000);
		boolean flag=homepage.homepageproductschecking();
		Assert.assertTrue(flag);
	}
	
	@Test(priority=43)
	public void megamenulink() throws InterruptedException {
		boolean flag=homepage.menuslink();
		Assert.assertTrue(flag);
	}
	@AfterMethod
	public void teardown() {
		driver.quit();
	}


}
