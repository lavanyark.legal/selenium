package com.core.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.Aboutuspage;
import com.core.qa.pages.Contactuspage;
import com.core.qa.pages.Homepage;

public class Contactuspagetestcases extends TestBase {
	
	Contactuspage contactpage;
	Homepage homepage;
	
	public Contactuspagetestcases() {
		 super();
	}
	
	@BeforeMethod
	public void setup() throws InterruptedException {
		initialization();
		contactpage=new Contactuspage();
		homepage=new Homepage();
		homepage.contactuscheck();
	}

	@Test(priority=1)
	public void contactusurltest() {
		String url=contactpage.contactusurl();
		Assert.assertEquals(url, prop.getProperty("url")+"contact");
	}
	
	@Test(priority=2)
	public void contactformemptytestt() {
		boolean flag=contactpage.contactusformemptycheck();
		Assert.assertTrue(flag);
	}
	
	@Test(priority=3)
	public void contactusformcheck() {
		boolean flag=contactpage.contactusformcheck();
		Assert.assertTrue(flag);
	}
	@AfterMethod
	public void teardown() {
		driver.quit();
	}

}
