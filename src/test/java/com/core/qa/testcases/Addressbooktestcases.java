package com.core.qa.testcases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.Addressbookpage;
import com.core.qa.pages.Myaccountpage;
import com.core.qa.pages.Signinpage;

public class Addressbooktestcases extends TestBase {
	Myaccountpage myaccount;
	Signinpage signinpage;
	Addressbookpage addressbook;
	 public Addressbooktestcases() {
		 super();
	}
	 @BeforeMethod
		public void setup() throws InterruptedException {
			initialization();
			myaccount=new 	Myaccountpage();
			signinpage=new Signinpage();
			addressbook=new Addressbookpage();
			signinpage.Signin1(prop.getProperty("Emailid"), prop.getProperty("password"));
			
		}
	 
	 @Test(priority=1)
		public void addaddressstest() throws InterruptedException {
		 addressbook.addaddresstest();
		
		}
	 @Test(priority=2)
		public void canceladdresstest() throws InterruptedException {
		 addressbook.canceladdresstest();
		
		}
	 @Test(priority=3)
		public void deleteaddresstest() throws InterruptedException {
		 addressbook.deleteaddresstest();
		
		}
	 @Test(priority=4)
		public void deletecanceladdresstest() throws InterruptedException {
		 addressbook.deletecanceladdresstest();
		
		}
	 @Test(priority=5)
		public void editaddresstest() throws InterruptedException {
		 addressbook.editaddresstest();
		
		}
		@AfterMethod
		public void teardown() {
			driver.quit();
		}
}
