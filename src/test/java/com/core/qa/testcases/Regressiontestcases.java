package com.core.qa.testcases;

import java.awt.AWTException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.beust.jcommander.Parameter;
import com.core.qa.base.TestBase;
import com.core.qa.pages.Addressbookpage;
import com.core.qa.pages.Createanaccountpage;
import com.core.qa.pages.GuestAddtocartBlank;
import com.core.qa.pages.Homepage;
import com.core.qa.pages.Myaccountpage;
import com.core.qa.pages.Myorderspage;
import com.core.qa.pages.Myquoterequestpage;
import com.core.qa.pages.ProductListingpage;
import com.core.qa.pages.Productdetailpage;
import com.core.qa.pages.Savedcardspage;
import com.core.qa.pages.Signinpage;
import com.core.qa.pages.flatPCTQuote;
import com.core.qa.pages.flatembquickQuoteSubmission;
import com.core.qa.pages.flatpctorder;
import com.core.qa.pages.raq;
import com.core.qa.pages.threedpctquote;
import com.core.qa.util.TestUtil;
import com.paulhammant.ngwebdriver.NgWebDriver;

public class Regressiontestcases extends TestBase{
	
	Homepage homepage;
	Signinpage signinpage;
	Createanaccountpage createanaccount;
	ProductListingpage productlistingpage;
	GuestAddtocartBlank productdetailpage;
	Productdetailpage productdetailpages;
	flatembquickQuoteSubmission quotesubmision;
	threedpctquote threepct;
	flatPCTQuote pctquote;
	flatpctorder flatpct;
	raq Raq;
	Myaccountpage myaccount;
	Myorderspage myorders;
	Myquoterequestpage myquoterequest;
	Addressbookpage addressbook;

	Savedcardspage savedcards;

	
	public Regressiontestcases() {
		 super();
	}
	

	@BeforeMethod
	
	public void setup() {
		initialization();
		homepage=new Homepage();
		
	}

	
	@Test(priority=1)
	public void Searchdropdown() throws InterruptedException {
		String sitename=prop.getProperty("url");
		String title=driver.getTitle();
		
		if(title.contains("Blankwear")) {
			String productname =homepage.Searchdropdowntest(prop.getProperty("blankwearstyleno"));
			String productnamedetailpage=driver.findElement(By.xpath("//div//h2[@class='style-number fw-500 mr-3 font-sm']")).getText();
			String testString = productnamedetailpage;
			  int startIndex = testString.indexOf(":");
			//  int endIndex = testString.indexOf(":");
			  String subString = testString.substring(startIndex+1);
			  System.out.println(subString);
			
			System.out.println(productnamedetailpage);
			Assert.assertTrue(subString.contains(productname));
		}
		else if(title.contains("Blank Hats | Wholesale Hats | Custom Caps | Otto Caps | Embroidered Hats")) {
			String productname =homepage.Searchdropdowntest(prop.getProperty("styleno"));
			String productnamedetailpage=driver.findElement(By.xpath("//div//h2[@class='style-number fw-500 mr-3 font-sm']")).getText();
			String testString = productnamedetailpage;
			  int startIndex = testString.indexOf(":");
			//  int endIndex = testString.indexOf(":");
			  String subString = testString.substring(startIndex+1);
			  System.out.println(subString);
			
			System.out.println(productnamedetailpage);
			Assert.assertTrue(subString.contains(productname));
		}
		else if(title.contains("Wholesale Clothing | Wholesale Tshirts | Bulk Tshirts | Polo Shirts")) {
			String productname =homepage.Searchdropdowntest(prop.getProperty("blankwearstyleno"));
			String productnamedetailpage=driver.findElement(By.xpath("//div//h2[@class='style-number fw-500 mr-3 font-sm']")).getText();
			String testString = productnamedetailpage;
			  int startIndex = testString.indexOf(":");
			//  int endIndex = testString.indexOf(":");
			  String subString = testString.substring(startIndex+1);
			  System.out.println(subString);
			
			System.out.println(productnamedetailpage);
			Assert.assertTrue(subString.contains(productname));
		}
		else if(title.contains("Wholesale Apparel | Men's Apparel | Women's Apparel | T-Shirts | Polos</title>")) {
			
			String productname =homepage.Searchdropdowntest(prop.getProperty("youapparelstyleno"));
			String productnamedetailpage=driver.findElement(By.xpath("//div//h2[@class='style-number fw-500 mr-3 font-sm']")).getText();
			String testString = productnamedetailpage;
			  int startIndex = testString.indexOf(":");
			//  int endIndex = testString.indexOf(":");
			  String subString = testString.substring(startIndex+1);
			  System.out.println(subString);
			
			System.out.println(productnamedetailpage);
			Assert.assertTrue(subString.contains(productname));
			
		}
	}

	@Test(priority=2)
	public void Banner1linktest() throws InterruptedException {
		
		boolean actualurl=homepage.Bannerlinkchecking1();
		Assert.assertTrue(actualurl);	
	}

	@Test(priority=3)
	public void Megamenupresent() throws InterruptedException {
		boolean flag=homepage.megamenuchecking();
		Assert.assertTrue(flag);
	}
	
	@Test(priority=4)
	public void megamenulink() throws InterruptedException {
		boolean flag=homepage.menuslink();
		Assert.assertTrue(flag);
	}
	
	@Test(priority=5)
	public void productsearchfilterlinktest() throws InterruptedException {
		boolean currenturl=homepage.productsearchfiltercheck();
		Assert.assertTrue(currenturl);		
		}

	@Test(priority=6)
	public void onsalelinktest() throws InterruptedException {
		boolean currenturl=homepage.onsalecheck();
		Assert.assertTrue(currenturl);			
	}
	@Test(priority=7)
	public void widgettest1() throws InterruptedException {
			String text=homepage.widgetcheck1();
			String listingpagetext=driver.findElements(By.xpath("//span[@class='breadcrumb-title active']//strong")).get(1).getText();
			System.out.println(text);
			System.out.println(listingpagetext);
			Assert.assertTrue(listingpagetext.equalsIgnoreCase(text));
	}

	@Test(priority=8)
	public void homeproductstest() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		Thread.sleep(5000);
		boolean flag=homepage.homepageproductschecking();
		Assert.assertTrue(flag);
	}
	
	@Test(priority=9)
	public void validunameandpassw() throws InterruptedException {
		signinpage=new Signinpage();
		boolean flag=signinpage.Signin1(prop.getProperty("Emailid"),(prop.getProperty("password")));
		Assert.assertTrue(flag);
	}

	
	@Test(priority=10)
	public void validdetails(String firstname,String lastname,String email,String password,String confirmpassword) throws InterruptedException {
		createanaccount=new Createanaccountpage();
		boolean flag=createanaccount.createaccount1(firstname,lastname,randomEmail(),password,confirmpassword);
		Assert.assertTrue(flag);

	}
	@Test(priority=11)
	public void Randomproductclicktest() throws InterruptedException {
		productlistingpage=new 	ProductListingpage();
		String[] text=productlistingpage.randomproductclick();
		String styleno=text[0];
		String price=text[1];
		String style=driver.findElement(By.xpath("//div[@class='review-wrapper']//h2")).getText();
		  System.out.println(style);

		String pricedetailpage=driver.findElement(By.xpath("//div[@class='product-save_banner ng-star-inserted']//p")).getText();
		  System.out.println(pricedetailpage);

		Assert.assertTrue(style.contains(styleno));
		Assert.assertEquals(price, pricedetailpage);
		
	}
	@Test(priority=12)
	public void Selectedcolourtest() throws InterruptedException {
		productlistingpage=new 	ProductListingpage();
		productdetailpage=new GuestAddtocartBlank();
		productdetailpages=new Productdetailpage();
		productlistingpage.randomproductclicktest2();
		String colour[]=productdetailpages.Selectedcolourtest();
		String selectedcolur=colour[0];
		String selectedcolur1=colour[1];
		Assert.assertTrue(selectedcolur.contains(selectedcolur1));
		
		
	}
	@Test(priority=13)
	public void Changecolour() throws InterruptedException {
		productlistingpage=new 	ProductListingpage();
		productdetailpage=new GuestAddtocartBlank();
		productdetailpages=new Productdetailpage();
		productlistingpage.randomproductclicktest2();
		String colour[]=productdetailpages.Changecolourtest();
		String selectedcolur=colour[0];
		String selectedcolur1=colour[1];
		System.out.println(selectedcolur);
		System.out.println(selectedcolur1);
		Assert.assertTrue(selectedcolur.contains(selectedcolur1));
		
		
	}
	@Test(priority=14)
	public void addanothercolur() throws InterruptedException {
		productlistingpage=new 	ProductListingpage();
		productdetailpage=new GuestAddtocartBlank();
		productdetailpages=new Productdetailpage();
		productlistingpage.randomproductclicktest2();
		String colour[]=productdetailpages.addanothercolurtest();
		String selectedcolur=colour[0];
		String selectedcolur1=colour[1];
		System.out.println(selectedcolur);
		System.out.println(selectedcolur1);
		Assert.assertTrue(selectedcolur.contains(selectedcolur1));
		
		
	}
	@Test(priority=15)
	public void Addtocart() throws InterruptedException {
		
		productlistingpage=new 	ProductListingpage();
		productdetailpage=new GuestAddtocartBlank();
		productdetailpages=new Productdetailpage();
		productlistingpage.randomproductclicktest2();
		String qty[]=productdetailpages.SingleProductdiffcoluraddtocart();
		String qtydetailpage=qty[0];
		String addtocartpopup=qty[1];
		
		Assert.assertEquals(qtydetailpage, addtocartpopup);
		
	}
	 @Test(priority=16)
		public void qtytest() throws InterruptedException {
		 	productlistingpage=new 	ProductListingpage();
			productdetailpage=new GuestAddtocartBlank();
			productdetailpages=new Productdetailpage();
			productlistingpage.randomproductclicktest2();
			productdetailpage.AddtocartQtytest();
			int qty[]=productdetailpage.qtytest();
			int qty1=qty[0];
			int qty2=qty[1];	
			Assert.assertEquals(qty1, qty2);
			
		}
	 @Test(priority=17)
		public void pricetest1() throws InterruptedException {
		 	productlistingpage=new 	ProductListingpage();
			productdetailpage=new GuestAddtocartBlank();
			productdetailpages=new Productdetailpage();
			productlistingpage.randomproductclicktest2();
			productdetailpage.AddtocartQtytest();
			float[] qty=productdetailpage.pricetest1();
			float qty1=qty[0];
			float qty2=qty[1];
			Assert.assertEquals(qty1, qty2);
			
		}

	 
	 @Test(priority=18)
		public void clearcart() throws InterruptedException {
		 	productlistingpage=new 	ProductListingpage();
			productdetailpage=new GuestAddtocartBlank();
			productdetailpages=new Productdetailpage();
			productlistingpage.randomproductclicktest2();
			productdetailpage.AddtocartQtytest();
			boolean size=productdetailpage.clearcarttest();			
			Assert.assertTrue(size);
			
		}
 	@Test(priority=19)
	public void cartupdationqtytest() throws InterruptedException {
 		productlistingpage=new 	ProductListingpage();
		productdetailpage=new GuestAddtocartBlank();
		productdetailpages=new Productdetailpage();
		productlistingpage.randomproductclicktest2();
		productdetailpage.AddtocartQtytest();
		int qty[]=productdetailpage.cartupdation();
		int qty1=qty[0];
		int qty2=qty[1];
		
		Assert.assertEquals(qty1, qty2);
		
	}
 	
	@Test(priority=20)
	public void cartupdationpricetest() throws InterruptedException {
		productlistingpage=new 	ProductListingpage();
		productdetailpage=new GuestAddtocartBlank();
		productdetailpages=new Productdetailpage();
		productlistingpage.randomproductclicktest2();
		productdetailpage.AddtocartQtytest();
		float[] qty=productdetailpage.cartupdationpricetest1();
		float qty1=qty[0];
		float qty2=qty[1];
		
		Assert.assertEquals(qty1, qty2);
		
}
	
	@Test(priority=20)
	public void freeshippingtest() throws InterruptedException {
		productlistingpage=new 	ProductListingpage();
		productdetailpage=new GuestAddtocartBlank();
		productdetailpages=new Productdetailpage();
		productlistingpage.randomproductclicktest2();
		productdetailpage.AddtocartQtytest();
		productdetailpage.cartupdationpricetest1();
		String title=driver.getTitle();

		if(title.contains("Wholesale Clothing | Wholesale Tshirts | Bulk Tshirts | Polo Shirts")) {
			boolean flag=productdetailpage.freeshippingtest();
			Assert.assertTrue(flag);
		}
		else if(title.contains("Wholesale Apparel | Men's Apparel | Women's Apparel | T-Shirts | Polos")) {
			boolean flag=productdetailpage.freeshippingtest();
			Assert.assertTrue(flag);
		}
		else if(title.contains("Wholesale T-Shirts | Bulk T-Shirts | Blankwear.com")) {
			boolean flag=productdetailpage.freeshippingtest();
			Assert.assertTrue(flag);
		}
		
		
}
	
	@Test(priority=20)
	public void freeshippingtest2() throws InterruptedException {
		productlistingpage=new 	ProductListingpage();
		productdetailpage=new GuestAddtocartBlank();
		productdetailpages=new Productdetailpage();
		productlistingpage.randomproductclicktest2();
		productdetailpage.AddtocartQtytest();
		String title=driver.getTitle();

		if(title.contains("Wholesale Clothing | Wholesale Tshirts | Bulk Tshirts | Polo Shirts")) {
			boolean flag=productdetailpage.freeshippingtest();
			Assert.assertTrue(flag);
		}
		else if(title.contains("Wholesale Apparel | Men's Apparel | Women's Apparel | T-Shirts | Polos")) {
			boolean flag=productdetailpage.freeshippingtest();
			Assert.assertTrue(flag);
		}
		else if(title.contains("Wholesale T-Shirts | Bulk T-Shirts | Blankwear.com")) {
			boolean flag=productdetailpage.freeshippingtest();
			Assert.assertTrue(flag);
		}
		
		
}
	 @Test(priority=21)
		public void removeicontest() throws InterruptedException {
		 	productlistingpage=new 	ProductListingpage();
			productdetailpage=new GuestAddtocartBlank();
			productdetailpages=new Productdetailpage();
			productlistingpage.randomproductclicktest2();
			productdetailpage.addanothercolurtest();
			productdetailpage.Removeiconclick();
			
			
		}
		@Test(priority=20)
		public void cartupdationpricetestsigninuser() throws InterruptedException {
			signinpage=new Signinpage();
			signinpage.Signin1(prop.getProperty("Emailid"), prop.getProperty("password"));
			productlistingpage=new 	ProductListingpage();
			productdetailpage=new GuestAddtocartBlank();
			productdetailpages=new Productdetailpage();
			productlistingpage.randomproductclicktest2();
			productdetailpage.AddtocartQtytest();
			
			float[] qty=productdetailpage.cartupdationpricetest1();
			float qty1=qty[0];
			float qty2=qty[1];
			
			Assert.assertEquals(qty1, qty2);
			
	}
		@Test(priority=22)
		public void AboutuspageTest() throws InterruptedException {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				boolean flag=homepage.aboutuscheck();
				Assert.assertTrue(flag);
			
		}
		@Test(priority=23)
		public void ContactuspageTest() throws InterruptedException {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				String text=homepage.contactuscheck();
				Assert.assertEquals(text, prop.getProperty("url")+"contact");
			
		}
		@Test(priority=24)
		public void PrivacypolicypageTest() throws InterruptedException {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				boolean text=homepage.privacypolicycheck();
				Assert.assertTrue(text);
			
		}
		@Test(priority=25)
		public void Termsandconditionstest() throws InterruptedException {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				String text=homepage.termsandconditioncheck();
				Assert.assertEquals(text, prop.getProperty("url")+"terms");
			
		}
		@Test(priority=26)
		public void Orderstatustest() throws InterruptedException {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				String text=homepage.orderstatuscheck();
				Assert.assertEquals(text, prop.getProperty("url")+"order-status");
			
		}
		@Test(priority=27)
		public void FAQtest() throws InterruptedException {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				String text=homepage.FAQcheck();
				Assert.assertEquals(text, prop.getProperty("url")+"faq");
			
		}
		
		
		
		@AfterMethod
		public void teardown() {
			driver.quit();
		}
}
