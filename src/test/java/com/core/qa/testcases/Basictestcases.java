package com.core.qa.testcases;

import java.awt.AWTException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.GuestAddtocartBlank;
import com.core.qa.pages.Homepage;
import com.core.qa.pages.ProductListingpage;
import com.core.qa.pages.Signinpage;
import com.core.qa.pages.flatPCTQuote;
import com.core.qa.pages.flatembquickQuoteSubmission;
import com.core.qa.pages.flatpctorder;
import com.core.qa.util.TestUtil;
import com.paulhammant.ngwebdriver.NgWebDriver;

public class Basictestcases extends TestBase {
	

	Homepage homepage;
	ProductListingpage productlistingpage;
	GuestAddtocartBlank productdetailpage;
	Signinpage signinpage;
	flatPCTQuote pctquote;
	flatpctorder flatpct;
	flatembquickQuoteSubmission flatembquickQuoteSubmission;
	String sheetName="Shipping Address";

	
	public Basictestcases() {
		 super();
	}
	
	@BeforeMethod
	public void setup() throws InterruptedException {
		initialization();
		homepage=new Homepage();
		
		
	}
	@Test(priority=1)
	public void homeproductstest() throws InterruptedException {
		boolean flag=homepage.homepageproductschecking();
		Assert.assertTrue(flag);
	}
	
	@Test(priority=2)
	public void megamenulink() throws InterruptedException {
		boolean flag=homepage.menuslink();
		Assert.assertTrue(flag);
	}
	
	@Test(priority=3)
	public void Banner1linktest() throws InterruptedException {
		
		boolean actualurl=homepage.Bannerlinkchecking1();
		Assert.assertTrue(actualurl);	
	}
	
	@Test(priority=4)
	public void Signintest() throws InterruptedException {
		boolean flag=homepage.Signinlinktest();
		Assert.assertTrue(flag);
	}

	@Test(priority=5)
	public void Searchdropdown() throws InterruptedException {
		String productname =homepage.Searchdropdowntest(prop.getProperty("capbargainstyleno"));
		String productnamedetailpage=driver.findElement(By.xpath("//div//strong[@class='style-number fw-500 mr-3 font-sm ng-star-inserted']")).getText();
		String testString = productnamedetailpage;
		  int startIndex = testString.indexOf(":");
		//  int endIndex = testString.indexOf(":");
		  String subString = testString.substring(startIndex+1);
		  System.out.println(subString);
		
		System.out.println(productnamedetailpage);
		Assert.assertTrue(subString.contains(productname));
	}
	
	@Test(priority=6)
	public void Searchbutton() throws InterruptedException {
		String productname =homepage.Searchbuttontest((prop.getProperty("capbargainstyleno")));
		Assert.assertTrue(productname.contains((prop.getProperty("capbargainstyleno"))));
	}
	
	//*****************Test case:Checking the Addtocart pop up that Qty added in the Product details page *******************
	 
	 @Test(priority=7)
		public void Addtocart() throws InterruptedException {
		 	signinpage=new Signinpage();
			productlistingpage=new 	ProductListingpage();
			productdetailpage=new GuestAddtocartBlank();
			//productlistingpage.randomproductclick();
			homepage.Searchbuttontest((prop.getProperty("capbargainstyleno")));
			String qty[]=productdetailpage.AddtocartQtytest();
			String qtydetailpage=qty[0];
			String addtocartpopup=qty[1];	
			Assert.assertEquals(qtydetailpage, addtocartpopup);		
			}
	 
	 @Test(priority=8)
		public void qtytest() throws InterruptedException {
		 signinpage=new Signinpage();
			productlistingpage=new 	ProductListingpage();
			productdetailpage=new GuestAddtocartBlank();
			//productlistingpage.randomproductclick();
			homepage.Searchbuttontest((prop.getProperty("capbargainstyleno")));
			productdetailpage.AddtocartQtytest();
			int qty[]=productdetailpage.qtytest();
			int qty1=qty[0];
			int qty2=qty[1];	
			Assert.assertEquals(qty1, qty2);
			
		}
	 
	 @Test(priority=9)
		public void pricetest1() throws InterruptedException {
		 signinpage=new Signinpage();
			productlistingpage=new 	ProductListingpage();
			productdetailpage=new GuestAddtocartBlank();
			//productlistingpage.randomproductclick();
			homepage.Searchbuttontest((prop.getProperty("capbargainstyleno")));
			productdetailpage.AddtocartQtytest();
			float[] qty=productdetailpage.pricetest1();
			float qty1=qty[0];
			float qty2=qty[1];
			
			System.out.println(qty1);
			System.out.println(qty2);

			Assert.assertEquals(qty1, qty2);
			
		}
	 @Test(priority=10)
		public void clearcart() throws InterruptedException {
		 signinpage=new Signinpage();
			productlistingpage=new 	ProductListingpage();
			productdetailpage=new GuestAddtocartBlank();
			//productlistingpage.randomproductclick();
			homepage.Searchbuttontest((prop.getProperty("capbargainstyleno")));
		 productdetailpage.AddtocartQtytest();
			boolean size=productdetailpage.clearcarttest();
			Assert.assertTrue(size);
			
		}
		@Test(priority=11)
		public void cartupdationpricetest() throws InterruptedException {
			signinpage=new Signinpage();
			productlistingpage=new 	ProductListingpage();
			productdetailpage=new GuestAddtocartBlank();
			//productlistingpage.randomproductclick();
			homepage.Searchbuttontest((prop.getProperty("capbargainstyleno")));
			productdetailpage.AddtocartQtytest();

			//productdetailpage.addanothercolurtest();
			float[] qty=productdetailpage.cartupdationpricetest1();
			float qty1=qty[0];
			float qty2=qty[1];
			
			Assert.assertEquals(qty1, qty2);
			
	}
		
	 
		 @Test(priority=12)
		 public void GuestuserfullcheckoutTest2() throws InterruptedException, AWTException {
			    signinpage=new Signinpage();
				productlistingpage=new 	ProductListingpage();
				productdetailpage=new GuestAddtocartBlank();
				//productlistingpage.randomproductclick(); 
				homepage.Searchbuttontest((prop.getProperty("capbargainstyleno")));
				productdetailpage.AddtocartQtytest();
			// productdetailpage.addanothercolurtest();
			 JavascriptExecutor jsDriver;
			 NgWebDriver ngWebDriver;
			 jsDriver = (JavascriptExecutor) driver;
			 ngWebDriver = new NgWebDriver(jsDriver);
			 try {
				 ngWebDriver.waitForAngularRequestsToFinish();

			 }
			 catch (Exception e) {

			}
			 Thread.sleep(5000);
			 productdetailpage.checkoutclick();
			 productdetailpage.Shippingaddress(prop.getProperty("fname"),prop.getProperty("email"),prop.getProperty("companyname"),prop.getProperty("address"),prop.getProperty("city"),prop.getProperty("state"),prop.getProperty("zipcode"),prop.getProperty("phonenumber"));
			 productdetailpage.paymenttest2();
			 productdetailpage.Billingaddresstest();
			 int[]qty= productdetailpage.qtytestcheckout();
			 int qty1=qty[0];
			 int qty2=qty[1];
			 Assert.assertEquals(qty1, qty2); 
			 float[] price=	productdetailpage.checkoutprice();
			 float price1=price[0];
			 float price2=price[1];
			 Assert.assertEquals(price1, price2);
			Thread.sleep(5000);
			 productdetailpage.Deliverymethodtest();
			 Thread.sleep(5000);
			//productdetailpage.placeorderbuttonclick();
			 driver.findElement(By.xpath("//div[@class='credit-select mt-2']//mat-radio-button[2]")).click();
			
			productdetailpage.Deliverymethodtest();
		    driver.findElement(By.xpath("//div[@class='credit-select mt-2']//mat-radio-button[1]")).click();
	
	
		}
		
		 @Test(priority=13)
			public void clearcartsignin() throws InterruptedException {
			 signinpage=new Signinpage();
				productlistingpage=new 	ProductListingpage();
				productdetailpage=new GuestAddtocartBlank();
				//productlistingpage.randomproductclick();
				homepage.Searchbuttontest((prop.getProperty("capbargainstyleno")));
			 signinpage.Signin1(prop.getProperty("Emailid"), prop.getProperty("password"));
			 Thread.sleep(5000);
			 productdetailpage.AddtocartQtytest();
			 JavascriptExecutor jsDriver;
			 NgWebDriver ngWebDriver;
			 jsDriver = (JavascriptExecutor) driver;
			 ngWebDriver = new NgWebDriver(jsDriver);
			 try {
				 ngWebDriver.waitForAngularRequestsToFinish();
	
			 }
			 catch (Exception e) {
		
			}
			boolean size=productdetailpage.clearcarttest();
			Assert.assertTrue(size);
			
			}
		@Test(priority=14)
		public void RegistereduserfullcheckoutTest2() throws InterruptedException, AWTException {
			signinpage=new Signinpage();
			productlistingpage=new 	ProductListingpage();
			productdetailpage=new GuestAddtocartBlank();
			//productlistingpage.randomproductclick();
			homepage.Searchbuttontest((prop.getProperty("capbargainstyleno")));
			productdetailpage.AddtocartQtytest();
			 Thread.sleep(5000);
			 signinpage.Signin1(prop.getProperty("Emailid"), prop.getProperty("password"));
			 Thread.sleep(2000);
			 JavascriptExecutor jsDriver;
			 NgWebDriver ngWebDriver;
			 jsDriver = (JavascriptExecutor) driver;
			 ngWebDriver = new NgWebDriver(jsDriver);
			 try {
				 ngWebDriver.waitForAngularRequestsToFinish();

			 }
			 catch (Exception e) {

			}
			 productdetailpage.checkoutbuttonclick();
			 productdetailpage.newshippingaddressadd(prop.getProperty("fname"), prop.getProperty("email"), prop.getProperty("companyname"), prop.getProperty("address"), prop.getProperty("city"), prop.getProperty("state"), prop.getProperty("zipcode"),prop.getProperty("phonenumber"));
			// Thread.sleep(7000);
			 int[]qty= productdetailpage.qtytestcheckout();
			 int qty1=qty[0];
			 int qty2=qty[1];
			 Assert.assertEquals(qty1, qty2); 
			 float[] price=	productdetailpage.checkoutprice();
			 float price1=price[0];
			 float price2=price[1];
			 Assert.assertEquals(price1, price2);
			 productdetailpage.Deliverymethodtest();
			// productdetailpage.placeorderbuttonclick();
		
			 driver.findElement(By.xpath("//div[@class='credit-select mt-2']//mat-radio-button[2]")).click();			
			 productdetailpage.Deliverymethodtest();
	 		 driver.findElement(By.xpath("//div[@class='credit-select mt-2']//mat-radio-button[1]")).click();
	
		}
		 @Test(priority=15)
			public void pctorder1() throws InterruptedException, AWTException {
			 productlistingpage=new 	ProductListingpage();
				productdetailpage=new GuestAddtocartBlank();
				productlistingpage.randomproductclick();
				pctquote=new flatPCTQuote();
				flatpct=new flatpctorder();
			 boolean flag=flatpct.pctordertest1();
			 Assert.assertTrue(flag);
			}
		 
		 @Test(priority=16)
			public void pctquote() throws InterruptedException, AWTException {
			 productlistingpage=new 	ProductListingpage();
				productdetailpage=new GuestAddtocartBlank();
				
				productlistingpage.randomproductclick();
				pctquote=new flatPCTQuote();
				//flatpct=new flatpctorder();
				pctquote.Quickquotetest1();
			}
		 
		 @Test(priority=17)
			public void quickquote() throws InterruptedException, AWTException {
			 productlistingpage=new 	ProductListingpage();
				productdetailpage=new GuestAddtocartBlank();
				//productlistingpage.randomproductclick();
				homepage.Searchbuttontest(prop.getProperty("capbargainstyleno"));
				flatembquickQuoteSubmission=new flatembquickQuoteSubmission();
				
				flatembquickQuoteSubmission.Quickquotetest1();
			}
		 
		 @AfterMethod
			public void teardown() {
				driver.quit();
			}
		
}
