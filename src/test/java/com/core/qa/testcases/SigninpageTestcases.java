
package com.core.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.Signinpage;
import com.core.qa.util.TestUtil;

public class SigninpageTestcases extends TestBase {
	
	Signinpage signinpage;
	
	 public SigninpageTestcases() {
		 super();
	}
	
	@BeforeMethod
	public void setup() {
		initialization();
		signinpage=new Signinpage();
		
	}
	
	@Test(priority=1)
	public void validunameandpassw() throws InterruptedException {
		boolean flag=signinpage.Signin1(prop.getProperty("Emailid"),(prop.getProperty("password")));
		Assert.assertTrue(flag);
	}
	@Test(priority=2)
	public void invalidpass() throws InterruptedException {
		boolean flag=signinpage.Signin2(prop.getProperty("Emailid"),"1234564");
		Assert.assertTrue(flag);
	}
	
	@Test(priority=3)
	public void emptyemailandpassw() throws InterruptedException {
		boolean flag=signinpage.Signin5();
		Assert.assertTrue(flag);
	}
	
	@Test(priority=4)
	public void validforgotpasswordemail() throws InterruptedException {
		
		boolean flag=signinpage.Signin3(prop.getProperty("Emailid"));
	
		Assert.assertTrue(flag);

	}
	@Test(priority=5)
	public void invalidforgotpassemail() throws InterruptedException {
		
		boolean flag=signinpage.Signin4(randomEmail());
		Assert.assertTrue(flag);
		
	}
	
	@Test(priority=6)
	public void forgotpassbacklink() throws InterruptedException {
		
		boolean flag=signinpage.Signin6();
		Assert.assertTrue(flag);

	}
	@Test(priority=7)
	public void createnewaccountlink() throws InterruptedException {

		boolean flag=signinpage.Signin7();
		Assert.assertTrue(flag);
		
	}
	
	
	@AfterMethod
	public void teardown() {
		driver.quit();
	}

}
