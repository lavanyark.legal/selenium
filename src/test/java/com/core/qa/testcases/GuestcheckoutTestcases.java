package com.core.qa.testcases;

import java.awt.AWTException;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.GuestAddtocartBlank;
import com.core.qa.pages.ProductListingpage;
import com.core.qa.pages.Signinpage;
import com.core.qa.util.TestUtil;
import com.paulhammant.ngwebdriver.NgWebDriver;

public class GuestcheckoutTestcases extends TestBase {
	
	ProductListingpage productlistingpage;
	GuestAddtocartBlank productdetailpage;
	Signinpage signinpage;
	
	String sheetName="Shipping Address";
	
	 public GuestcheckoutTestcases() {
		 super();
	}
	 @BeforeMethod
		public void setup() throws InterruptedException {
			initialization();
			signinpage=new Signinpage();
			productlistingpage=new 	ProductListingpage();
			productdetailpage=new GuestAddtocartBlank();
			productlistingpage.randomproductclick();
			
		}
//*****************Test case:Checking the Addtocart pop up that Qty added in the Product details page *******************
	 
	 @Test(priority=9)
		public void Addtocart() throws InterruptedException {
			String qty[]=productdetailpage.AddtocartQtytest();
			String qtydetailpage=qty[0];
			String addtocartpopup=qty[1];	
			Assert.assertEquals(qtydetailpage, addtocartpopup);		
		}
//	//Checking the Qty and subqty in the Shopping cart page	
	 
	 @Test(priority=10)
		public void qtytest() throws InterruptedException {
		 productdetailpage.AddtocartQtytest();
			int qty[]=productdetailpage.qtytest();
			int qty1=qty[0];
			int qty2=qty[1];	
			Assert.assertEquals(qty1, qty2);
			
		}	 
//	//Checking the Inputqty and subqty in shopping cart page
//
	 @Test(priority=11)
		public void qtytest2() throws InterruptedException {
		 productdetailpage.AddtocartQtytest();
			int qty[]=productdetailpage.qtytest2();
			int qty1=qty[0];
			int qty2=qty[1];
			
			Assert.assertEquals(qty1, qty2);
			
		}
	//Checking the Price and Subtotalprice in the Shopping cart page	 

	 @Test(priority=12)
		public void pricetest1() throws InterruptedException {
		 productdetailpage.AddtocartQtytest();
			float[] qty=productdetailpage.pricetest1();
			float qty1=qty[0];
			float qty2=qty[1];
			
			Assert.assertEquals(qty1, qty2);
			
		}
	//Checking the Clearcart functionality 
 
	 @Test(priority=13)
		public void clearcart() throws InterruptedException {
		 productdetailpage.AddtocartQtytest();
			boolean size=productdetailpage.clearcarttest();
			Assert.assertTrue(size);
			
		}

	 //Checking the Qty after updating the Qty of the Shopping cart

	 	@Test(priority=16)
		public void cartupdationqtytest() throws InterruptedException {
		 productdetailpage.addanothercolurtest();
			int qty[]=productdetailpage.cartupdation();
			int qty1=qty[0];
			int qty2=qty[1];
			
			Assert.assertEquals(qty1, qty2);
			
		}
	 //Checking the price after updating the Qty of the Shopping cart
	 	
		@Test(priority=17)
		public void cartupdationpricetest() throws InterruptedException {
			productdetailpage.addanothercolurtest();
			float[] qty=productdetailpage.cartupdationpricetest1();
			float qty1=qty[0];
			float qty2=qty[1];
			
			Assert.assertEquals(qty1, qty2);
			
	}
	//Checking the Remove icon functionality and checking qty updated
		 @Test(priority=18)
			public void removeicontest() throws InterruptedException {
			productdetailpage.addanothercolurtest();
		 	productdetailpage.Removeiconclick();				
			}
	//Checking the checkoutbutton functionality

	 @Test(priority=19)
			public void checkoutbutton() throws InterruptedException {
			 productdetailpage.addanothercolurtest();
			 Thread.sleep(5000);
			 productdetailpage.checkoutclick();
					
			}
		 
		
	
		 
//	//Checking the full Guestcheckoutflow with same shipping address	 
	 @Test(priority=20, dataProvider="getshippingaddressdata")
		public void GuestuserfullcheckoutTest1(String FullName,String Email,String Companyname,String Streetaddress,String city,String state,String zip,String phonenumber) throws InterruptedException, AWTException {
		 productdetailpage.AddtocartQtytest();
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 Thread.sleep(5000);
		 productdetailpage.checkoutclick();
		 productdetailpage.Shippingaddress(FullName, Email, Companyname, Streetaddress, city,state,zip,phonenumber);
		
		 productdetailpage.paymenttest1();
		 int[]qty= productdetailpage.qtytestcheckout();
		 int qty1=qty[0];
		 int qty2=qty[1];
		 Assert.assertEquals(qty1, qty2); 
		 float[] price=	productdetailpage.checkoutprice();
		 float price1=price[0];
		 float price2=price[1];
		 Assert.assertEquals(price1, price2);
		 Thread.sleep(5000);
		 productdetailpage.Deliverymethodtest2();
	
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}	
		 //productdetailpage.placeorderbuttonclick();


		}
////Checking the full Guestcheckoutflow with DIFF shipping address	  
	 @Test(priority=21,dataProvider="getshippingaddressdata")
	 public void GuestuserfullcheckoutTest2(String FullName,String Email,String Companyname,String Streetaddress,String city,String state,String zip,String phonenumber) throws InterruptedException, AWTException {
		 productdetailpage.addanothercolurtest();
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
		 Thread.sleep(5000);
		 productdetailpage.checkoutclick();
		 productdetailpage.Shippingaddress(FullName, Email, Companyname, Streetaddress, city,state,zip,phonenumber);
		 productdetailpage.paymenttest2();
		 productdetailpage.Billingaddresstest();
		 int[]qty= productdetailpage.qtytestcheckout();
		 int qty1=qty[0];
		 int qty2=qty[1];
		 Assert.assertEquals(qty1, qty2); 
		 float[] price=	productdetailpage.checkoutprice();
		 float price1=price[0];
		 float price2=price[1];
		 Assert.assertEquals(price1, price2);
		 Thread.sleep(5000);
		 productdetailpage.Deliverymethodtest();
		 Thread.sleep(5000);
		// productdetailpage.placeorderbuttonclick();
	}
//	 
	 @Test(priority=22, dataProvider="getshippingaddressdata")
		public void GuestuserfullcheckoutTest3(String FullName,String Email,String Companyname,String Streetaddress,String city,String state,String zip,String phonenumber) throws InterruptedException {
		 productdetailpage.AddtocartQtytest(); 
		 
		 Thread.sleep(5000);
		 productdetailpage.checkoutclick();
		 productdetailpage.Shippingaddress(FullName, Email, Companyname, Streetaddress, city,state,zip,phonenumber);
		 productdetailpage.paymenttest1();
		 int[]qty= productdetailpage.qtytestcheckout();
		 int qty1=qty[0];
		 int qty2=qty[1];
		 Assert.assertEquals(qty1, qty2); 
		 float[] price=	productdetailpage.checkoutprice();
		 float price1=price[0];
		 float price2=price[1];
		 Assert.assertEquals(price1, price2);
		 Thread.sleep(5000);
		// productdetailpage.placeorderbuttonclick();
		 	
		}
	
	 @AfterMethod
		public void teardown() {
			driver.quit();
		}
}
