package com.core.qa.testcases;

import java.awt.AWTException;


import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.GuestAddtocartBlank;
import com.core.qa.pages.ProductListingpage;
import com.core.qa.pages.flatembquickQuoteSubmission;
import com.core.qa.util.TestUtil;
import com.core.qa.pages.Signinpage;

public class FlatEmbroidreyQuickquotesubmissionTestcases extends TestBase {
	
	ProductListingpage productlistingpage;
	GuestAddtocartBlank productdetailpage;
	flatembquickQuoteSubmission quotesubmision;
	Signinpage signinpage;

	public FlatEmbroidreyQuickquotesubmissionTestcases() {
		 super();
	}
	 @BeforeMethod
		public void setup() throws InterruptedException {
			initialization();
			
			productlistingpage=new 	ProductListingpage();
			productdetailpage=new GuestAddtocartBlank();
			productlistingpage.randomproductclick();
			quotesubmision=new flatembquickQuoteSubmission();
		}

	
		
//	 @Test(priority=36,dataProvider="gettestData")
//		public void quickquotetest1(String phone,String zip,String comment) throws InterruptedException, AWTException {
//		 boolean flag=quotesubmision.Quickquotetest1(phone, zip, comment);
//		 Assert.assertTrue(flag);
//		}
//	 
//	 @Test(priority=37)
//		public void quickquotetest2() throws InterruptedException, AWTException {
//		 boolean flag=quotesubmision.Quickquotetest2();
//		 Assert.assertTrue(flag);
//		}
//	 
	 @Test(priority=38)
		public void quickquotetest3() throws InterruptedException, AWTException {
		 signinpage=new Signinpage();
		 signinpage.Signin1(prop.getProperty("Emailid"),prop.getProperty("password"));
		 quotesubmision.Quickquotetest3();
		}
	 
	 @Test(priority=39)
		public void quickquotetest4() throws InterruptedException, AWTException {
		 boolean flag=quotesubmision.Quickquotetest4();
		 Assert.assertTrue(flag);
		}
	 
	 @Test(priority=40)
		public void quickquotetest5() throws InterruptedException, AWTException {
		 boolean flag=quotesubmision.Quickquotetest5();
		 Assert.assertTrue(flag);
		}
	 
		@AfterMethod
		public void teardown() {
			driver.quit();
		}
}


