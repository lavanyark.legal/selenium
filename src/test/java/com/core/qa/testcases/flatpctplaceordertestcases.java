package com.core.qa.testcases;

import java.awt.AWTException;


import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.GuestAddtocartBlank;
import com.core.qa.pages.ProductListingpage;
import com.core.qa.pages.Signinpage;
import com.core.qa.pages.flatPCTQuote;
import com.core.qa.pages.flatpctorder;
import com.core.qa.pages.pctplaceorder;
import com.core.qa.util.TestUtil;

public class flatpctplaceordertestcases extends TestBase{
	ProductListingpage productlistingpage;
	GuestAddtocartBlank productdetailpage;
	flatPCTQuote pctquote;
	flatpctorder flatpct;
	Signinpage signinpage;
	String sheetName="Shipping Address";
	 @BeforeMethod
		public void setup() throws InterruptedException {
			initialization();
			
			productlistingpage=new 	ProductListingpage();
			productdetailpage=new GuestAddtocartBlank();
			productlistingpage.randomproductclick();
			pctquote=new flatPCTQuote();
			flatpct=new flatpctorder();
		}
	 

	
	
	 
	 @Test(priority=44)
		public void pctorder1() throws InterruptedException, AWTException {
		 boolean flag=flatpct.pctordertest1();
		 Assert.assertTrue(flag);
		}

	 @Test(priority=45,dataProvider="getshippingaddressdata")
		public void pctorder2(String fname,String email,String companyname,String address,String city,String state,String zipcode,String phonenumber) throws InterruptedException, AWTException {
		 boolean flag=flatpct.pctordertest2(fname, email, companyname, address, city, state, zipcode, phonenumber);
		 Assert.assertTrue(flag);
		}
	 @Test(priority=46,dataProvider="getshippingaddressdata")
		public void pctorder3(String fname,String email,String companyname,String address,String city,String state,String zipcode,String phonenumber) throws InterruptedException, AWTException {
		
		 signinpage=new Signinpage();
		 signinpage.Signin1(prop.getProperty("Emailid"),prop.getProperty("password"));
		 boolean flag=flatpct.pctordertest3();
		 Assert.assertTrue(flag);
		}
	 @Test(priority=47,dataProvider="getshippingaddressdata")
		public void pctorder4(String fname,String email,String companyname,String address,String city,String state,String zipcode,String phonenumber) throws InterruptedException, AWTException {
		 boolean flag=flatpct.Quickquotetest5(fname, email, companyname, address, city, state, zipcode, phonenumber);
		 Assert.assertTrue(flag);
		}
	 @Test(priority=48)
		public void pctorder5() throws InterruptedException, AWTException {
		 boolean flag=flatpct.Quickquotetest6();
		 Assert.assertTrue(flag);
		}
	 
		@AfterMethod
		public void teardown() {
			driver.quit();
		}

}
