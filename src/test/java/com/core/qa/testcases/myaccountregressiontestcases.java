package com.core.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.Addressbookpage;
import com.core.qa.pages.Myaccountpage;
import com.core.qa.pages.Myorderspage;
import com.core.qa.pages.Myquoterequestpage;
import com.core.qa.pages.Savedcardspage;
import com.core.qa.pages.Signinpage;

public class myaccountregressiontestcases extends TestBase {
	
	Signinpage signinpage;
	Myaccountpage myaccount;
	Myorderspage myorders;
	Myquoterequestpage myquoterequest;
	Addressbookpage addressbook;

	Savedcardspage savedcards;
	 public myaccountregressiontestcases() {
		 super();
	}
	 
	 @BeforeMethod
		public void setup() throws InterruptedException {
			initialization();
			myaccount=new 	Myaccountpage();
			signinpage=new Signinpage();
			signinpage.Signin1(prop.getProperty("Emailid"), prop.getProperty("password"));
			
		}
	 
	 @Test(priority=28)
		public void accountdashboardtesttable() throws InterruptedException {
		
		 	boolean flag =myaccount.accountdashboardtest();
		 	Assert.assertTrue(flag);
	 }

	 @Test(priority=29)
		public void myorderscustomsearchtest() throws InterruptedException {
			myorders=new Myorderspage();
			
		 myorders.Myorderscustomtablesearchtest();
		 	
			
		}
	 
	 @Test(priority=30)
		public void Myquoterequestorderdetailstest() throws InterruptedException {
		 myquoterequest=new Myquoterequestpage();
		 myquoterequest.Myquoterequestorderdetailstest();
		 	
			
			
		}
	 @Test(priority=31)
		public void mysavedcardstest2() throws InterruptedException {
	
			savedcards=new Savedcardspage();
			
		 savedcards.mysavedcards2();
		
		}
	 
	 @Test(priority=32)
		public void addaddressstest() throws InterruptedException {
		
			addressbook=new Addressbookpage();
			
		 addressbook.addaddresstest();
		
		}
	 @Test(priority=33)
		public void accountinfortest() throws InterruptedException {
		 
		 	boolean flag= myaccount.accountinfotest();
		 	Assert.assertTrue(flag);
		
		}
	 @Test(priority=34)
		public void faq() throws InterruptedException {
		
		 myaccount.faqtest();
		}
	 
	 @Test(priority=35)
		public void logout() throws InterruptedException {
		
		 myaccount.logouttest();
		}
	 
	 @AfterMethod
		public void teardown() {
			driver.quit();
		}

}
