package com.core.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.Myaccountpage;
import com.core.qa.pages.Myquoterequestpage;
import com.core.qa.pages.Signinpage;

public class Myquoterequestpagetestcases extends TestBase {
	Myaccountpage myaccount;
	Signinpage signinpage;
	Myquoterequestpage myquoterequest;
	 public Myquoterequestpagetestcases() {
		 super();
	}
	 @BeforeMethod
		public void setup() throws InterruptedException {
			initialization();
			myquoterequest=new Myquoterequestpage();
			signinpage=new Signinpage();
			signinpage.Signin1(prop.getProperty("Emailid"), prop.getProperty("password"));
			
		}
	
//	 @Test(priority=1)
//		public void myquoterequesttest() throws InterruptedException {
//		 	boolean flag =myquoterequest.myquoterequesttest();
//		 	Assert.assertTrue(flag);
//			
//			
//		}
//	 @Test(priority=2)
//		public void Myquoterequestsearchtest() throws InterruptedException {
//		 myquoterequest.Myquoterequesttablesearchtest();
//		 
//			
//			
//		}
//	 @Test(priority=3)
//		public void Myquoterequeststatustest() throws InterruptedException {
//		 myquoterequest.Myquoterequeststatustest();
//		 	
//			
//			
//		}
//	 @Test(priority=4)
//		public void Myquoterequestorderdetailstest() throws InterruptedException {
//		 myquoterequest.Myquoterequestorderdetailstest();
//		 	
//			
//			
//		}
	 
//	 @Test(priority=5)
//		public void Myquoterequestpaginationstest() throws InterruptedException {
//		 myquoterequest.Myquoterequestpaginationtest();
//				
//		}
	 @Test(priority=5)
		public void Myquoterequestapprovaltest() throws InterruptedException {
		 myquoterequest.approvependingquote();
				
		}
	 

		@AfterMethod
		public void teardown() {
			driver.quit();
		}
	
}
