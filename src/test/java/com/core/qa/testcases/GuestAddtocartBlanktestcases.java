package com.core.qa.testcases;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.ProductListingpage;
import com.core.qa.pages.Productdetailpage;
import com.core.qa.pages.Signinpage;
import com.core.qa.pages.GuestAddtocartBlank;

public class GuestAddtocartBlanktestcases extends TestBase {
	
	ProductListingpage productlistingpage;
	GuestAddtocartBlank productdetailpage;
	Productdetailpage productdetailpages;
	Signinpage signinpage;
	 public GuestAddtocartBlanktestcases() {
		 super();
	}
	
	 @BeforeMethod
		public void setup() {
			initialization();
			productlistingpage=new 	ProductListingpage();
			productdetailpage=new GuestAddtocartBlank();
			productdetailpages=new Productdetailpage();
			productlistingpage.randomproductclicktest2();
			signinpage=new Signinpage();
		}
//	 
//		@Test(priority=1)
//		public void Viewfulldescriptiontest() throws InterruptedException {
//			productdetailpages.Viewfulldescriptionlinktest();
//			
//			
//		}
//		
//		@Test(priority=2)
//		public void Wishlisttest() throws InterruptedException {
//			productdetailpages.Wishlisttest();
//			
//			
//		}
//		
//		@Test(priority=3)
//		public void Wishlisttest2() throws InterruptedException {
//			productdetailpages.Wishlisttest2();
//			
//		}
//		
//		@Test(priority=4)
//		public void sharetest() throws InterruptedException {
//			productdetailpages.sharetest();
//			
//		}
//		
//
//		@Test(priority=5)
//		public void pricecomparelowaslowasandtierprice() throws InterruptedException {
//			String price[]=productdetailpages.lowaspricetest();
//			String aslowasprice=price[0];
//			String tierprice=price[1];
//			Assert.assertTrue(tierprice.contains(aslowasprice));
//			
//			
//		}
//		@Test(priority=6)
//		public void Selectedcolourtest() throws InterruptedException {
//			String colour[]=productdetailpages.Selectedcolourtest();
//			String selectedcolur=colour[0];
//			String selectedcolur1=colour[1];
//			Assert.assertTrue(selectedcolur.contains(selectedcolur1));
//			
//			
//		}
//		@Test(priority=7)
//		public void Changecolour() throws InterruptedException {
//			String colour[]=productdetailpages.Changecolourtest();
//			String selectedcolur=colour[0];
//			String selectedcolur1=colour[1];
//			System.out.println(selectedcolur);
//			System.out.println(selectedcolur1);
//			Assert.assertTrue(selectedcolur.contains(selectedcolur1));
//			
//			
//		}
//		@Test(priority=8)
//		public void addanothercolur() throws InterruptedException {
//			String colour[]=productdetailpages.addanothercolurtest();
//			String selectedcolur=colour[0];
//			String selectedcolur1=colour[1];
//			System.out.println(selectedcolur);
//			System.out.println(selectedcolur1);
//			Assert.assertTrue(selectedcolur.contains(selectedcolur1));
//			
//			
//		}
//		@Test(priority=9)
//		public void Addtocart() throws InterruptedException {
//			String qty[]=productdetailpages.SingleProductdiffcoluraddtocart();
//			String qtydetailpage=qty[0];
//			String addtocartpopup=qty[1];
//			
//			Assert.assertEquals(qtydetailpage, addtocartpopup);
//			
//		}
//	
//////*********************************Shopping Cart Testcases****************************//
//		 @Test(priority=11)
//			public void qtytest() throws InterruptedException {
//			 productdetailpage.AddtocartQtytest();
//				int qty[]=productdetailpage.qtytest();
//				int qty1=qty[0];
//				int qty2=qty[1];	
//				Assert.assertEquals(qty1, qty2);
//				
//			}
//	 
//		 @Test(priority=12)
//			public void qtytest2() throws InterruptedException {
//			 productdetailpage.AddtocartQtytest();
//				int qty[]=productdetailpage.qtytest2();
//				int qty1=qty[0];
//				int qty2=qty[1];
//				
//				Assert.assertEquals(qty1, qty2);
//				
//			}
//
//		 @Test(priority=13)
//			public void pricetest1() throws InterruptedException {
//			 productdetailpage.AddtocartQtytest();
//				float[] qty=productdetailpage.pricetest1();
//				float qty1=qty[0];
//				float qty2=qty[1];
//				
//				Assert.assertEquals(qty1, qty2);
//				
//			}
//
//		 
//		 @Test(priority=15)
//			public void clearcart() throws InterruptedException {
//			 productdetailpage.AddtocartQtytest();
//				boolean size=productdetailpage.clearcarttest();
//				
//				Assert.assertTrue(size);
//				
//			}
//
//	 	@Test(priority=17)
//		public void cartupdationqtytest() throws InterruptedException {
//		 productdetailpage.AddtocartQtytest();
//			int qty[]=productdetailpage.cartupdation();
//			int qty1=qty[0];
//			int qty2=qty[1];
//			
//			Assert.assertEquals(qty1, qty2);
//			
//		}
//	 	
//		@Test(priority=18)
//		public void cartupdationpricetest() throws InterruptedException {
//		 productdetailpage.AddtocartQtytest();
//			float[] qty=productdetailpage.cartupdationpricetest1();
//			float qty1=qty[0];
//			float qty2=qty[1];
//			
//			Assert.assertEquals(qty1, qty2);
//			
//	}
//			 
//		 @Test(priority=20)
//			public void removeicontest() throws InterruptedException {
//			 productdetailpage.addanothercolurtest();
//		 	productdetailpage.Removeiconclick();
//				
//				
//			}
//		 @Test(priority=21)
//			public void subtotaltest() throws InterruptedException {
//			 productdetailpage.addanothercolurtest();
//		 	productdetailpage.Estimatedeliverydatecheck();
//								
//			}
//		 @Test(priority=22)
//			public void checkoutbutton() throws InterruptedException {
//			 productdetailpage.addanothercolurtest();
//			 Thread.sleep(5000);
//			 productdetailpage.checkoutclick();
//					
//			}
//		 @Test(priority=22)
//			public void guestcheckout() throws InterruptedException {
//			 productdetailpage.addanothercolurtest();
//			 Thread.sleep(5000);
//			 productdetailpage.checkoutclick();
//			 productdetailpage.qtytestcheckout();
//					
//			}
		 @Test(priority=23)
			public void guestcheckoutprice() throws InterruptedException, AWTException {
			 productdetailpage.AddtocartQtytest();
			 Thread.sleep(10000);
			// signinpage.Signin1(prop.getProperty("Emailid"), prop.getProperty("password"));
			
			 productdetailpage.checkoutclick();
			 productdetailpage.Shippingaddress(prop.getProperty("fname"), prop.getProperty("email"), prop.getProperty("companyname"), prop.getProperty("address"), prop.getProperty("city"), prop.getProperty("state"), prop.getProperty("zipcode"), prop.getProperty("phonenumber"));
			productdetailpage.paymenttest1();
			int[]qty= productdetailpage.qtytestcheckout();
			 int qty1=qty[0];
			 int qty2=qty[1];
			 Assert.assertEquals(qty1, qty2); 
			 float[] price=	productdetailpage.checkoutprice();
			 float price1=price[0];
			 float price2=price[1];
			 Assert.assertEquals(price1, price2);
			 Thread.sleep(5000);
			 productdetailpage.Deliverymethodtest2();
			 Thread.sleep(5000);
					
			}
		 
		 @Test(priority=23)
			public void guestcheckoutprice2() throws InterruptedException, AWTException {
			 productdetailpage.AddtocartQtytest();
			 Thread.sleep(10000);
			// signinpage.Signin1(prop.getProperty("Emailid"), prop.getProperty("password"));
			
			 productdetailpage.checkoutclick();
			 productdetailpage.Shippingaddress(prop.getProperty("fname"), prop.getProperty("email"), prop.getProperty("companyname"), prop.getProperty("address"), prop.getProperty("city"), prop.getProperty("state"), prop.getProperty("zipcode"), prop.getProperty("phonenumber"));
			productdetailpage.paymenttest2();
			 productdetailpage.Billingaddresstest();
			 int[]qty= productdetailpage.qtytestcheckout();
			 int qty1=qty[0];
			 int qty2=qty[1];
			 Assert.assertEquals(qty1, qty2); 
			 float[] price=	productdetailpage.checkoutprice();
			 float price1=price[0];
			 float price2=price[1];
			 Assert.assertEquals(price1, price2);
			 Thread.sleep(5000);
			 productdetailpage.Deliverymethodtest();
			 Thread.sleep(5000);
			
					
			}
		 @AfterMethod
			public void teardown() {
				driver.quit();
			}
}
