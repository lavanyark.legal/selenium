
package com.core.qa.testcases;

import java.awt.AWTException;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.GuestAddtocartBlank;
import com.core.qa.pages.ProductListingpage;
import com.core.qa.pages.Signinpage;
import com.core.qa.util.TestUtil;
import com.paulhammant.ngwebdriver.NgWebDriver;

public class RegisteredusercheckoutflowTestcases extends TestBase {
	
	
	ProductListingpage productlistingpage;
	GuestAddtocartBlank productdetailpage;
	Signinpage signinpage;
	
	String sheetName="Shipping Address";

	 public RegisteredusercheckoutflowTestcases() {
		 super();
	}

	 @BeforeMethod
		public void setup() throws InterruptedException {
			initialization();
			signinpage=new Signinpage();
			productlistingpage=new 	ProductListingpage();
			productdetailpage=new GuestAddtocartBlank();
			productlistingpage.randomproductclick();
			
		}
	 
//*****************Test case:Checking the Addtocart pop up that Qty added in the Product details page *******************
	 
	 @Test(priority=22)
		public void Addtocart() throws InterruptedException {
			String qty[]=productdetailpage.AddtocartQtytest();
			 Thread.sleep(10000);
			 signinpage.Signin1(prop.getProperty("Emailid"), prop.getProperty("password"));
			 Thread.sleep(10000);
			 JavascriptExecutor jsDriver;
			 NgWebDriver ngWebDriver;
			 jsDriver = (JavascriptExecutor) driver;
			 ngWebDriver = new NgWebDriver(jsDriver);
			 try {
				 ngWebDriver.waitForAngularRequestsToFinish();

			 }
			 catch (Exception e) {

			}
			String qtydetailpage=qty[0];
			String addtocartpopup=qty[1];	
			Assert.assertEquals(qtydetailpage, addtocartpopup);		
		}
	//Checking the Qty and subqty in the Shopping cart page	
	 
	 @Test(priority=23)
		public void qtytest() throws InterruptedException {
		 productdetailpage.AddtocartQtytest();
		 Thread.sleep(10000);
		 signinpage.Signin1(prop.getProperty("Emailid"), prop.getProperty("password"));
		 Thread.sleep(10000);
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
			int qty[]=productdetailpage.qtytest();
			int qty1=qty[0];
			int qty2=qty[1];	
			Assert.assertEquals(qty1, qty2);
			
		}	 
	//Checking the Inputqty and subqty in shopping cart page

	 @Test(priority=24)
		public void qtytest2() throws InterruptedException {
		 productdetailpage.AddtocartQtytest();
		 Thread.sleep(10000);
		 signinpage.Signin1(prop.getProperty("Emailid"), prop.getProperty("password"));
		 Thread.sleep(10000);
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
			int qty[]=productdetailpage.qtytest2();
			int qty1=qty[0];
			int qty2=qty[1];
			
			Assert.assertEquals(qty1, qty2);
			
		}
	//Checking the Price and Subtotalprice in the Shopping cart page	 

	 @Test(priority=25)
		public void pricetest1() throws InterruptedException {
		 productdetailpage.AddtocartQtytest();
		 Thread.sleep(10000);
		 signinpage.Signin1(prop.getProperty("Emailid"), prop.getProperty("password"));
		 Thread.sleep(10000);
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {

		}
			float[] qty=productdetailpage.pricetest1();
			float qty1=qty[0];
			float qty2=qty[1];
			
			Assert.assertEquals(qty1, qty2);
			
		}



	 @Test(priority=28)
		public void clearcart() throws InterruptedException {
		 signinpage.Signin1(prop.getProperty("Emailid"), prop.getProperty("password"));
		 Thread.sleep(5000);
		 productdetailpage.AddtocartQtytest();
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {
	
		}
		boolean size=productdetailpage.clearcarttest();
		Assert.assertTrue(size);
		
		}	
	
	 @Test(priority=29)
	 public void RegistereduserfullcheckoutTest1() throws InterruptedException {
		 signinpage.Signin1(prop.getProperty("Emailid"), prop.getProperty("password"));
		 Thread.sleep(7000);
		 productdetailpage.AddtocartQtytest();
		 JavascriptExecutor jsDriver;
		 NgWebDriver ngWebDriver;
		 jsDriver = (JavascriptExecutor) driver;
		 ngWebDriver = new NgWebDriver(jsDriver);
		 try {
			 ngWebDriver.waitForAngularRequestsToFinish();

		 }
		 catch (Exception e) {
	
		}
		// productdetailpage.checkoutclick();
		 
		
}



@Test(priority=30)
public void RegistereduserfullcheckoutTest2(String FullName,String Email,String Companyname,String Streetaddress,String city,String state,String zip,String phonenumber) throws InterruptedException {
	 productdetailpage.AddtocartQtytest();
	 Thread.sleep(5000);
	 signinpage.Signin1(prop.getProperty("Emailid"), prop.getProperty("password"));
	 Thread.sleep(10000);
	 JavascriptExecutor jsDriver;
	 NgWebDriver ngWebDriver;
	 jsDriver = (JavascriptExecutor) driver;
	 ngWebDriver = new NgWebDriver(jsDriver);
	 try {
		 ngWebDriver.waitForAngularRequestsToFinish();

	 }
	 catch (Exception e) {

	}
	 productdetailpage.checkoutbuttonclick();
	 productdetailpage.newshippingaddressadd(FullName, Email, Companyname, Streetaddress, city, state, zip, phonenumber);
	 Thread.sleep(7000);
	 productdetailpage.newcardadd();
	 Thread.sleep(7000);
	 productdetailpage.newbillingaddressadd();
	 Thread.sleep(7000);
	 int[]qty= productdetailpage.qtytestcheckout();
	 int qty1=qty[0];
	 int qty2=qty[1];
	 Assert.assertEquals(qty1, qty2); 
	 float[] price=	productdetailpage.checkoutprice();
	 float price1=price[0];
	 float price2=price[1];
	 Assert.assertEquals(price1, price2);
	// productdetailpage.placeorderbuttonclick();
}
@Test(priority=31)
public void RegistereduserfullcheckoutTest3() throws InterruptedException, AWTException {
	 Thread.sleep(5000);
	 productdetailpage.addanothercolurtest();
	 Thread.sleep(10000);
	 signinpage.Signin1(prop.getProperty("Emailid"), prop.getProperty("password"));
	 Thread.sleep(10000);
	 JavascriptExecutor jsDriver;
	 NgWebDriver ngWebDriver;
	 jsDriver = (JavascriptExecutor) driver;
	 ngWebDriver = new NgWebDriver(jsDriver);
	 try {
		 ngWebDriver.waitForAngularRequestsToFinish();

	 }
	 catch (Exception e) {

	}
	 productdetailpage.checkoutbuttonclick();
	 
	 productdetailpage.shippingaddresschange();
	 Thread.sleep(5000);
	 productdetailpage.paymentmethodchange();
	 Thread.sleep(5000);
	productdetailpage.billingaddresschange();
	 Thread.sleep(5000);
	 int[]qty= productdetailpage.qtytestcheckout();
	 int qty1=qty[0];
	 int qty2=qty[1];
	 Assert.assertEquals(qty1, qty2); 
	 float[] price=	productdetailpage.checkoutprice();
	 float price1=price[0];
	 float price2=price[1];
	 Assert.assertEquals(price1, price2);
	productdetailpage.Deliverymethodtest();
}
@AfterMethod
public void teardown() {
	driver.quit();
}
}

