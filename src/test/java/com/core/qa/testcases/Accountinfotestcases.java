package com.core.qa.testcases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.Accountdashboardpage;
import com.core.qa.pages.Accountinfopage;
import com.core.qa.pages.Homepage;
import com.core.qa.pages.Myaccountpage;
import com.core.qa.pages.Signinpage;
import com.core.qa.pages.Subscriptionspage;

public class Accountinfotestcases extends TestBase{
	Myaccountpage myaccount;
	Signinpage signinpage;
	Accountdashboardpage accountdashboard;
	Accountinfopage accountinfopage;

	 public Accountinfotestcases() {
		 super();
	}
	 
	 @BeforeMethod
		public void setup() throws InterruptedException {
			initialization();
			myaccount=new 	Myaccountpage();
			signinpage=new Signinpage();
			accountinfopage=new Accountinfopage();

			accountdashboard=new Accountdashboardpage();
			signinpage.Signin1(prop.getProperty("Emailid"), prop.getProperty("password"));
			
			 
			
		}
	 @Test(priority=1)
		public void accountinfotest() throws InterruptedException {
	
		 accountinfopage.accountinfotest();
			
		}
	 
	 @Test(priority=2)
		public void saveaccountinfo() throws InterruptedException {
		
		 accountinfopage.saveaccountinfo();
		}

	 
	 @Test(priority=3)
		public void cancelaccountinfo() throws InterruptedException {
		
		 accountinfopage.cancelaccountinfo();
		}
	 
	 @Test(priority=4)
		public void changepassword() throws InterruptedException {
		
		 accountinfopage.changepassword();
		}
	 
	 @AfterMethod
		public void teardown() {
			driver.quit();
		}
}
