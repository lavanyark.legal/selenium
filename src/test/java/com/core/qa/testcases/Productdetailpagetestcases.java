package com.core.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.ProductListingpage;
import com.core.qa.pages.Productdetailpage;

public class Productdetailpagetestcases extends TestBase {
	
	ProductListingpage productlistingpage;
	Productdetailpage productdetailpage;
	 public Productdetailpagetestcases() {
		 super();
	}
	
	 @BeforeMethod
		public void setup() {
			initialization();
			productlistingpage=new 	ProductListingpage();
			productdetailpage=new Productdetailpage();
			productlistingpage.randomproductclicktest2();
			
		}
	 
		@Test
		public void Viewfulldescriptiontest() throws InterruptedException {
			productdetailpage.Viewfulldescriptionlinktest();
			
			
		}
		
		@Test
		public void Wishlisttest() throws InterruptedException {
			productdetailpage.Wishlisttest();
			
			
		}
		
		@Test
		public void Wishlisttest2() throws InterruptedException {
			productdetailpage.Wishlisttest2();
			
		}
		
		@Test
		public void sharetest() throws InterruptedException {
			productdetailpage.sharetest();
			
		}
		

		@Test
		public void pricecomparelowaslowasandtierprice() throws InterruptedException {
			String price[]=productdetailpage.lowaspricetest();
			String aslowasprice=price[0];
			String tierprice=price[1];
			Assert.assertTrue(tierprice.contains(aslowasprice));
			
			
		}
		@Test
		public void Selectedcolourtest() throws InterruptedException {
			String colour[]=productdetailpage.Selectedcolourtest();
			String selectedcolur=colour[0];
			String selectedcolur1=colour[1];
			Assert.assertTrue(selectedcolur.contains(selectedcolur1));
			
			
		}
		@Test
		public void Changecolour() throws InterruptedException {
			String colour[]=productdetailpage.Changecolourtest();
			String selectedcolur=colour[0];
			String selectedcolur1=colour[1];
			System.out.println(selectedcolur);
			System.out.println(selectedcolur1);
			Assert.assertTrue(selectedcolur.contains(selectedcolur1));
			
			
		}
		@Test
		public void addanothercolur() throws InterruptedException {
			String colour[]=productdetailpage.addanothercolurtest();
			String selectedcolur=colour[0];
			String selectedcolur1=colour[1];
			System.out.println(selectedcolur);
			System.out.println(selectedcolur1);
			Assert.assertTrue(selectedcolur.contains(selectedcolur1));
			
			
		}
		@Test
		public void Addtocart() throws InterruptedException {
			String qty[]=productdetailpage.SingleProductdiffcoluraddtocart();
			String qtydetailpage=qty[0];
			String addtocartpopup=qty[1];
			
			Assert.assertEquals(qtydetailpage, addtocartpopup);
			
		}
	
		@AfterMethod
		public void teardown() {
			driver.quit();
		}

}
