package com.core.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.Myaccountpage;
import com.core.qa.pages.Myorderspage;
import com.core.qa.pages.Signinpage;

public class Myorderstestcases extends TestBase {

	Myaccountpage myaccount;
	Signinpage signinpage;
	Myorderspage myorders;
	 public Myorderstestcases() {
		 super();
	}
	 @BeforeMethod
		public void setup() throws InterruptedException {
			initialization();
			myaccount=new 	Myaccountpage();
			myorders=new Myorderspage();
			signinpage=new Signinpage();
			signinpage.Signin1(prop.getProperty("Emailid"), prop.getProperty("password"));
			
		}
	
	 
	 @Test(priority=1)
		public void myordersurltest() throws InterruptedException {
		 	String flag =myorders.myorderstest();
		 	Assert.assertEquals(prop.getProperty("url")+"account/orders", flag);
			
			
		}
	 @Test(priority=2)
		public void myorderscustomsearchtest() throws InterruptedException {
		 myorders.Myorderscustomtablesearchtest();
		 
			
			
		}
	 @Test(priority=3)
		public void myorderscustomstatustest() throws InterruptedException {
		 myorders.Myorderscustomstatustest();
		 	
			
			
		}
	 @Test(priority=4)
		public void myorderscustomorderdetailstest() throws InterruptedException {
		 myorders.Myorderscustomorderdetailstest();
		 	
			
			
		}
	 
	 @Test(priority=5)
		public void myorderscustompaginationstest() throws InterruptedException {
		 myorders.Myorderscustompaginationtest();
		 	
			
			
		}
	 @Test(priority=6)
		public void myordersblanksearchtest() throws InterruptedException {
		 myorders.Myordersblanktablesearchtest();
		 	
			
			
		}
	 
	 @Test(priority=7)
		public void myordersblankstatustest() throws InterruptedException {
		 myorders.Myordersblankstatustest();
		 	
			
			
		}
	 @Test(priority=8)
		public void myordersblankorderdetailtest() throws InterruptedException {
		 myorders.Myordersblankorderdetailstest();
		 	
			
			
		}
	 @Test(priority=9)
		public void myordersblankpaginationstest() throws InterruptedException {
		 myorders.Myordersblankpaginationtest();
		 	
			
			
		}
	 

		@AfterMethod
		public void teardown() {
			driver.quit();
		}

}
