package com.core.qa.testcases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.Accountdashboardpage;
import com.core.qa.pages.Homepage;
import com.core.qa.pages.Myaccountpage;
import com.core.qa.pages.Signinpage;
import com.core.qa.pages.Subscriptionspage;

public class Subscriptiontestcases extends TestBase{
	
	Myaccountpage myaccount;
	Signinpage signinpage;
	Accountdashboardpage accountdashboard;
	Subscriptionspage subscription;
	Homepage homepage;
	 public Subscriptiontestcases() {
		 super();
	}
	 
	 @BeforeMethod
		public void setup() throws InterruptedException {
			initialization();
			myaccount=new 	Myaccountpage();
			signinpage=new Signinpage();
			subscription=new Subscriptionspage();
			homepage=new Homepage();
			accountdashboard=new Accountdashboardpage();
			signinpage.Signin1(prop.getProperty("Emailid"), prop.getProperty("password"));
			 myaccount.subscriptiontest();
			 
			
		}
	 @Test(priority=1)
		public void subscriptiontest1() throws InterruptedException {
		 Thread.sleep(5000);
		 subscription.subsciptiontest1();
			
		}
	 
	 @Test(priority=2)
		public void subscriptiontest2() throws InterruptedException {
		 Thread.sleep(5000);
		 subscription.subsciptiontest2();
			
		}
	 @AfterMethod
		public void teardown() {
			driver.quit();
		}

}
