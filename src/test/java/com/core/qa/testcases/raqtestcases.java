package com.core.qa.testcases;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.GuestAddtocartBlank;
import com.core.qa.pages.ProductListingpage;
import com.core.qa.pages.Signinpage;
import com.core.qa.pages.flatembquickQuoteSubmission;
import com.core.qa.pages.raq;

public class raqtestcases extends TestBase{
	
	
	ProductListingpage productlistingpage;
	GuestAddtocartBlank productdetailpage;
	raq Raq;
	Signinpage signinpage;
	public raqtestcases() {
		 super();
	}
	
	 @BeforeMethod
		public void setup() {
			initialization();
			Raq=new raq();
		}
	 
	 @Test(priority=32)
		public void quickquotetest1() throws InterruptedException, AWTException {
		 boolean flag=Raq.Quickquotetest1();
		 Assert.assertTrue(flag);
		}
	 @Test(priority=37)
		public void quickquotetest2() throws InterruptedException, AWTException {
		 boolean flag=Raq.Quickquotetest2();
		 Assert.assertTrue(flag);
		}
	 
	 @Test(priority=38)
		public void quickquotetest3() throws InterruptedException, AWTException {
		 signinpage=new Signinpage();
		 signinpage.Signin1(prop.getProperty("Emailid"),prop.getProperty("password"));
		 boolean flag=Raq.Quickquotetest3();
		 Assert.assertTrue(flag);
		}
	 
	 @Test(priority=39)
		public void quickquotetest4() throws InterruptedException, AWTException {
		 boolean flag=Raq.Quickquotetest4();
		 Assert.assertTrue(flag);
		}
	 
	 @Test(priority=40)
		public void quickquotetest5() throws InterruptedException, AWTException {
		 boolean flag=Raq.Quickquotetest5();
		 Assert.assertTrue(flag);
		}
		@AfterMethod
		public void teardown() {
			driver.quit();
		}
}
