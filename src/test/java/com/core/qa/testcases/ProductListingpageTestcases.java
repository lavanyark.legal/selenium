package com.core.qa.testcases;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.ProductListingpage;
import com.core.qa.pages.Signinpage;

public class ProductListingpageTestcases extends TestBase {
	ProductListingpage productlistingpage;
	
	 public ProductListingpageTestcases() {
		 super();
	}
	
	@BeforeMethod
	public void setup() {
		initialization();
		productlistingpage=new 	ProductListingpage();
		
	}
	
//	@Test
//	public void Totalitemscounttest() throws InterruptedException {
//		int[] count= productlistingpage.totalitemscount();
//		int itemscount=count[0];
//		int datacount=count[1];
//		Assert.assertEquals(itemscount, datacount);
//		
//		
//	}
	
	
//	@Test(priority=1)
//	public void Sortbydefaulttest1() throws InterruptedException {
//		String sorttext=productlistingpage.bydefaultsorting();
//		Assert.assertTrue(sorttext.contains("Popularity"));
//	}
//	
//	@Test(priority=2)
//	public void Relavancetest() throws InterruptedException {
//		String sorttext=productlistingpage.Relevencetest();
//		Assert.assertTrue(sorttext.contains("Relevance"));
//	}
//	@Test(priority=2)
//	public void lowestfirsttest() throws InterruptedException {
//		String sorttext=productlistingpage.lowerstfirstsorttest();
//		Assert.assertTrue(sorttext.contains("Lowest first"));
//	}
//	@Test(priority=3)
//	public void highestfirsttest() throws InterruptedException {
//		String sorttext=productlistingpage.highestfirstsorttest();
//		Assert.assertTrue(sorttext.contains("Highest first"));
//	}
//	
//	@Test(priority=4)
//	public void mainncategoryfiltertest() throws InterruptedException {
//		productlistingpage.categoryfiltercounttest();
//		
//		
//	}
//	
//	@Test(priority=5)
//	public void Accesseriesfiltertest() throws InterruptedException {
//		String text=productlistingpage.Accessoriesfiltertest();
//		Assert.assertEquals(text, prop.getProperty("url")+"p/accessories/ca/158");
//		
//	}
//	@Test(priority=6)
//	public void Workwearfiltercounttest() throws InterruptedException {
//		int[] count= productlistingpage.workwearfiltercounttest();
//		int itemscount=count[0];
//		int datacount=count[1];
//		Assert.assertEquals(itemscount, datacount);
//}
//	
//		
//		@Test(priority=7)
//		public void colurgroupcounttest() throws InterruptedException {
//			int[] count= productlistingpage.Colourgroupfiltercounttest();
//			int itemscount=count[0];
//			int datacount=count[1];
//			Assert.assertEquals(itemscount, datacount);
//		
//		
//	}
//	@Test(priority=8)
//	public void polosubfiltertest() throws InterruptedException {
//		String text=productlistingpage.polossubfiltertest();
//		Assert.assertEquals(text, prop.getProperty("url")+"p/polosknits/ca/145?categories=145&categories=146");
//		
//	}
//	@Test(priority=9)
//	public void Tshirtsubfiltertest() throws InterruptedException {
//		String text=productlistingpage.Tshirtsubfiltertest();
//		Assert.assertEquals(text, prop.getProperty("url")+"p/t-shirts/ca/222?categories=222&categories=229");
//		
//	}
//	@Test(priority=10)
//	public void Randomproductclicktest() throws InterruptedException {
//		String[] text=productlistingpage.randomproductclick();
//		String styleno=text[0];
//		String price=text[1];
//		String style=driver.findElement(By.xpath("//div[@class='review-wrapper']//h2")).getText();
//		String pricedetailpage=driver.findElement(By.xpath("//div[@class='product-save_banner ng-star-inserted']//p")).getText();
//		Assert.assertTrue(style.contains(styleno));
//		Assert.assertEquals(price, pricedetailpage);
//		
//	}
//	@Test(priority=11)
//	public void Randomproductclicktest2() throws InterruptedException {
//		productlistingpage.randomproductclicktest2();	
//		
//		
//	}
//	@Test(priority=11)
//	public void pricefiltertest() throws InterruptedException {
//		productlistingpage.pricefiltertest();
//		
//	}
	
	@Test(priority=11)
	public void test() throws InterruptedException {
		productlistingpage.productdetailpagetest();
		
	}
	
	@AfterMethod
	public void teardown() {
		driver.quit();
	}

}
