package com.core.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.GuestAddtocartBlank;
import com.core.qa.pages.Myaccountpage;
import com.core.qa.pages.ProductListingpage;
import com.core.qa.pages.Signinpage;

public class Myaccounttestcases extends TestBase {
	
	Myaccountpage myaccount;
	Signinpage signinpage;
	 public Myaccounttestcases() {
		 super();
	}
	 @BeforeMethod
		public void setup() throws InterruptedException {
			initialization();
			myaccount=new 	Myaccountpage();
			signinpage=new Signinpage();
			signinpage.Signin1(prop.getProperty("Emailid"), prop.getProperty("password"));
			
		}
	 @Test(priority=1)
		public void accountdashboardtesttable() throws InterruptedException {
		 	boolean flag =myaccount.accountdashboardtest();
		 	Assert.assertTrue(flag);
	 }
			
			

	 
	 @Test(priority=2)
		public void myorderstest() throws InterruptedException {
		 	String flag =myaccount.myorderstest();
		 	Assert.assertEquals(prop.getProperty("url")+"account/orders", flag);
			
			
		}
	 
	 @Test(priority=3)
		public void myquoterequesttest() throws InterruptedException {
		 	boolean flag =myaccount.myquoterequesttest();
		 	Assert.assertTrue(flag);
			
			
		}
	 
	 @Test(priority=4)
		public void mywishlisttest() throws InterruptedException {
		 	boolean flag =myaccount.mywishlist();
		 	Assert.assertTrue(flag);
				
		}
	 
	 @Test(priority=5)
		public void mysavedcardstest() throws InterruptedException {
		 	myaccount.mysavedcards();
		
		}
	 
	 @Test(priority=6)
		public void mysavedcardstest2() throws InterruptedException {
		 	myaccount.mysavedcards2();
		
		}
	 
	 @Test(priority=7)
		public void accountinfortest() throws InterruptedException {
		 	boolean flag= myaccount.accountinfotest();
		 	Assert.assertTrue(flag);
		
		}
	 
	 @Test(priority=8)
		public void addressstest() throws InterruptedException {
		 myaccount.addresstest();
		
		}
	 
	 @Test(priority=9)
		public void faq() throws InterruptedException {
		 myaccount.faqtest();
		}
	 
	 @Test(priority=10)
		public void subscription() throws InterruptedException {
		 myaccount.subscriptiontest();
		}
	 
	 
	 @Test(priority=11)
		public void logout() throws InterruptedException {
		 myaccount.logouttest();
		}
	 
		@AfterMethod
		public void teardown() {
			driver.quit();
		}
}

