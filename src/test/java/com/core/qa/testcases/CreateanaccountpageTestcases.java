package com.core.qa.testcases;


import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.Createanaccountpage;
import com.core.qa.util.TestUtil;



public class CreateanaccountpageTestcases extends TestBase {
	
	
	Createanaccountpage createanaccount;
	public static String randomEmail = randomEmail();
	public static String mailid=randomEmail;
	
	public CreateanaccountpageTestcases() {
		super();
	}
	@BeforeMethod
	public void setup() {
		initialization();
		createanaccount=new Createanaccountpage();
	
	}

	
	@Test(priority=1)
	public void validdetails(String firstname,String lastname,String email,String password,String confirmpassword) throws InterruptedException {
		boolean flag=createanaccount.createaccount1(firstname,lastname,randomEmail(),password,confirmpassword);
		Assert.assertTrue(flag);

	}
	
	@Test(priority=2)
	public void alreadyregisterdmail() throws InterruptedException {
		boolean flag=createanaccount.createaccount2(prop.getProperty("firstname"),prop.getProperty("lastname"),prop.getProperty("Emailid"),prop.getProperty("password1"),prop.getProperty("confirmpassword"));
		
		Assert.assertTrue(flag);
		
	}
	
	@Test(priority=3)
	public void invalidconfirmpass() throws InterruptedException {
		boolean flag=createanaccount.createaccount3(prop.getProperty("firstname"),prop.getProperty("lastname"),randomEmail(),prop.getProperty("password1"),"14785");
		Assert.assertTrue(flag);
		
	}
	
	@Test(priority=4)
	public void newslettersubs() throws InterruptedException {
		boolean flag=createanaccount.createaccount4(prop.getProperty("firstname"),prop.getProperty("lastname"),randomEmail(),prop.getProperty("password1"),prop.getProperty("confirmpassword"));
		Assert.assertTrue(flag);
	}
	
	@Test(priority=5)
	public void emptydata() throws InterruptedException {
		boolean flag=createanaccount.createaccount5();
		Assert.assertTrue(flag);
	}
	
	@AfterMethod
	public void teardown() {
		driver.quit();
	}


}
