package com.core.qa.testcases;


import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.GuestAddtocartBlank;
import com.core.qa.pages.Orderstatuspage;
import com.core.qa.pages.ProductListingpage;
import com.core.qa.pages.Signinpage;
import com.core.qa.util.TestUtil;

public class Orderstatuspagetestcase extends TestBase {
	
	ProductListingpage productlistingpage;
	GuestAddtocartBlank productdetailpage;
	Signinpage signinpage;
	Orderstatuspage orderstatus;
	GuestcheckoutTestcases guestcheckouttestcases;
	
	String sheetName="Shipping Address";

	 public Orderstatuspagetestcase() {
		 super();
	}
	 @BeforeMethod
		public void setup() throws InterruptedException {
			initialization();
			productlistingpage=new 	ProductListingpage();
			productdetailpage=new GuestAddtocartBlank();
			productlistingpage.randomproductclick();
			orderstatus=new Orderstatuspage();
		}
	
	 @Test(priority=1)
	 	public void emptyformtest() throws InterruptedException {

		 boolean flag=orderstatus.emptyformcheck();
		 Assert.assertTrue(flag);	 
	 }
	 
	 	
	 @Test(priority=2, dataProvider="getshippingaddressdata")
	    public void orderstatusvaliddettest(String FullName,String Email,String Companyname,String Streetaddress,String city,String state,String zip,String phonenumber) throws InterruptedException {
		 productdetailpage.AddtocartQtytest(); 
		 Thread.sleep(5000);
		 productdetailpage.checkoutclick();
		 productdetailpage.Shippingaddress(FullName, Email, Companyname, Streetaddress, city,state,zip,phonenumber);
		 productdetailpage.paymenttest1();
		 int[]qty= productdetailpage.qtytestcheckout();
		 int qty1=qty[0];
		 int qty2=qty[1];
		 Assert.assertEquals(qty1, qty2); 
		 float[] price=	productdetailpage.checkoutprice();
		 float price1=price[0];
		 float price2=price[1];
		 Assert.assertEquals(price1, price2);
		 Thread.sleep(5000);
		 
		 
	 }
}
