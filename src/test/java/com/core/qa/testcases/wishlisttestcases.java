package com.core.qa.testcases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.core.qa.base.TestBase;
import com.core.qa.pages.ProductListingpage;
import com.core.qa.pages.Productdetailpage;
import com.core.qa.pages.Signinpage;
import com.core.qa.pages.wishlistpage;

public class wishlisttestcases extends TestBase {
	
	wishlistpage wishlist;
	ProductListingpage productlistingpage;
	Productdetailpage productdetailpage;
	 public wishlisttestcases() {
		 super();
	}
	 
	 @BeforeMethod
		public void setup() throws InterruptedException {
			initialization();
			wishlist=new wishlistpage();
			productlistingpage=new 	ProductListingpage();
			productdetailpage=new Productdetailpage();
			productlistingpage.randomproductclicktest2();
			productdetailpage.Wishlisttest2();
			 wishlist.mywishlist();
		}
	 

	 @Test(priority=1)
		public void wishlisttest1() throws InterruptedException {
		 wishlist.wishlisttest1();
				
		}
	 
	 @Test(priority=2)
		public void wishlisttest2() throws InterruptedException {
		 wishlist.deletewishlist();
				
		}
	 
	 @Test(priority=3)
		public void wishlisttest3() throws InterruptedException {
		 wishlist.wishlisttest3();
				
		}
		@AfterMethod
		public void teardown() {
			driver.quit();
		}
}
